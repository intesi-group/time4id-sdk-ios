//
//  NSURLResponse+PrettyLogging.h
//  Time4ID-Core
//
//  Created by francesco scalise on 19/11/23.
//

#import <Foundation/Foundation.h>

@interface NSURLResponse (PrettyLogging)

- (NSString *)prettyLogForResponseWithElapsedTime:(NSTimeInterval)elapsedTime
                                            error:(NSError *)error
                                       dictionary:(NSDictionary *)dictionary
                                          request:(NSURLRequest *)request;

@end
