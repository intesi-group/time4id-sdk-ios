//
//  RsaModel.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/23/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

@interface RSAGenerator : NSObject
{
    SecKeyRef publicKey;
    SecKeyRef privateKey;
}

- (void)encryptWithPublicKey:(const uint8_t *)plainBuffer cipherBuffer:(uint8_t *)cipherBuffer;
- (void)decryptWithPrivateKey:(uint8_t *)cipherBuffer plainBuffer:(uint8_t *)plainBuffer;
- (NSData*)decryptWithPrivateKey:(NSData*)cipheredData;
- (void)generateKeyPair;
- (NSData *)getPublicKeyBits;
- (NSData *)stripPublicKeyHeader:(NSData *)d_key;
- (NSString *)getRSAPublicKeyAsBase64;

@end
