//
//  RRecognition.h
//  VirtualRAO
//
//  Created by David Beduzzi on 18/07/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>
#import <Time4ID/RCAInfo.h>
#import <Time4ID/RCATemplate.h>
#import <Time4ID/RRecognitionItem.h>

@interface RRecognition : RLMObject

@property NSString* recognitionId;
@property NSString* ticket;
@property VRRecStatus status;
@property NSDate*   startDate;
@property NSDate*   updateDate;
@property RCAInfo*  ca;
@property RCATemplate* caTemplate;
@property NSString* user;
//@property VRRecognitionType recognitionType;
@property VRProductType   productType;
@property VRRecognitionMode recognitionMode;
@property NSString* userName;
@property NSString* userSurname;
@property NSString* userEmail;
@property NSString* recognitionLanguage;

// Privacy agreement info:
@property NSDate*   showDate;
@property bool      privacyAgreement;

// Filled stuff:
@property RLMArray<RRecognitionItem*><RRecognitionItem>* recognitionItems;

+(RRecognition*)recognitionWithDictionary:(NSDictionary*)recognition language:(NSString*)language ticket:(NSString*)ticket error:(NSError**)pError;

+(RRecognition*)objectForTicket:(NSString*)ticket;

-(bool)updateStatusOnEdit:(NSError**) pError;

-(NSArray*)getRecognitionData:(NSError**)error;
-(NSArray*)getMMFileData:(NSError**)error;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<RRecognition>
RLM_COLLECTION_TYPE(RRecognition)
