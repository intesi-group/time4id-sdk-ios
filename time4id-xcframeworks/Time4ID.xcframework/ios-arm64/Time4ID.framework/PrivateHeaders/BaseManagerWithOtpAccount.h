//
//  BaseManagerWithOtpAccount.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/6/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/OtpAccountCore.h>

@interface BaseManagerWithOtpAccount : NSObject

@property (nonatomic, retain) OtpAccountCore *otpAccount;

- (id)initWithOtpAccount:(OtpAccountCore *)_otpAccount;
- (BOOL)syncOtpAccount:(OtpAccountCore *)otpAccountToSync;

@end
