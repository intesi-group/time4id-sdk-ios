//
//  NSURLRequest+CURL.h
//  Time4ID-Core
//
//  Created by francesco scalise on 30/06/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSURLRequest (CURL)

- (NSString *)curl;
- (NSString *)curlVerbose:(BOOL)verbose;

@end

NS_ASSUME_NONNULL_END
