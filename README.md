# Time4ID SDK iOS

Official repository for Time4ID iOS SDK, a simple mobile suite to use Time4Mind by iOS.

For integration info, please refer to the [project wiki](https://gitlab.com/intesi-group/time4id-sdk-ios/-/wikis/home).

For technical API documentation, please refer [here](https://intesi-group.gitlab.io/time4id-sdk-ios/).

## Summary

Minimum supported iOS version is `iOS 12.0`
Dependencies:
- AFNetworking
- OpenSSL
- Realm
- CustomLoggerSDK (optional)

## Where are Time4ID.xcframework?

`Time4ID.xcframework` and its dependencies are located in the `time4id-xcframeworks` folder.

```
cd time4id-xcframeworks
ls -la
```

## Customer integration

### Getting started

### Step 1 (check xcworkspace)

```
cd sdk-poc-ios/TIME4ID-CUSTOMER-INTEGRATION
open TIME4ID-CUSTOMER-INTEGRATION.xcworkspace
```

### Step 2 (update license, username, password, accessToken)

- Add `<your-license>.p7m` to the `license` folder 

SWIFT
- Open Xcode -> File -> Add Files to TIME4ID-CUSTOMER-INTEGRATION-Swift (Swift version) -> Add the license
- Go to `AppDelegate.swift`
- Update these constants

```swift
let licenseFilename: String     = "<LICENSE FILENAME>"
let loginType: LoginType        = .unknown
```

### Step 3 (choose login type)

```swift
enum LoginType {
    
    case unknown
    case usernameAndPassword(_ username: String, _ password: String)
    case accessToken(_ accessToken: String)
}
```

### Step 4 (run)

- Product -> Run (simulator or physical device)

### Step 5 (check Xcode console)

here a sample output

```
Time4ID SDK 2.25.3 initialized

--- STEP 1 ---
✅✅✅ SDK SETUP DONE ✅✅✅
--- STEP 2 ---
✅✅✅ LOGIN DONE ✅✅✅
--- STEP 3 ---
✅✅✅ TOKEN INITIALIZATION DONE ->
   TOKEN_ID [<your token_id>]
✅✅✅
--- STEP 4 ---
✅✅✅ TOKEN FETCH DONE ✅✅✅
--- STEP 5 ---
✅✅✅ SEED GENERATION DONE ✅✅✅
--- STEP 6 ---
✅✅✅ TOKEN ACTIVATION DONE ✅✅✅
--- STEP 7 ---
✅✅✅ OTP GENERATION DONE ->
   OTP [<otp_value>]
✅✅✅
[...]
```