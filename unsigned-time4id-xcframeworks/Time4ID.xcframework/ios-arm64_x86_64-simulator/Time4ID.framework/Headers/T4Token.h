//
//  T4SharedToken.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 05/01/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Time4ID/T4TokenId.h>
#import <Time4ID/T4Service.h>
#import <Time4ID/T4Otp.h>

/** The secondary channel used to transmit the confirmation code during the enrollment process
 */
typedef NS_ENUM(NSUInteger, T4TokenBearer) {
/** Token doesn't require a confirmation code */
    T4TokenBearerNone = 0,
/** The confirmation code is sent to the user's device in an SMS text message. */
    T4TokenBearerSMS = 1,
/** The confirmation code is sent to the user via email to his/her registered email address. */
    T4TokenBearerEmail = 2,
/** The confirmation code is an OTP generated internally by a service token present on the App. */
    T4TokenBearerApp = 4,
/** The confirmation code is sent to the App through a push noification message. */
    T4TokenBearerPush = 8
};

/** Token synchronization status, as returned in the isSychronized callback
 */
typedef NS_ENUM(NSUInteger, T4TokenSyncStatus) {
    /** Token synchronization status is unknown, usually this is because an error. */
    T4TokenSyncStatusUnknown = 0,
    /** Token is synchronized. */
    T4TokenSyncStatusOk = 1,
    /** Token is locked. */
    T4TokenSyncStatusLocked = 2,
    /** Token is out of sync and requires a synchronization. */
    T4TokenSyncStatusOutOfSync = 3
};

/** Ticket consumption options
 */
typedef NS_ENUM(NSUInteger, T4OtpTicketOptions) {
    /** The OTP generation ticket can be kept for further use. */
    T4OtpTicketKeepAfterUse = 0,
    /** The OTP generation ticket should be invalidated immediately after the OTP generation. */
    T4OtpTicketInvalidateAfterUse = 1
};


/** A T4Token contains all the data and the functionalities to administer the token and to generate OTPs.
 */
@interface T4Token : NSObject <NSSecureCoding>

// Basic functions:

/**
 Returns all the tokens for the logged user
 
 @result NSArray containing the T4Token instances for all the user tokens present on the device
 */
+(NSArray<T4Token *> * _Nullable)findAll;

/**
 Returns the instance of the token identified by the tokenId specified.
 
 @param tokenId The token searched for
 
 @result The found token, or _nil_ if the token was not found on the device
 
 @see T4TokenId
 
 */
+(T4Token* _Nullable)find:(T4TokenId* _Nonnull)tokenId;

/**
 Remove the specified token locally from the device
 
 @param tokenId identifier of the token to remove
 
 @result Operation result code. _T4_OK_ if the operation was successful or an error code.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_NO_USER | -16 | No user logged in the SDK |
 | T4SDK_INVALID_TOKENID | -17 | Specified token is not present on the local storage |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @warning *Note:* The selector deletes the token just locally. Use the deleteToken API to remove the token from the backend also. Use the _remove_ API only if the token was already deleted by the backend.
 */
+(T4Error)remove:(T4TokenId* _Nonnull)tokenId;

/**
 Save or update the token on the local device.
 
 @result Operation result code. _T4_OK_ if the operation was successful or an error code.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_NO_USER | -16 | No user logged in the SDK |
 | T4SDK_INVALID_TOKENID | -17 | Specified token is not present on the local storage |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
-(T4Error)save;

/**
 Returns the _T4TokenId_ identifier for the token
 
 @result The token identifier
 */
-(T4TokenId* _Nonnull)getTokenId;

/**
 Returns the token provider type
 
 @result The token provider type
 
 @see T4TokenProviderType
 */
-(T4TokenProviderType)getProviderType;

/**
 Returns the user owning this token
 
 @result The user owning the token. Should always be the current logged in user.
 */
-(NSString* _Nonnull)getUsername;

/**
 Returns the token creation date on the device
 
 @result Token creation date
 */
-(NSDate* _Nonnull)getCreationDate;

/**
 Returns the token status
 
 @result The token status
 
 @see T4TokenStatus
 */
-(T4TokenStatus)getStatus;

/**
 Returns the confirmation code channel during the token's enrollment
 
 @result The confirmation code channel
 
 @see T4TokenBearer
 */
-(T4TokenBearer)getTokenBearer;

/**
 Returns the token's domain
 
 @result The token's domain
 */
-(NSString* _Nullable)getDomain;

/**
 Returns the token label
 
 @result The token label
 */
-(NSString* _Nullable)getLabel;

/** Returns the wrong PIN count.
 
 This value is meaningful only if the token is PIN protected (protection is T4PPNumeric or T4PPAlphanumeric) and the PIN error evidence is set to _YES_. It is the number of consecutive errors inserting the PIN.
 
 @result Wrong PIN count.
 */
-(NSInteger)getWrongPinCount;

/** Maximum number of PIN errors allowed before the Token becomes locally locked.
 
 This value is meaningful only if the token is PIN protected (protection is T4PPNumeric or T4PPAlphanumeric) and the PIN error evidence is set to _YES_.
 
 @result The maximum number of consecutive PIN errors allowed for a PIN protected token with PIN error evidence.
 
 @warning *Note:* If the PIN error evidence is set to _NO_, the maximum wrong PIN count is a value kept on the server and is unknown to the SDK.

 */
-(NSInteger)getMaxTryPinCount;

/** Returns the length of the generated OTP
 
 @result Length of the generated OTP
 */
-(NSInteger)getOtpLength;

/** Returns the OTP time step in seconds.
 
 The time step is the validity window of a generated OTP. Usually it is 30 seconds
 
 @result The OTP time step in seconds.
 */
-(NSInteger)getTimeStep;

/** Returns the OTP type.
 
 The generated OTP can be time based or event based
 
 @result The OTP type.
 
 @see T4OtpType
 */
-(T4OtpType)getOtpType;

/** Returns the status of the begin-end algorithm.
 
 If enabled, the OTP contains also information about the specific device that generated it.
 
 @result _true_ if the begin-end algorithm is enabled, otherwise _false_
 */
-(bool)isBEEnabled;

/** Returns the wrong PIN evidence.
 
 @result _true_ if the token is PIN protected and the wrong PIN evidence is set, otherwise _false_
 */
-(bool)hasWrongPinEvidence;

/**
 Returns the time interval in seconds after which the next OTP will be available for the current token.

 If the returned value is greater than zero, an authenticated OTP for this token has already been validated by the backend.
 In this case, the `generateOTP` function will fail until the specified time interval has passed, as the generated OTP for this timeframe has been consumed and is no longer valid.

 If the returned value is zero or negative, the OTP can be generated immediately.

 Normally, an OTP can always be generated. However, generation may fail if an implicitly generated OTP was authenticated by the SDK internally. Since only one OTP can be authenticated for each time step, the SDK will not generate another OTP until the authenticated OTP's time step has expired. This mechanism prevents the SDK from generating an OTP that has already been consumed by the backend, which would fail authentication if reused.

 Applications can use this API when OTP generation fails with the error `_T4SDK_OTP_NOT_AVAILABLE_` to determine how much time remains until a new OTP becomes available for the token.

 @result The time interval in seconds until the next OTP is available.
 */
- (NSTimeInterval)getNextOtpAvailableTime;

/**
 Returns the date and time when the next OTP will be available for the current token.

 This function provides the exact date and time at which the next OTP can be generated.
 The returned value may be `nil` if the SDK cannot determine the next available date, indicating that the OTP can be generated immediately.

 If a non-nil date is returned, it represents the moment when the OTP generation becomes valid again for the token, based on the current SDK state and token authentication logic.

 Applications can use this API to display the next OTP availability to users or for scheduling OTP generation attempts.

 @result An NSDate object representing the moment when the next OTP will be available.
 */
- (NSDate * _Nullable)getNextOtpAvailableDate;

/** Returns the token protection level
 
 @result Token protection level
 
 @see T4PinProtection
 */
-(T4PinProtection)getProtection;

/**
 Returns the device where the token is currently active.
 
 This value is _nil_ for tokens active on current device. If a token was migrated or re-enrolled on a different device, it will contain the label of the remote enabled device for the token.
 
 @result The label of the enabled device
 */
- (NSString* _Nullable)getEnabledDevice;

/** Returns an App defined value set to the token.
 
 @param sKey key identifying the value to retrieve
 
 @result The retrieved value, or _nil_ if not found
 */
-(id _Nullable)getAppData:(NSString* _Nonnull)sKey;

/** Sets an App defined value associated to the token.
 
 The App may need to save values to be associated to tokens, for instance the position of the token in a list, or some extra status managed by the App.
 
 @param sKey  key identifying the set value
 @param value the value saved to the token
 
 @warning *Important:* save only small strings or numbers to not make the token data too big
 */
-(void)setAppData:(NSString* _Nonnull)sKey value:(id _Nonnull)value;

/**
 Removes the data corresponding to the specified key.
 
 @param sKey the key to remove
 */
-(void)removeAppData:(NSString* _Nonnull)sKey;

/** Returns the service associated with the token, or _nil_ if the service was not retrieved or if the token doesn't have an associated service.
 
 @result The corresponding service
 
 @see T4Service
 */
-(T4Service* _Nullable)getService;

/**
 ![Backend Call](connectm.png "Backend Call")  Downloads or updates the token's service from the remote server.
 If the service is present, tries to update the service on the local DB and stores the updates. Otherwise downloads and stores the service data locally.
 
 @param completion Called when the service is downloaded/updated. If successful, the downloaded/updated service is also returned
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_NO_SERVICES | 513 | Searched data wasn't found |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_INVALID_LOGIN  | 517 | User login failed |
 | T4U_MAX_DEVICES | 520 | No more devices can be registered |
 | T4U_USER_NOT_FOUND | 769 | User not found |
 | T4U_NO_DATA_CHANGED | 770 | An update request for an object have not remote updates |
 | T4U_SESSION_GOOGLE_XPIRED | 13057 | Google session expired |
 | T4U_SERVICE_NOT_FOUND | 17664 | Service not found |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 | T4SDK_INVALID_PARAMS | -24 | Invalid parameters |
 
 */
-(void)downloadService:(void (^ _Nonnull)(T4Response * _Nonnull, T4Service * _Nullable))completion;

/**
 Returns the best service identifying name the is abke to obtain
 
 First attemts to recover the label from the service associated with the token. If this is not possible, uses the service identifier in the token. If the service identifier is _nil_ returns _nil_
 
 @result The service name
 */
-(NSString* _Nullable)getServiceName;

/** ![Backend Call](connectm.png "Backend Call")  Sets a new token label and on the backend token database.
 
 @param newLabel The new label.
 @param completion The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_PIN | 787 | Invalid PIN |
 | T4E_PIN_WRONG_LENGTH | 823 | Wrong pin length |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | Token is not authenticated by a previous call to verifyProtection |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is in an invalid status, an active token is required |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
-(void)setLabel:(NSString * _Nonnull)newLabel completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** Enables or disables the begin-end algorithm for the OTP generation of the token
 
 @param enable _true_ to enable begin-end, _false_ to disable
 */
-(void)enableBE:(bool)enable;

/** ![Backend Call](connectm.png "Backend Call")  Retrieves the tokens's seed from the backend and stores in the local token.
 
 The seed is saved encrypted with the specified protection. When this operation completes, the token is not activated. To activate the configured token locally and on the backend, call Activate.
 
 @param verificationCode The verification code provided by secondary channel
 @param protection       The protection level desired for this token
 @param pin             Protection PIN in case the _protection_ parameter requires a PIN. Otherwise this parameter is ignored and _nil_ may be passed.
 @param pinErrorEvidence If the _protection_ parameter requires a PIN, _true_ sets the PIN error evidence, _false_ disables it. If the token protection is not with PIN, this parameter is ignored.
 @param completion       The completion block called when the operation is completed with the operation outcome in the T4Response
 
 @see T4PinProtection
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Service token is locked |
 | T4E_TOKEN_OUT_OF_SYNCH | 816 | Service token requires synchronization |
 | T4E_TOKEN_ALREADY_INITIALIZED | 822 | Token was already initialized |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_SVR_CODE_INVALID | -4006 | Authentication code proviced is invalid |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_WRONG_DEVICE | -4015 | Wrong device |
 | T4E_SERVICE_TOKEN_NOT_FOUND | -4016 | Service token not found |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_INVALID_SEED | -31 | Downloaded seed is invalid |
 | T4SDK_INTERNAL | -32 | An internal cryptographic operation has failed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4_ERR_LICENSE | -2017 | Specified protection is not allowed by the provided SDK license |
 
 */
-(void)getSeed:(NSString* _Nullable)verificationCode protection:(T4PinProtection)protection pin:(NSString* _Nullable)pin pinErrorEvidence:(bool)pinErrorEvidence completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Retrieves the tokens's seed from the backend and stores in the local token.
 
 The seed is saved encrypted with the specified protection. When this operation completes, the token is not activated. To activate the configured token locally and on the backend, call Activate.
 
 @param verificationCode The verification code provided by secondary channel
 @param protection       The protection level desired for this token
 @param pin              Protection PIN in case the _protection_ parameter requires a PIN. Otherwise this parameter is ignored and _nil_ may be passed.
 @param pinErrorEvidence If the _protection_ parameter requires a PIN, _true_ sets the PIN error evidence, _false_ disables it. If the token protection is not with PIN, this parameter is ignored.
 @param protectionGroup  The token is added to the specified protection group
 @param completion       The completion block called when the operation is completed with the operation outcome in the T4Response
 
 @see T4PinProtection
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Service token is locked |
 | T4E_TOKEN_OUT_OF_SYNCH | 816 | Service token requires synchronization |
 | T4E_TOKEN_ALREADY_INITIALIZED | 822 | Token was already initialized |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_SVR_CODE_INVALID | -4006 | Authentication code proviced is invalid |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_WRONG_DEVICE | -4015 | Wrong device |
 | T4E_SERVICE_TOKEN_NOT_FOUND | -4016 | Service token not found |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_INVALID_SEED | -31 | Downloaded seed is invalid |
 | T4SDK_INTERNAL | -32 | An internal cryptographic operation has failed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_INVALID_PROTECTION | -38 | Trying to add the token to a group with protection different that the token's specified seed protection |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4_ERR_LICENSE | -2017 | Specified protection is not allowed by the provided SDK license |
 
 */
-(void)getSeed:(NSString* _Nullable)verificationCode protection:(T4PinProtection)protection pin:(NSString* _Nullable)pin pinErrorEvidence:(bool)pinErrorEvidence protectionGroup:(NSInteger)protectionGroup completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call") Retrieves multiple seeds from backend with a single call.
 
 At completion an array with the successfully imported tokens is returned. If one or more token import fails, the last failed token error is returned.
 
 @param verificationCode The verification code provided by secondary channel
 @param aTokens          Tokens which seeds need to be imported
 @param protection       The protection level desired for this token
 @param sPin             Protection PIN in case the _protection_ parameter requires a PIN. Otherwise this parameter is ignored and _nil_ may be passed.
 @param pinErrorEvidence If the _protection_ parameter requires a PIN, _true_ sets the PIN error evidence, _false_ disables it. If the token protection is not with PIN, this parameter is ignored.
 @param completion       The completion block called when the operation is completed with the operation outcome in the T4Response object, and an array containing the successfully imported seed tokens.
 
 @see T4PinProtection
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Service token is locked |
 | T4E_TOKEN_OUT_OF_SYNCH | 816 | Service token requires synchronization |
 | T4E_TOKEN_ALREADY_INITIALIZED | 822 | Token was already initialized |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_SVR_CODE_INVALID | -4006 | Authentication code proviced is invalid |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_WRONG_DEVICE | -4015 | Wrong device |
 | T4E_SERVICE_TOKEN_NOT_FOUND | -4016 | Service token not found |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_INVALID_SEED | -31 | Downloaded seed is invalid |
 | T4SDK_INTERNAL | -32 | An internal cryptographic operation has failed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_INVALID_PROTECTION | -38 | Trying to add the token to a group with protection different that the token's specified seed protection |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4_ERR_LICENSE | -2017 | Specified protection is not allowed by the provided SDK license |
 
 */
+(void)getMultiSeed:(NSString* _Nullable)verificationCode tokens:(NSArray<T4Token*>* _Nonnull)aTokens protection:(T4PinProtection)protection pin:(NSString* _Nullable)sPin pinErrorEvidence:(bool)pinErrorEvidence completion:(void (^_Nonnull)(T4Response*_Nonnull,NSArray<T4Token*>*_Nullable))completion;

/** ![Backend Call](connectm.png "Backend Call")  Activates the downloaded token.
 
 Activation is required so the backend knows the token is correctly configured on the local device.
 
 @param pin        Protection PIN if the token is PIN protected
 @param antifraudEnabled if true, generateOtp API will send antifraud data to server
 @param completion  The completion block called when the operation is completed with the operation outcome in the T4Response
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Token is locked |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is in an invalid status |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
-(void)activate:(NSString* _Nullable)pin antiFraudEnabled:(BOOL)antifraudEnabled completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Activates the downloaded token.
 
 Activation is required so the backend knows the token is correctly configured on the local device.
 
 @param pin        Protection PIN if the token is PIN protected
 @param completion  The completion block called when the operation is completed with the operation outcome in the T4Response
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Token is locked |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is in an invalid status |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
-(void)activate:(NSString* _Nullable)pin completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Authenticates the token remotely.
 
 The command creates an OTP implicitely and sends it to the backend for verification.
 
 @param pin        Protection PIN if the token is PIN protected
 @param completion  The completion block called when the operation is completed with the operation outcome in the T4Response
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Token is locked |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
-(void)authenticate:(NSString* _Nullable)pin completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Authenticates the challenge using an implicit OTP generated by the token.
 
 @param pin               Protection PIN if the token is PIN protected
 @param challengeResponse The challenge to be signed by the generated OTP
 @param completion        The completion block called when the operation is completed with the operation outcome in the T4Response
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Token is locked |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 
 */
-(void)authenticate:(NSString* _Nullable)pin challengeResponse:(NSString * _Nonnull)challengeResponse completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call")  Requests the server to create a token for the client
 
 @param otpProvider     The OTP provider copnfiguration used. Specify if selected by the App. If the provider is selected by the backend, this parametes is _nil_.
 @param params          Dictionary containing token configuration parameters
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | telephoneNumber| NSString | User's telephone number. This is required only if the confirmation code will be sent to the user via SMS (token bearer = 1) |
 | email | NSString | User's email. Required if the code is sent via email (token bearer = 2) |
 | service | NSString | The service which the new token belongs to |
 | label | NSString | New token's label: a human readable string to identify the token for the user |
 
 @param completion      The completion callback. Receives a response and, if successful, a token ID
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_TOKEN_ALREADY_EXISTS | 821 | Token already exists on PkBox |
 | T4E_PIN_WRONG_LENGTH | 823 | Wrong pin length |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_SVR_DUPLICATE_TOKEN | -4007 | Duplicate token |
 | T4E_SVR_MAX_TOKEN_REACHED | -4009 | Max token allowed reached |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
+(void)initToken:(NSString* _Nullable)otpProvider params:(NSDictionary* _Nullable)params completion:(void (^ _Nonnull)(T4Response* _Nonnull,T4TokenId* _Nullable))completion;

/** ![Backend Call](connectm.png "Backend Call")  Retrieves an already created token from the backend
 
 @param identifier      The remote token identifier. An identifier, received by the backend normally through a push notification.
 @param completion      The completion callback. Receives a response and, if successful, the new token. The token is in _T4TokenStatusNotInitialized_ ans must be initialized by calling the _getSeed_ API
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_TOKEN_ALREADY_INITIALIZED | 822 | Token was already initialized |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_MAX_SESSION_REACHED | -4003 | Maximum number of open sessions was reached |
 | T4E_AUTHENTICATION_ERROR | - 4004 | Time4ID authentication error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
+(void)getToken:(NSString* _Nonnull)identifier completion:(void (^ _Nonnull)(T4Response* _Nonnull,T4Token* _Nullable))completion;

/** ![Backend Call](connectm.png "Backend Call")  Requires from the backend the confirmation code required by the enrollment process.
 
 The confirmation code will be sent by the channel specified by the _T4TokenBearer_ for the token.
 
 @param serviceTokenId The token which should generate the confirmation code. For the _service_ token to generate the code, pass _nil_ for this parameter.
 @param completion The response is successful if the confirmation code was successfully sent.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_SERVICE_TOKEN_NOT_FOUND | -4016 | Service token not found |
 
 */
-(void)pushOtp:(T4TokenId* _Nullable)serviceTokenId completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Renews an already created token.
 
 A token renewal can be necessary if for some reason the token cryptographic data (e.g. seed) becomes corrupted or no more safe.
 After a renewal, it will be necessary to download again the token seed. Indeed the backend will re-create a new seed for the token.
 
 @param completion The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_SERVICE_TOKEN_NOT_FOUND | -4016 | Service token not found |
 
 */
- (void)renew:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call")  Deletes a token on the local device and on the backend.
 
 @param tokenId     token identifier for the token to be deleted
 @param completion The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4SDK_NOT_FOUND | -23 | Token not found on local storage |
 
 */
+ (void)deleteToken:(T4TokenId* _Nonnull)tokenId completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion __attribute__((deprecated("Use the new method: `eraseTokenFromAppAndFromServer`")));

/** ![Backend Call](connectm.png "Backend Call")  Deletes a token on the local device and on the backend.
 
 @param tokenId     token identifier for the token to be deleted
 @param completion The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4SDK_NOT_FOUND | -23 | Token not found on local storage |
 
 */
+ (void)eraseTokenFromAppAndFromServer:(T4TokenId* _Nonnull)tokenId completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Synchronizes the token with the backend using two consecutive OTP values.
 
 @param pin        Protection PIN if the token is PIN protected
 @param completion The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
- (void)synchronize:(NSString* _Nullable)pin completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call")  Checks if the token is synchronized.
 
 @param completion The completion block called when the operation is completed with the operation outcome in the T4Response. The _T4TokenSyncStatus_ parameter returns the synchronization status for the token:
 
 @see T4TokenSyncStatus
 
 __Errors__
 
| Error   | Value    | Description |
|---------|--------|---------|
| T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
| T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
| T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
| T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
| T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
| T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
| T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 
*/
- (void)isSynchronized:(void (^ _Nonnull)(T4Response* _Nonnull, T4TokenSyncStatus syncStatus ))completion; //*

/** ![Backend Call](connectm.png "Backend Call")  Generates a challenge for a challenge-response type authentication
 
 @param completion The completion block called when the operation is completed with the operation outcome and in case of success with the challenge string in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 
 */
- (void)generateChallenge:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call")  Retrieves the list of pending tokens for the logged user
 
 @param completion The completion block called when the operation is completed with the operation outcome and in case of success an array of T4Token objects. To save a token of the list locally, use [T4Token save].
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_MAX_SESSION_REACHED | -4003 | Maximum number of open sessions was reached |
 | T4E_AUTHENTICATION_ERROR | - 4004 | Time4ID authentication error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 
 */
+ (void)getPendingTokenList:(void (^ _Nonnull)(T4Response* _Nonnull,NSArray* _Nullable))completion __attribute__((deprecated("Use the new method signature with type specification for the returned array: `getPendingTokens`")));

/** ![Backend Call](connectm.png "Backend Call")  Retrieves the list of pending tokens for the logged user
 
 @param completion The completion block called when the operation is completed with the operation outcome and in case of success an array of T4Token objects. To save a token of the list locally, use [T4Token save].
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_MAX_SESSION_REACHED | -4003 | Maximum number of open sessions was reached |
 | T4E_AUTHENTICATION_ERROR | - 4004 | Time4ID authentication error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 
 */
+ (void)getPendingTokens:(void (^ _Nonnull)(T4Response * _Nonnull, NSArray<T4Token *> * _Nullable))completion;

/**
 ![Backend Call](connectm.png "Backend Call")  Retrieves info about the specified token
 
 @param tokenId            Token identifier
 @param completion         completion callback for the command: returns the updated token into the resulting response
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_NOT_FOUND | -23 | Token not found on local storage |
 
 */
+(void)getTokenInfo:(T4TokenId* _Nonnull)tokenId completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/**
  ![Backend Call](connectm.png "Backend Call") Performs complete enroll for a token of the specified service.
 The API tries to enroll the first token found on backend for the logged user, belonging to the specified service. If no token is found for the service an T4E_NOT_FOUND ERROR will be returned.

 @param service The filter service. Only tokens belonging to the specified service will be considered.
 @param code Seed protection code. The protection code is required when the downloaded seed will be ciphered with a protection code. If no code is provided before the enroll operation, specify _nil_
 @param protection The desired protection for the token. See: _T4PinProtection_
 @param pin If the _protection_ parameter specifies a level of protection requiring a PIN, specify the token seed PIN. Otherwise set this parameter to _nil_
 @param errorEvidence If the _protection_ parameter specifies a level of protection requiring a PIN, _true_ provides a local PIN error evicence, but lows the seed protection level considerably. If in doubt specify _false_. This parameter is ignored if a PIN protection is not specified.
 @param completion Returns the operation outcome and, if auccessful, the enrolled token.
 */
+ (void)enrollForService:(NSString* _Nullable )service code:(NSString* _Nullable)code protection:(T4PinProtection)protection pin:(NSString* _Nullable )pin pinErrorEvidence:(bool)errorEvidence completion:(void (^_Nonnull)(T4Response* _Nonnull,T4Token* _Nullable))completion;

/**
 ![Backend Call](connectm.png "Backend Call")  Updates status of the local tokens with their backend counterparts.
 
 @param completion The completion block receives a T4Response containg the operation outcome. The returned array is a list of T4Token objects which have been updated. Token which have been disabled have the _newdev_ parameter in the token's _appData_ set to the name of the new device where the token was enabled if the token was enabled on a new device.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 
 */
+ (void)updateStatusForAll:(void (^ _Nonnull)(T4Response* _Nonnull,NSArray* _Nullable))completion; //*

/** ![Backend Call](connectm.png "Backend Call")  Updates the token status
 
 If the operation is successful the token is updated with new status.
 
 @param completion Completion block receiving the operation outcome and the status information. The response dictionary contains a flag _isUpdated_. If the flag is _TRUE_, the token status is updated, otherwise it is unchanged.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 
 */
- (void)updateStatus:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/**
 Update the local token with the remote notification status info.
 
 Using data into the remote notification, find a corresponding local token and update its status and the new token device in case the token was disabled to be enabled on another device. In this case the new device description is put in the appData
 
 @param remoteNotification the remote notification dictionary. Should have the following structure:
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | tokenid | NSString | Internal identifier of the updated token |
 | status | NSInteger | The new token status (_T4TokenStatus_) |
 | newdev | NSString | OPTIONAL: In case the new token status is T4TokenStatusDisabled, this may contain a description of the device where the token service was activated. |
 
 @result The modified token, or _nil_ if no local token was updated
 */
+(T4Token* _Nullable)updateWithRemoteNotification:(NSDictionary* _Nonnull)remoteNotification;

// Google:

/** Gets the information of the new token to be created from the scanned QR code, and creates the token structure.
 
 After creating the token, the token must be completed using the _completeWithGoogleAuthSecret_
 
 @param qrCodeUri string containing the read QRCode information
 @param completion Completion block receiving the operation outcome and the created token.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_NO_USER | -16 | No logged user into the SDK |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_INVALID_QRCODE | -43 | The provided QRCode content is invalid |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
+ (void)initializeTokenWithGoogleAuthQRCode:(NSString* _Nonnull)qrCodeUri completion:(void (^ _Nonnull)(T4Response* _Nonnull,T4Token* _Nullable))completion; //*

/**
 Sets the message that will be displayed when a TouchId is required
 
 @param message message to be displayed
 */
+(void)setTouchIdMessage:(NSString* _Nonnull)message;

/**
 Completes google authenticator token generation by setting the seed in the internal DB and by initializing the token.
 
 The completed token is already active and ready to use.
 
 @param qrCodeUri string containing the read QRCode information
 @param protection the type of protection required for the token
 @param pin the security PIN of the token, if the token is PIN protected
 @param pinErrorEvidence PIN error evidence
 @param completion Completion block receiving the operation outcome
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_NO_USER | -16 | No logged user into the SDK |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_INVALID_QRCODE | -43 | The provided QRCode content is invalid |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
-(void)completeWithGoogleAuthQRCode:(NSString* _Nonnull)qrCodeUri protection:(T4PinProtection)protection pin:(NSString* _Nullable)pin pinErrorEvicence:(bool)pinErrorEvidence completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

// OTP Generation API!

/** Creates the OTP
 
 @param pin The OTP PIN. This can be <i>nil</i> if the token is not PIN protected
 @param completion The asynchronous block called when the Otp generation is completed. The block receives the T4Otp structure and an error code.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_INVALID_PIN | -11 | Provided PIN with error evidence is invalid |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Touch ID is not supported by this device |
 | T4SDK_TOUCHID_AUTH_FAILED | -13 | Touch ID authentication failed |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_SVR_HALFKEY | -33 | Server halfkey was not downloaded for the implicit token protection |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_PIN_REQUIRED | -44 | PIN is required by the token seed protection |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @see T4Otp
 */
- (void)generateOtp:(NSString * _Nullable)pin completion:(void (^ _Nonnull)(T4Otp* _Nullable,T4Error))completion;

/** Creates the OTP
 
 @param pin The OTP PIN. This can be <i>nil</i> if the token is not PIN protected
 @param antiFraudEnabled if true, generateOtp API will send antifraud data to server
 @param completion The asynchronous block called when the Otp generation is completed. The block receives the T4Otp structure and an error code.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_INVALID_PIN | -11 | Provided PIN with error evidence is invalid |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Touch ID is not supported by this device |
 | T4SDK_TOUCHID_AUTH_FAILED | -13 | Touch ID authentication failed |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_SVR_HALFKEY | -33 | Server halfkey was not downloaded for the implicit token protection |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_PIN_REQUIRED | -44 | PIN is required by the token seed protection |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @see T4Otp
 */
- (void)generateOtp:(NSString * _Nullable)pin antiFraudEnabled:(BOOL)antiFraudEnabled completion:(void (^ _Nonnull)(T4Otp* _Nullable,T4Error))completion;


/**
 Creates the OTP on an already authenticated token.
 This generation command is synchronous, but fails if the required token is not authenticated.
 
 @param removeAuthentication If _YES_ the authentication on the token is removed after generating the OTP. Further calls to this API will fail.
 @param error T4Error structure with the error code.
 
 @result The generated T4Otp structure
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | -34 | Token was not authenticated using verifyProtection |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @see T4Otp
 */
-(T4Otp* _Nullable)generateVerifiedOtp:(bool)removeAuthentication error:(T4Error* _Nonnull)error;

/**
 Creates the OTP on an already authenticated token.
 This generation command is synchronous, but fails if the required token is not authenticated.
 
 @param removeAuthentication If _YES_ the authentication on the token is removed after generating the OTP. Further calls to this API will fail.
 @param antiFraudEnabled if true, generateOtp API will send antifraud data to server
@param error T4Error structure with the error code.
 
 @result The generated T4Otp structure
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | -34 | Token was not authenticated using verifyProtection |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |

 
 @see T4Otp
 */
-(T4Otp* _Nullable)generateVerifiedOtp:(bool)removeAuthentication antiFraudEnabled:(BOOL)antiFraudEnabled error:(T4Error* _Nonnull)error;

/** Generates an OTP using the OCRA algorithm based on a server receive challenge and the token's seed.
 
 Is used to authenticate transactions. Normally the challenge contains a transaction identifier.
 
 @param serverChallenge remote challenge received
 @param pin The OTP PIN. This can be <i>nil</i> if the token is protected with T4PPNone
 @param completion The asynchronous block called when the Otp generation is completed. The block receives the T4Otp structure and an error code.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_INVALID_PIN | -11 | Provided PIN with error evidence is invalid |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Touch ID is not supported by this device |
 | T4SDK_TOUCHID_AUTH_FAILED | -13 | Touch ID authentication failed |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_SVR_HALFKEY | -33 | Server halfkey was not downloaded for the implicit token protection |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_PIN_REQUIRED | -44 | PIN is required by the token seed protection |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @see T4Otp
 */
- (void)generateOtpChallenge:(NSString * _Nonnull)serverChallenge pin:(NSString * _Nullable)pin completion:(void (^ _Nonnull)(T4Otp* _Nullable,NSInteger))completion;

/** Generates an OTP using the OCRA algorithm based on a server receive challenge and the token's seed.
 
 Is used to authenticate transactions. Normally the challenge contains a transaction identifier.
 
 @param serverChallenge remote challenge received
 @param pin The OTP PIN. This can be <i>nil</i> if the token is protected with T4PPNone
 @param antiFraudEnabled if true, generateOtp API will send antifraud data to server
 @param completion The asynchronous block called when the Otp generation is completed. The block receives the T4Otp structure and an error code.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_INVALID_PIN | -11 | Provided PIN with error evidence is invalid |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Touch ID is not supported by this device |
 | T4SDK_TOUCHID_AUTH_FAILED | -13 | Touch ID authentication failed |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_SVR_HALFKEY | -33 | Server halfkey was not downloaded for the implicit token protection |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_PIN_REQUIRED | -44 | PIN is required by the token seed protection |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |

 @see T4Otp
 */
- (void)generateOtpChallenge:(NSString * _Nonnull)serverChallenge pin:(NSString * _Nullable)pin antiFraudEnabled:(BOOL)antiFraudEnabled completion:(void (^ _Nonnull)(T4Otp* _Nullable,NSInteger))completion;

/**
 Generates an OTP using the OCRA algorithm based on a server receive challenge and the token's seed.
 Creates the OTP on an already authenticated token.
 This generation command is synchronous, but fails if the required token is not authenticated.
 
 @param serverChallenge remote challenge received
 @param removeAuthentication If _YES_ the authentication on the token is removed after generating the OTP. Further calls to this API will fail.
 @param antiFraudEnabled if true, generateOtp API will send antifraud data to server
 @param error T4Error error code.
 
 @result The generated T4Otp structure
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | -34 | Token was not authenticated using verifyProtection |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @see T4Otp
 */
-(T4Otp* _Nullable)generateVerifiedOtpChallenge:(NSString * _Nonnull)serverChallenge removeAuthentication:(bool)removeAuthentication antiFraudEnabled:(BOOL)antiFraudEnabled error:(T4Error* _Nullable)error;

/** ![Backend Call](connectm.png "Backend Call")  Authenticates the token locally.
 
 Calling this API is required to perform: _generateVerifiedOtp_, _completeChangeProtection_, _addToProtectionGroup_ and _removeFromProtectionGroup_
 If the token is PIN protected and _checkPin_ is set on _true_, a remote PIN check with the server will be performed.
 
 @param pin     Protection PIN if the token is PIN protected. Otherwise this parameter is ignored and can be _nil_
 @param checkPin Performs an authenticate with the generated OTP on the backend in case the protection requires a PIN and the PIN error evidence is not set. This is strongly suggested if the verifyProtection is used to perform changes on the token, because a wrong PIN will corrupt the token seed.
 @param authSeconds Time for past verification to be kept valid. If the token was verifired within _authSeconds_ seconds, another verification will not be performed, and the token will still be treated as verified. Otherwise the token verification will be required. If _authSeconds_ is _0_, the verification will ALWAYS be performed.
 @param completion Completion block receiving the operation outcome.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_OTP | 808 | Invalid OTP |
 | T4E_TOKEN_LOCKED | 809 | Token is locked |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_INVALID_PIN | -11 | Provided PIN with error evidence is invalid |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Touch ID is not supported by this device |
 | T4SDK_TOUCHID_AUTH_FAILED | -13 | Touch ID authentication failed |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_SVR_HALFKEY | -33 | Server halfkey was not downloaded for the implicit token protection |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_OTP_NOT_AVAILABLE | -41 | OTP is not available for this time window |
 | T4SDK_PIN_REQUIRED | -44 | PIN is required by the token seed protection |
 
 */
- (void)verifyProtection:(NSString* _Nullable)pin checkPin:(bool)checkPin authSeconds:(NSTimeInterval)authSeconds completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/**
 _true_ id the token is authenticated via __verifyprotection__ API. If the token is not authenticated it returns _false_

 @return the token authentication status
 
 @see verifyProtection
 */
- (bool)isAuthenticated;


/** Completes a local protection change ( for instance change PIN ).
 
 Before successfully calling this API, the token must be authenticated by calling _verifyProtection_.
 
 @param newProtection    New protection level for the token
 @param newPin           new protection PIN if the protection level is T4PPNumeric or T4PPAlphanumeric. Othewise this parameter is ignored and can be _nil_.
 @param pinErrorEvidence PIN error evidence.
 @param completion       The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is not active |
 | T4SDK_GENERATION_INTERNAL | -14 | Internal OTP generation error |
 | T4SDK_TOKEN_LOCKED | -15 | Token is locked |
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | -34 | Token was not authenticated using verifyProtection |
 | T4SDK_INVALID_PROTECTION | -38 | The protection level specified is lower than the minimum required |
 | T4SDK_PIN_TOO_SHORT | -39 | The specified PIN is shorter than the minimum required |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @warning *Note:* If the token changing protection belongs to a protection group, ALL the tokens in that protection group will change their protection to the specified one.
 */
- (void)changeProtection:(T4PinProtection)newProtection pin:(NSString* _Nullable)newPin pinErroEvidence:(bool)pinErrorEvidence completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/** Returns the token protection group
 
 @result The token protection group. If 0 is returned, the token is not part of any protection group
 */
- (NSInteger)getProtectionGroup;

/** Adds the token to the specified protection group.
 
 Before successfully calling this API, the token must be authenticated by calling _verifyProtection_.
 
 @param protectionGroup The protection group where to add the token.
 @param groupPin        PIN of the protection group if it is PIN protected, otherwise is ignored and can be _nil_
 @param completion      The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_INVALID_PARAMS | -24 | Token is already part of the group, or the group specified is invalid !
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | -34 | Token was not authenticated using verifyProtection |
 | T4SDK_INVALID_PROTECTION | -38 | The protection level specified is lower than the minimum required |
 | T4SDK_PIN_TOO_SHORT | -39 | The specified PIN is shorter than the minimum required |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 @warning *Note:* If there are no other tokens in the protection group, the token will be automatically added to the protection group using the current protection protection level. In case of PIN protection active, the specified PIN will be set and the current PIN error evidence value will be used. In case there are other tokens in the protection group, the protection set for the token will be the same required by the other protection group members. In this case, if the protection group is PIN protected, the specified PIN _MUST_ be the PIN protecting the members of the protection group. If the token was in a different protection group, it will be moved to the new protection group.
 */
- (void)addToProtectionGroup:(NSInteger)protectionGroup pin:(NSString* _Nullable)groupPin completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** Removes the token from its protection group.
 
 Before successfully calling this API, the token must be authenticated by calling _verifyProtection_.
 
 @param completion The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_INTERNAL | -32 | Internal SDK error |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | -34 | Token was not authenticated using verifyProtection |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
- (void)removeFromProtectionGroup:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** Removes authentication from the token if authenticated

 */
- (void)removeAuthentication;

-(bool)isUploadRequired;

/**
 Uploads an OATH token on the Time4ID backend

 @param removeAuthentication Remova authentication from token after exporting
 @param completion completion callback
 */
-(void)uploadVerifiedGoogleToken:(bool)removeAuthentication completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/**
 Aligns moving factor on the backend with local OATH event-based tokens

 @param completion completion callback
 */
+(void)updateGoogleTokensMovingFactor:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

// Temporary!
-(NSString* _Nullable)getServiceString;
-(NSString* _Nullable)getCtxNode; // for Push Authentication

// Debug purpose:
+(void)lastGenerationInfoEnable:(bool)bEnable wheel:(NSInteger)wheel;
+(NSArray<NSDictionary*>* _Nullable)getLastGenerationInfo;

-(void)generateAuthenticationTicketWithPin:(NSString* _Nullable)pin completion:(void (^ _Nonnull)(NSString* _Nullable authenticationTicket, NSError* _Nullable error))completion;
-(T4Otp* _Nullable)generateOtpForAuthenticationTicket:(NSString* _Nonnull)ticket options:(T4OtpTicketOptions)options error:(NSError* _Nullable * _Nullable)error;
-(T4Otp* _Nullable)generateOtpChallenge:(NSString * _Nonnull)serverChallenge forAuthenticationTicket:(NSString* _Nullable)ticket options:(T4OtpTicketOptions)options error:(NSError* _Nullable * _Nullable)error;
-(void)deleteAuthenticationTicket:(NSString* _Nonnull)ticket;
@end
