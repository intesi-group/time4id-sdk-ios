//
//  NSData_Conversion.h
//  time4eid
//
//  Created by David Beduzzi on 04/07/14.
//  Copyright (c) 2014 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString* hexStringFromData( NSData* data, bool bUppercase );
NSData* dataFromHexString(NSString* hexString );
