//
//  T4DeviceInfo.h
//  Time4ID-Core
//
//  Created by francesco scalise on 13/12/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface T4DeviceInfo : NSObject

+(NSDictionary*)getDeviceInfo;

@end

NS_ASSUME_NONNULL_END
