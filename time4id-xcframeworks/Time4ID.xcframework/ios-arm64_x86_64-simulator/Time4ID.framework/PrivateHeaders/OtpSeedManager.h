//
//  OtpSeedManager.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/2/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/T4Response.h>
#import <Time4ID/BaseManagerWithOtpAccount.h>
#import <Time4ID/OtpSeedCore.h>
#import <Time4ID/HardeningManagerCore.h>
#import <Time4ID/DatabaseManagerCore.h>

// Otp configuration flags:
#define OTP_CONF_ENABLEDEVICEID                 0x0001
#define OTP_CONF_ENABLERAWOTP                   0x0002
#define OTP_CONF_DISABLEWRONGPINNOTIFICATION    0x0004
#define OTP_CONF_FIXEDMOVINGFACTOR              0x0200

#define T4PPAll (T4PPNone|T4PPNumeric|T4PPAlfanumeric|T4PPBiometric)

#define OTP_FLAG_SET(field,flag,status) ((status)?(field|=(flag)):(field&=~((int)(flag))))

#define ALG_MASK                ((int)(7<<6))
#define OTP_ALG_GET(field)      (((field)>>6)&7)
#define OTP_ALG_SET(field,alg)  field=(int)(((int)(field))&(~ALG_MASK))|((alg)<<6)

/**
    Allows to set and manage the seed for the current token
 */
@interface OtpSeedManager : BaseManagerWithOtpAccount

/**
 Generates a seed inside the
 
    @param paramsArray      array of params used to generate the seed
    @param pin              PIN protecting the generated seed
    @param pinProtection    The token PIN protection level
    @param pinErrorEvidence Token PIN error evidence
 
    @return <i>true</i> if the seed was successfully created, otherwise </false>.
 */
- (BOOL)generateSeedWithParams:(NSArray *)paramsArray pin:(NSString *)pin inProtection:(T4PinProtection)pinProtection pinErrorEvidence:(bool)pinErrorEvidence;

/**
 Returns the deciphered seed for the current token. <b>If the seed is PIN protected without error evidence and a wrong PIN is passed as pin, the function returns correctly, but the seed is corrupted. To be certain the seed is correct, in this case verify the PIN with an implicit authentication operation!</b>.
 
    @param pin The PIN protecting the seed if it is PIN protected, otherwise <i>nil</i>.
 
    @return structure containing the deciphered seed.
 
    @see OtpSeedCore
 */
- (OtpSeedCore *)getDecipheredSeedWithPin:(NSString *)pin;

/**
 Increments the moving factor for the current token if it is event based.
 
    @return <i>true</i> if the seed was successfully created, otherwise </false>.
 */
- (BOOL)incrementMovingFactor;

/**
 Sets the seed for the current token.<br>
    <i><font color="red">deprecated</font></i>: user setSeedEx to set the PIN protection and PIN error evidence at the same time of seed ciphering
 
    @param seed new seed to be set for the token
    @param pin  token PIN if pin protection is present. Otherways is ignored.
 
    @return <i>true</i> if the seed was successfully set, otherwise </false>.
 
    @see OtpSeedCore
 */
- (BOOL)setSeed:(OtpSeedCore *)seed pin:(NSString *)pin  __deprecated;

/**
 Sets the seed for the current token.
 
    @param seed             new seed to be set for the token.
    @param pin  token PIN if pin protection is present. Otherways is ignored.
    @param pinProtection    token PIN protection level.
    @param pinErrorEvidence token PIN error evidence.
 
    @return <i>true</i> if the seed was successfully set, otherwise </false>.
 
    @see T4PinProtection
    @see OtpSeedCore
 */
- (BOOL)setSeedEx:(OtpSeedCore *)seed pin:(NSString *)pin pinProtection:(T4PinProtection)pinProtection pinErrorEvidence:(bool)pinErrorEvidence;

/**
 Sets the seed for the current token.
 
 @param seed             new seed to be set for the token.
 @param pin  token PIN if pin protection is present. Otherways is ignored.
 @param pinProtection    token PIN protection level.
 @param pinErrorEvidence token PIN error evidence.
 @param otpAccountId     the AccountId for the specified token
 
 @return <i>true</i> if the seed was successfully set, otherwise </false>.
 
 @see T4PinProtection
 @see OtpSeedCore
 */
- (BOOL)setSeedEx:(OtpSeedCore *)seed pin:(NSString *)pin pinProtection:(T4PinProtection)pinProtection pinErrorEvidence:(bool)pinErrorEvidence otpAccountId:(OtpAccountIdCore*)otpAccountId;

@end
