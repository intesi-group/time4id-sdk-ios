//
//  Time4ID.h
//  Time4ID-Core
//
//  Created by francesco scalise on 12/12/22.
//

#import <Foundation/Foundation.h>

//! Project version number for Time4ID.
FOUNDATION_EXPORT double Time4IDVersionNumber;

//! Project version string for Time4ID.
FOUNDATION_EXPORT const unsigned char Time4IDVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Time4ID/PublicHeader.h>

#import <Time4ID/T4Biometrics.h>
#import <Time4ID/T4Credential.h>
#import <Time4ID/T4DeviceInfo.h>
#import <Time4ID/T4PushAuthentication.h>
#import <Time4ID/T4PushSignatureDocAuth.h>
#import <Time4ID/T4PushSignatureDocument.h>
#import <Time4ID/T4PushSignatureInfo.h>
#import <Time4ID/T4Registration.h>
#import <Time4ID/T4Response.h>
#import <Time4ID/T4Service.h>
#import <Time4ID/RCertificates.h>
#import <Time4ID/T4ID.h>
#import <Time4ID/T4LicenseManager.h>
#import <Time4ID/T4Otp.h>
#import <Time4ID/T4Token.h>
#import <Time4ID/T4TokenId.h>
#import <Time4ID/T4User.h>
#import <Time4ID/T4TokenStore.h>
#import <Time4ID/Base64Util.h>

/*
#import <Time4ID/VRao.h>
#import <Time4ID/VRRecognition.h>
#import <Time4ID/VRRecognitionItem.h>
*/

/** LEGACY */
/*
#import <Time4ID/Reachability.h>
#import <Time4ID/OtpAccountIdCore.h>
*/

