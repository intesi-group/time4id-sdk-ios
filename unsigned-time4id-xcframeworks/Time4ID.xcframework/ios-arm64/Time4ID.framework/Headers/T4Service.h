//
//  T4Service.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 15/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Time4ID/T4Response.h>

/**
 Represents a Strong Authentication service description.
 Contains:
 
  - Service security informations (like the minimum required PIN size for instance)
  - Graphical assets (background color, service icon)
  - Descriptive informations (User-friendly service label, description, service provider url ...)
 */
@interface T4Service : NSObject

/** ![Backend Call](connectm.png "Backend Call")  Updates all services bound to local tokens from the backend
 
 @param completion Callback called when the services update is completed. The returned _T4Response_ contains the operation result. If not all services updates are successful, the returning dictionary will contain errors for all the services that failed to update. Id __ALL__ the services failed, the command will fail with error.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_REMOTE_UPDATE | -40 | Update for all services has failed |
 
 @see T4Response
 */
+(void) updateAllServices:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

+(T4Service* _Nonnull)customService:(NSString* _Nonnull)service ctxNode:(NSString* _Nonnull)ctxNode domain:(NSString* _Nullable)domain description:(NSString* _Nullable)description icon:(UIImage* _Nonnull)icon color:(UIColor* _Nonnull)color companyUrl:(NSString* _Nullable)companyUrl label:(NSString* _Nonnull)label minProtection:(T4PinProtection)minProtection minPINLength:(NSInteger)minPINLength otpImage:(NSData* _Nullable)otpImage;

// Getters:

/** Returns the service domain
 
 @return The domain for the service
 */
-(NSString* _Nullable)getDomain;

/** Returns a description for the service.
 
 The service description is a free text of any length that may describe the service use or anything else...
 
 @return The service description
 */
-(NSString* _Nullable)getDescription;

/**
 Returns the service Icon.
 
 The service icon can be used by the App for a graphical representation of the service
 
 @return The service icon
 */
-(UIImage* _Nullable)getIcon;

/** Returns the background color.
 
 The Service background color can be used for graphical decorative purposes.
 
 @return Service color
 */
-(UIColor* _Nullable)getColor;

/** Service company URL.
 
 An URL provided by the service company. The App can use it to publish a link to the service company or service use details.
 
 @return The service company URL
 */
-(NSString* _Nullable)getCompanyUrl;

/** Returns the service label.
 
 The service label is a user-friendly service name the App can use to identify the service on its interface.
 
 @return The service label
 */
-(NSString* _Nullable)getLabel;

/** Returns the minimum token protection required by the service provider.
 
 The SDK operations that set the token protection will fail if trying to set a protection lower than specified in this parameter.
 
 @return The required protection.
 
 @see T4PinProtection
 */
-(T4PinProtection)getMinRequiredProtection;

/** If the TouchID is available on the device, this will be enforced on the service's tokens.
 
 @return _true_ if touchId is required, otherwise _false_.
 */
-(bool)isTouchIdRequired;

/** Returns the minimum PIN length required by the service provider.
 
 If the token is PIN protected, the SDK will return an error when trying to protect the token with a PIN shorter than the minimum PIN length.
 
 @return The minimum PIN length allowed.
 */
-(NSInteger)getMinPinLength;

/** Returns the image associated with the service OTP, or _nil_ if such image was not provided
 
 The image is in SVG format.
 
 @return The binary encoded image.
 */
-(NSData* _Nullable)geOtpImage;

@end
