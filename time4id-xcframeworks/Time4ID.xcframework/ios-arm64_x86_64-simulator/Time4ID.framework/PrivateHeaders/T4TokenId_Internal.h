//
//  T4TokenId_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 11/03/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef T4TokenId_Internal_h
#define T4TokenId_Internal_h


#endif /* T4TokenId_Internal_h */

@interface T4TokenId (LibraryInternalMethods)

-(NSMutableDictionary*)addToDictionary:(NSMutableDictionary*)dictionary;
- (BOOL)isEqualToTokenId:(T4TokenId *)tokenId;

@end
