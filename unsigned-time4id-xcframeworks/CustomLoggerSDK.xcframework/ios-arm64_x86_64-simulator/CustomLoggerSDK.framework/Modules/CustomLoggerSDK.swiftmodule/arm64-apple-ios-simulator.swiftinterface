// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.10 (swiftlang-5.10.0.13 clang-1500.3.9.4)
// swift-module-flags: -target arm64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name CustomLoggerSDK
@_exported import CustomLoggerSDK
import Foundation
import Swift
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
@_exported import os.log
@_exported import os
public class CustomLogger {
  public var oslog: os.OSLog {
    get
  }
  public var logLevel: CustomLoggerSDK.CustomLogLevel
  public var isSystemLogEnabled: Swift.Bool
  public init(_ bundle: Foundation.Bundle = .main, category: Swift.String? = nil)
  public init(_ aClass: Swift.AnyClass, category: Swift.String? = nil)
  @objc deinit
}
extension CustomLoggerSDK.CustomLogger {
  public func disable()
}
extension CustomLoggerSDK.CustomLogger {
  @inlinable public func debug(_ message: Swift.String) {
        guard logLevel >= .debug else { return }

        oslog.debug(message)
    }
  @inlinable public func info(_ message: Swift.String) {
        guard logLevel >= .info else { return }

        oslog.info(message)
    }
  @inlinable public func error(_ message: Swift.String) {
        guard logLevel >= .error else { return }

        oslog.error(message)
    }
  @inlinable public func fault(_ message: Swift.String) {
        guard logLevel >= .fault else { return }

        oslog.fault(message)
    }
  public func print(_ value: @autoclosure () -> Any)
  public func dump(_ value: @autoclosure () -> Any)
  public func trace()
}
extension os.OSLog {
  @inlinable internal func debug(_ message: Swift.String) {
        debug("%{private}@", message)
    }
  @inlinable internal func info(_ message: Swift.String) {
        let m = message
        info("%{public}@", m)
    }
  @inlinable internal func error(_ message: Swift.String) {
        let m = message
        error("%{public}@", m)
    }
  @inlinable internal func fault(_ message: Swift.String) {
        let m = message
        let icon = ""
        fault("%{public}@", m)
        fault("%{public}@%{public}@", icon, Thread.callStackSymbols)
    }
}
extension os.OSLog {
  @usableFromInline
  internal func debug(_ message: Swift.StaticString, _ args: any Swift.CVarArg...)
  @usableFromInline
  internal func info(_ message: Swift.StaticString, _ args: any Swift.CVarArg...)
  @usableFromInline
  internal func error(_ message: Swift.StaticString, _ args: any Swift.CVarArg...)
  @usableFromInline
  internal func fault(_ message: Swift.StaticString, _ args: any Swift.CVarArg...)
  @usableFromInline
  internal func log(_ message: Swift.StaticString, type: os.OSLogType, _ a: [any Swift.CVarArg])
}
public enum CustomLogLevel : Swift.Comparable {
  case fault
  case error
  case info
  case debug
  public var rawValue: Swift.String {
    get
  }
  public static func == (a: CustomLoggerSDK.CustomLogLevel, b: CustomLoggerSDK.CustomLogLevel) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public static func < (a: CustomLoggerSDK.CustomLogLevel, b: CustomLoggerSDK.CustomLogLevel) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
}
@objc public enum Objc_CustomLogLevel : Swift.Int {
  case fault
  case error
  case info
  case debug
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc final public class Objc_CustomLogger : ObjectiveC.NSObject {
  @objc final public var logLevel: CustomLoggerSDK.Objc_CustomLogLevel {
    @objc get
    @objc set
  }
  @objc final public var isSystemLogEnabled: Swift.Bool {
    @objc get
    @objc set
  }
  @objc public init(bundle: Foundation.Bundle = .main, category: Swift.String?)
  @objc public init(aClass: Swift.AnyClass, category: Swift.String?)
  @objc final public func debug(_ message: Swift.String)
  @objc final public func info(_ message: Swift.String)
  @objc final public func error(_ message: Swift.String)
  @objc final public func fault(_ message: Swift.String)
  @objc final public func print(_ value: @autoclosure () -> Any)
  @objc final public func dump(_ value: @autoclosure () -> Any)
  @objc final public func trace()
  @objc final public func disable()
  @objc deinit
}
extension CustomLoggerSDK.CustomLogLevel : Swift.Hashable {}
extension CustomLoggerSDK.Objc_CustomLogLevel : Swift.Equatable {}
extension CustomLoggerSDK.Objc_CustomLogLevel : Swift.Hashable {}
extension CustomLoggerSDK.Objc_CustomLogLevel : Swift.RawRepresentable {}
