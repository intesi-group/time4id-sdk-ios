//
//  T4Service_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 15/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

/**
 Internal methods
 */
@interface T4Service (LibraryInternalMethods)

// Retrieves the service inline
+(T4Service* _Nullable)getService:(NSString* _Nullable)service ctxNode:(NSString* _Nonnull)ctxNode;

// Downloads the service from remote
+(void) downloadService:(NSString* _Nullable)service ctxNode:(NSString* _Nonnull)ctxNode cmdId:(NSUInteger)cmdId completion:(void (^ _Nonnull)(T4Response* _Nonnull,T4Service* _Nullable))completion;

// Service comparer
- (BOOL)isEqualToService:(T4Service * _Nonnull)t4service;

@end
