//
//  T4ID.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 11/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Time4ID/T4Response.h>
#import <Time4ID/T4User.h>
#import <Time4ID/T4Token.h>

/** Callback passed by the SDK to the login renewal callback.
 
 @param bAbortOperation _true_ if the login renewal operation was aborted or cancelled by user: the operation that required the login renewal will fail. _false_ to continue: the SDK will try to complete the operation that required the login renewal.
 */
typedef void (^ ContinuationCallback)(bool bAbortOperation);

/**
 The login renewal callback.
 
 Set the login renewal callback to allow the SDK to automatically renew the user's login if the session is expired. If the SDK requires a remote operation and it fails to renew the session internally, it will call the specified LoginRenewalFunction callback, if it was set. The LoginRenewalFunction should:
 
 - Launch an App defined login controller, which allows the user to complete the login process
 - After the login is completed, call the _continuationCallback_ passed by the SDK with the abort operation configured.
 
 If the operation is aborting, the SDK will return cancelling the operation. If the operation is continuing, the SDK will try to re-submit the operation.
 
 @param username             The username for the user logged at the moment the operation was tried. Beacuse the operation was submitted on the behalf of this user, the App _SHOULD_ try to login the specified user, and abort the operation if this is not possible.
 @param loginType            Login type for the current user specified by _username_. Beacuse the operation was submitted on the behalf of this user, the App _SHOULD_ try to login with the specified login type, and abort the operation if this is not possible.
 @param continuationCallback Call this in your code after retrying the login. The SDK will 
 
 @see setLoginRenewalFunction for a sample implementation
 
 @warning *Note:* Make sure you always call the continuationCallback after the login retry is completed
 */
typedef void (^ LoginRenewalFunction)(NSString* _Nullable username, T4LoginType loginType, ContinuationCallback _Nonnull continuationCallback );

/** OAUTH renewal callback for Google login.
 
 If the APP uses Google login, the SDK allows to transparently renew Google OAUTH parameters by implementing this.
 
 @param continuationCallback Call this after the OAUTH renewal.
 
 @see setGoogleOauthFunction for the sandard implementation.
 
 @warning *Note:* Make sure you always call the continuationCallback after the OAUTH renewal is completed
 */
typedef void (^ GoogleOauthFunction)( ContinuationCallback _Nonnull continuationCallback );

/** Function called at SDK startup, if an URL is retrieved from the pasteboard.
 
 Implementing this callback can be used if a workflow wants to start the app using particular data.
 
 @param url the URL retrieved from the pasteboard. If at the SDK startup the pasteboard doesn't contain an URL, this callback won't get called
 
 */
typedef bool (^ StartupUrlFunction)( NSURL* _Nullable url );

@interface T4ID : NSObject

/** Returns the current library version
 
 @return The Time4ID SDK library version
 */
+(NSString* _Nonnull)getLibraryVersion;

/** Configures migration parameters.
 
 Call this api __before__ calling initWithLicense or initWithLicenseFile to specify that a migration should be
 made from previous SDK versions (1.x.x) and parameters about the migration process.
 The migration process, once activated calling this API will:

 - Import CID, CIA and SALT
 - Import the logged user info (including OAUTH parameters)
 - Import the security tokens from the 1.x SQLite3 local database specified
 - Cleanup the DB if required by the given flag
 
 @param sMigrationDB     The SQLite3 Database containing the tokens to import
 @param bDestroyWhenDone This flag tells the API to cleanup all the 'old' data after a successful migration
 */
+(void)migrateOnLibraryInitializationWithSQLiteDB:(NSString* _Nonnull)sMigrationDB destroyWhenDone:(bool)bDestroyWhenDone;

/** Configures migration parameters.
 
 Call this api __before__ calling initWithLicense or initWithLicenseFile to specify that a migration should be
 made from shared token to private token
 
 @param isNeeded  start flag
 */
//+(void)migrateFromSharedToRealmDbIfNeeded:(bool)isNeeded;

/** Initializes the Time4ID SDK with the license provided by Intesi Group
 
 Sample code for SDK initialization. In this sample the licence file provided is included in the App's resources
 
    // Retrieve license from local resource:
    NSString* sLicensePath = [[NSBundle mainBundle] pathForResource:@"t4id_license.txt" ofType:@"p7m"];
    NSData* dLicense = [[NSData alloc] initWithContentsOfFile:sLicensePath];

    // Initialize SDK with license:
    T4Error error = [T4ID initWithLicense:dLicense sharedKeychain:@"T3BDF4PRXW.com.intesigroup.time4id.Time4IDSDK"];
    if( error != T4_OK )
    {
        T4DLog(@"Invalid license");
    }
    else
    {
        T4DLog(@"Time4ID SDK %@ initialized!", [T4ID getLibraryVersion]);
    }
 
 @param license The license data
 @param sharedKeychain If a shared keychain is used to store token, the shared keychain specified in the _entitlements_ file must be passed her. If the shared keychain is not used, pass _nil_ can be passed.
 
 @return T4_OK if successful, otherwise an error code
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_SQLITE_OPEN_FAILED | -20 | Failed to open SQLite DB when migrating from 1.x SDK |
 | T4SDK_MIGRATION_FAILED | -21 | Migration from SDK 1.x failed |
 | T4_ERR_LICENSE | -2017 | Invalid SDK license |
 
 @warning *Important:* Do not use the shared keychain specified in the SDK initialization for any App shared data! Use other shared keychains if Apps reqwuire to share additional data.
 */
+(T4Error)initWithLicense:(NSData* _Nonnull)license sharedKeychain:(NSString* _Nullable)sharedKeychain;

/** Initializes the Time4ID SDK with the license provided by Intesi Group
 
 Sample code for SDK initialization. In this sample the licence file provided is included in the App's resources
 
    // Retrieve license from local resource:
    NSString* sLicensePath = [[NSBundle mainBundle] pathForResource:@"t4id_license.txt" ofType:@"p7m"];
    NSData* dLicense = [[NSData alloc] initWithContentsOfFile:sLicensePath];

    // Initialize SDK with license:
    T4Error error = [T4ID initWithLicense:dLicense sharedKeychain:@"T3BDF4PRXW.com.intesigroup.time4id.Time4IDSDK"];
    if( error != T4_OK )
    {
        T4DLog(@"Invalid license");
    }
    else
    {
        T4DLog(@"Time4ID SDK %@ initialized!", [T4ID getLibraryVersion]);
    }
 
 @param license The license data
 @param sharedKeychain If a shared keychain is used to store token, the shared keychain specified in the _entitlements_ file must be passed her. If the shared keychain is not used, pass _nil_ can be passed.
 @param pError A nonnull pointer to an NSError variable. If the function fails, this object will contain the error information
 
 @return T4_OK if successful, otherwise an error code
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_SQLITE_OPEN_FAILED | -20 | Failed to open SQLite DB when migrating from 1.x SDK |
 | T4SDK_MIGRATION_FAILED | -21 | Migration from SDK 1.x failed |
 | T4_ERR_LICENSE | -2017 | Invalid SDK license |
 
 @warning *Important:* Do not use the shared keychain specified in the SDK initialization for any App shared data! Use other shared keychains if Apps reqwuire to share additional data.
 */
+(T4Error)initWithLicense:(NSData* _Nonnull)license sharedKeychain:(NSString* _Nullable)sharedKeychain error:(NSError* _Nullable * _Nonnull)pError;

/**
 Initializes the Time4ID SDK with the license file provided by Intesi Group
 
 This API uses license file present in the application resources.
 
 @param licenseFilename The license file name
 @param sharedKeychain If a shared keychain is used to store token, the shared keychain specified in the _entitlements_ file must be passed her. If the shared keychain is not used, pass _nil_ can be passed.
 
 @return T4_OK if successful, otherwise an error code
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_SQLITE_OPEN_FAILED | -20 | Failed to open SQLite DB when migrating from 1.x SDK |
 | T4SDK_MIGRATION_FAILED | -21 | Migration from SDK 1.x failed |
 | T4SDK_NOT_FOUND | -23 | License file specified wasn't found |
 | T4_ERR_LICENSE | -2017 | Invalid SDK license |
 
 */
+(T4Error)initWithLicenseFile:(NSString* _Nonnull)licenseFilename sharedKeychain:(NSString* _Nullable)sharedKeychain;

/**
 Initializes the Time4ID SDK with the license file provided by Intesi Group
 
 This API uses license file present in the application resources.
 
 @param licenseFilename The license file name
 @param sharedKeychain If a shared keychain is used to store token, the shared keychain specified in the _entitlements_ file must be passed her. If the shared keychain is not used, pass _nil_ can be passed.
  @param pError A nonnull pointer to an NSError variable. If the function fails, this object will contain the error information
 
 @return T4_OK if successful, otherwise an error code
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4SDK_SQLITE_OPEN_FAILED | -20 | Failed to open SQLite DB when migrating from 1.x SDK |
 | T4SDK_MIGRATION_FAILED | -21 | Migration from SDK 1.x failed |
 | T4SDK_NOT_FOUND | -23 | License file specified wasn't found |
 | T4_ERR_LICENSE | -2017 | Invalid SDK license |
 
 */
+(T4Error)initWithLicenseFile:(NSString* _Nonnull)licenseFilename sharedKeychain:(NSString* _Nullable)sharedKeychain error:(NSError* _Nullable * _Nonnull)pError;

/** Sets the login renewal callback
 
 Sample implementation:
 
    [T4ID setLoginRenewalFunction:^(NSString *username, T4LoginType nLoginType, ContinuationCallback continuationCallback) {
        if( nLoginType = T4LoginTime4Mind )
        {
            NSString* sPassword = getPasswordFromLoginWiew(); // Login view implementation specific to App, retrieving password.
            [T4User loginUser:username password:sPassword nodeId:nil contextType:0 rememberMe:YES completion:^(T4Response *resp) {
                continuationCallback([resp hasError]); // Abort if login renewal failed
            }];
        }
        else
            continuationCallback(YES); // Abort if unsupported login mode!
    }];
 
 @param loginRenewalBlock The login renewal implementation block
 
 @warning *Note:* The continuation callback MUST be called in a background thread (use dispatch_async if needed).
 */
+(void)setLoginRenewalFunction:(LoginRenewalFunction _Nonnull)loginRenewalBlock;


/** Sets the Google OAUTH renewal callback.
 
 The implementation is straightforward, but since the Googler Sign-in library is included and managed into the App, the renewal can't be performed inside the SDK.
 Suggested implementation:

    [T4ID setGoogleOauthFunction:^(ContinuationCallback continuationCallback) {
        [[GIDSignIn sharedInstance] signInSilently]; // Google sign-in API
        continuationCallback(NO);
    }];
 
 @param googleOauthBlock The OAUTH renewal implementation block.
 
 @warning *Important:* The continuation callback MUST be called in a background thread (use dispatch_async if needed).
 */
+(void)setGoogleOauthFunction:(GoogleOauthFunction _Nonnull)googleOauthBlock; //*

/** Sets the callback to be called if at SDK startup an URL is read from the pasteboard
 
 @param startupUrlFunction The callback called.
 
 */
+(void)setStartupUrlFunction:(StartupUrlFunction _Nonnull)startupUrlFunction;

/** Returns the service token bearer
 
 @return The service token bearer
 
 @see T4TokenBearer
 */
+(T4TokenBearer)getServiceTokenBearer;

/** ![Backend Call](connectm.png "Backend Call")  Returns the service profile information for the specified service
 
 The function returns a dictionary with the following structure
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | accountData | NSDictionary | Dictionary containing the data about the current user account |
 | serviceData | NSDictionary | Dictionary containing the data about service configuration |
 
 <p>
 <b>accountData</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | accountStatus | NSNumber | 0 → not active<br>1 → active<br>2 → not active password reset pending<br>3 → active, password reset pending<br>4 → deactive<br>5 → deactive, password reset pending |
 | authMode | NSNumber | 0 → password<br>1 → otp<br>2 → token |
 | description   | NSString    | Account description. Can be _nil_ |
 | externalUid | NSString | User identifier. Can be _nil_ |
 | login | NSString | User's login, usually it is the registration email |
 | nickname | NSString | User's nickname. Can be _nil_ |
 
 <p>
 <b>serviceData</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | attributes | NSDictionary | Service attributes |
 | description | NSString | Service description |
 | name | NSString | The service name. Should be the same value passed in the _service_ parameter |
 
 <p>
 <b>attributes</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | BEGIN_END_DISABLED | NSNumber | _true_ if the begin-end algorithm is enabled, otherwise _false_ |
 | CTX_NODE | NSString | Context node of this service |
 | DEFAULT_OTP_MODE | NSString |  |
 | DOMAIN | NSString | Service domanin, for instance: _Time4Mind_ |
 | LANGUAGE | NSString | Service language. For instance: it, en |
 | SINGLE_DEVICE_MODE | NSNumber | _true_ if a service token can be present on a single user's device. If _false_ the begin-end algorithm should be enabled to distinguish which device generated an OTP. |
 | SRV_TOKEN_APP_REQUIRED | NSNumber | _true_ if the service token is required, otherwise _false_ |
 | SRV_TOKEN_BEARER | NSNumber | Service token bearer.<br>0 → App (service token is required for this mode)<br>1 → sms<br>2 → e-mail |
 
 @param service The backend service. For instance _"Time4eID"_ if you want to retrieve informations about the Time4ID service.
 @param completion If successful the response dictionary in the T4Response contains the service data
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_NO_SERVICES | 513 | Searched data wasn't found |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_SESSION_GOOGLE_XPIRED | 13057 | Google session expired |
 | T4U_SERVICE_NOT_FOUND | 17664 | Service not found |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 | T4SDK_INVALID_PARAMS | -24 | Invalid parameters |
 
 */
+(void)getServiceProfile:(NSString* _Nonnull)service completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Set the device token for the device to receive the PUSH notifications by Time4ID.
 
 The PUSH notifications sent by the backend have to be managed directly by the
 
    (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
 
 callback of the App
 
 @param deviceToken deviceToken provided to the App in the _didRegisterForRemoteNotificationsWithDeviceToken_ callback
 @param completion The response is successful if the device token was registered for the current device.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_SESSION_GOOGLE_XPIRED | 13057 | Google session expired |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 | T4SDK_INVALID_PARAMS | -24 | The device token provided is invalid |
 
 */
+(void)setDeviceToken:(NSData* _Nonnull)deviceToken completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*


/**
 Returns the list of user's devices, or the label of the current device
 
 @param completion Array of user's devices. If successful, each element is a dictionary with:
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | label | NSString | The device's description label |
 | cid | NSString | The device's CID |
 
 */
+(void)listDevices:(void (^ _Nonnull)(NSArray<NSDictionary*>* _Nullable, T4Response* _Nonnull))completion; //*


/**
 Sets all the tokens associated to the specified device to migration mode.
 The source tokens will be put in 'migration' mode. The destination device of the migration is always the current device

 @param cid The device to be migrated
 @param completion Completion callback with the call result and the migrating tokens.
 */
+(void)migrateDeviceWithCid:(NSString* _Nonnull)cid completion:(void (^ _Nonnull)(T4Response* _Nonnull,NSArray<T4Token*>* _Nullable))completion;

/** Set a cookie for the API calls to the server.
 
 @param cookie The cookie
 */
+(void)setCookie:(NSString* _Nonnull)cookie;

/** Set an arbitrary header for the calls to the backend server
 
 @param header Header name
 @param value  Header value
 */
+(void)setHeader:(NSString* _Nonnull)header value:(NSString* _Nonnull)value;

/** Remove a previously set header
 
 @param header Header name
 */
+(void)removeHeader:(NSString* _Nonnull)header;

/** Get the information about the configured headers
 
 @return A dictionary containing the configured headers
 */
+(NSDictionary* _Nonnull)getHeaderList;

/**
 The SDK performed a migration of tokens from SDK 1.x.
 The flag is _true_ only when the SDK data was migrated at the current library initialization.
 
 @return _true_ if the migration was performed in this library initialization
 */
+(bool)didMigrateFrom1;


/**
 Returns the migrated tokens users.
 After a successful migration from SDK 1.x to SDK 2.x, the API returns an array containing the usernames of the of the migrated tokens. The array is returned ONLY if a successful migration has just been performed. If the APP was already using 2.x API, the returned array will be empty.

 @return String array containing the migrated tokens usernames.
 */
+(NSArray<NSString*>* _Nullable)getMigratedTokensUsers;



/**
 Checks if an API is implemented on the Time4ID backend and returns the outcome

 @param api name of the API to check
 @param completion _true_ it the API is present, otherwise _false_
 */
+(void)T4IDApiExists:(NSString* _Nonnull)api completion:(void (^ _Nonnull)(bool bExists))completion;

/**
 Checks if an API is implemented on the Time4User backend and returns the outcome
 
 @param api name of the API to check
 @param completion _true_ it the API is present, otherwise _false_
 */
+(void)T4UApiExists:(NSString* _Nonnull)api completion:(void (^ _Nonnull)(bool bExists))completion;

/**
 Parses a received Dynamic Link
 
 @param message received dynamic link
 @param completion A completion callback that is called with the decoded challenge and extra information
 
 The completion callback is called with:
 
 - *error* : Is _nil_ if the operation is OK, otherwise contains error info
 - *tokenId* : The token identifier of the token that is supposed to sign the transaction (challenge)
 - *challenge* : The challenge to be signed by one of the challenge sign operations like [T4Token generateOtpChallenge]
 - *extraInfo* : Other information extracted from the parsed QR Code
 */
+(void)parseDynamicLinkMessage:(NSString* _Nonnull)message completion:(void (^ _Nonnull)(NSError* _Nullable error, T4TokenId* _Nullable tokenId, NSString* _Nullable challenge, NSString* _Nullable extraInfo))completion;


+(NSArray<NSString*>* _Nullable)getSessionLog;
+(void)addExtraLogInfo:(NSString* _Nullable)extraLogInfo;

+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString;

+(T4Error)dropSharedTokenItems;

@end
