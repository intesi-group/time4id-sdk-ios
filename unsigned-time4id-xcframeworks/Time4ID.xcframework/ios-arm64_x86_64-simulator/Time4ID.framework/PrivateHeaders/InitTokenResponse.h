//
//  InitTokenResponse.h
//  T4M_Connector
//
//  Created by Matteo Argento on 17/01/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Time4ID/T4Response.h>
#import <Time4ID/PinStatus.h>
#import <Time4ID/TokenStatus.h>
#import <Time4ID/OtpResult.h>

/**
 *  The structure returned by a remot token initialization.
 */
@interface InitTokenResponse : T4Response

/**
 *  The cryptographic provider for the generated token (for instance: totp)
 */
@property (nonatomic, retain) NSString *otpProvider;

/**
 *  The token index. This parameter is saved in the SDK database as the token device id, because it is used in the begin-end algorithms to identify the device that generated a particular OTP. It is used only if the service is configured so that more devices can have a token from the same issuer.
 */
@property (nonatomic) int tokenIndex;

/**
 *  Unique token identified for the provider.
 */
@property (nonatomic, retain) NSString *otpId;

/**
 *  The uniqueTokenId. This is a unique token identifier alternative to the copy: otpId/otpProvider.
 */
@property (nonatomic, retain) NSString *uniqueTokenIndex;

/**
 *  The token time step in seconds
 */
@property (nonatomic) int timeStep;

/**
 *  The length of the OTP generated for the token
 */
@property (nonatomic) int length;

/**
 *  A descriptive label associated to the token. This can be changed ny the user.
 */
@property (nonatomic, retain) NSString *label;

/**
 *  The issuer service associated with the token. The service publishes descriptive data of the token such as icon, background color, description etc...
 */
@property (nonatomic, retain) NSString *service;

/**
 *  The token's serial number
 */
@property (nonatomic, retain) NSString *serialNumber;

/**
 *  The service domain. For instance: Time4ID
 */
@property (nonatomic, retain) NSString *domain;

/**
 *  The token status
 */
@property (nonatomic, retain) TokenStatus *tokenStatus;

/**
 *  The token PIN status
 */
@property (nonatomic, retain) PinStatus *pinStatus;

/**
 *  Token owner's telephone number, if configured. Otherwise _nil_
 */
@property (nonatomic, retain) NSString *telephoneNumber;

/**
 *  Context node the token belongs to. This parameter must be saved in order to perform other backend operations.
 */
@property (nonatomic, retain) NSString *ctxNode;

/**
 *  The token calculation algorithm. If _nil_ the default is SHA-256
 */
@property (nonatomic, retain) NSString *macAlg;

/**
 *  The service token bearer. It can be 0 -> App, 1 -> sms, 2-> e-mail
 */
@property (nonatomic) int srvTokenBearer;

@end
