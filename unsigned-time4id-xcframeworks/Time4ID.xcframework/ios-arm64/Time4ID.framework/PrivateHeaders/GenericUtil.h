//
//  GenericUtils.h
//  iOS_gui
//
//  Created by Enrico on 05/11/12.
//  Copyright (c) 2012 Sinossi Software Srl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GenericUtil : NSObject

+ (NSString *)platform;
+ (NSString *)model;
+ (BOOL)isNotNilOrNullValue:(id)value;
+ (int)getIntValue: (id)value orDefault: (int)nDefault;
+ (NSString*)getStrValue: (id)value orDefault: (NSString*)sDefault;

@end
