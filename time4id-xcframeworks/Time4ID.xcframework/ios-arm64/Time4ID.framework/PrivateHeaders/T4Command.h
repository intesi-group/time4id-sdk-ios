//
//  T4Command.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 15/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "T4Response.h"

#define TIMEOUT_CONNECTION  30
#define ENDPOINT_T4USER     [[T4LicenseManager sharedManager] getTime4userEndpoint]
#define ENDPOINT_T4ID       [[T4LicenseManager sharedManager] getTime4eidEndpoint]

@interface T4Command : NSObject <NSURLConnectionDelegate>


+(void)commandWithUrl:(NSString* _Nonnull)sUrl command:(NSString* _Nonnull)command params:(NSDictionary* _Nonnull)dParams secureParams:(NSArray* _Nullable)secureParams completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;
+(void)commandWithUrl:(NSString* _Nonnull)sUrl command:(NSString* _Nonnull)command params:(NSDictionary* _Nonnull)dParams secureParams:(NSArray* _Nullable)secureParams supportedMimeTypes:(NSArray<NSString*>* _Nullable)aSupportedMimeTypes completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;
-(id _Nonnull)initWithUrl:(NSString* _Nonnull)sUrl command:(NSString* _Nonnull)command params:(NSDictionary* _Nonnull)dParams secureParams:(NSArray* _Nullable)secureParams supportedMimeTypes:(NSArray<NSString*>* _Nullable)aSupportedMimeTypes  completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;
- (void)execute;

// Utility:
+(void)getEndToEndCertificate:(void (^ _Nonnull)(NSData* _Nullable, T4Response* _Nonnull))completion;
+(NSMutableDictionary* _Nonnull)dictionaryWithSessionAddCidCia:(bool)bAddCidCia;
+(NSData* _Nullable)getEncryptionKey;

// Cookie commands:
+(void)setCookie: (NSString* _Nonnull) cookie;
+(void)setHttpHeader:(NSString* _Nonnull)header value:(NSString* _Nonnull)value;
+(void)updateCookie: (NSString* _Nonnull) cookie addFields:(bool)addFields;
+(void)removeHttpHeader:(NSString* _Nonnull)header;
+(NSDictionary* _Nonnull)getHttpHeaders;
+(NSMutableDictionary* _Nullable)getCookieComponents;


+(NSArray<NSString*>* _Nullable)getSessionLog;
+(void)addExtraLogInfo:(NSString* _Nonnull)extraLogInfo;

@end

typedef int (^DebugHeaders)(NSDictionary* _Nullable headers,NSString* _Nullable setCookie);
void setDebugHeadersCallback( DebugHeaders _Nonnull debugHeaders );
