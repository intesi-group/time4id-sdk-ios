//
//  T4PushAuthentication.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 24/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/T4Response.h>
#import <Time4ID/T4Token.h>
#import <Time4ID/T4PushSignatureDocAuth.h>
#import <Time4ID/T4Credential.h>

/** Push Notification transaction possible statuses:
 */
typedef NS_ENUM(NSInteger, T4PAStatus){
/** Push Authentication transaction has not yet been processed by receiver
     */
    T4PANew = 0,
/** Push Authentication transaction has been approver
     */
    T4PAApproved = 1,
/** Push Authentication transaction has been rejected
     */
    T4PADenied = 2,
/** There has been an internal error
     */
    T4PAError = 3,
/** Push Authentication transaction has expired
     */
    T4PAExpired = 4,
/** Push Authentication transaction has been canceled by the user
     */
    T4PACanceled = 5,
/** The transaction is in progress and an update will be sent to the App
     */
    T4PAInProgress = 6,
/** The transaction was not completed correctly, the user should retry
     */
    T4PARetry = 7,
/** The transaction has gone in an undefined status
     */
    T4PAUndefined = -1
};

/** Token selection for tokenless transactions:
 */
typedef NS_ENUM(NSUInteger, T4PATokenSelect){
/** Always select from token list
     */
    T4PATokenSelectAlways = 0,
/** Select from token list unless there is exactly 1 token available, in this case select the token automatically
     */
    T4PATokenSelectIfMany = 1,
/** Silently select any token. For best user experience the avaliable token with the lowest protection level is selected with the order:
     * 1 - Implicit protection
     * 2 - Biometric protection
     * 3 - Numeric PIN
     * 4 - Alphanumeric PIN
     */
    T4PATokenPickAnyToken = 2
};

/** Push Authentication token confirmation:
 */
typedef NS_ENUM(NSUInteger, T4PATokenConfirm){
/** Push Authentication token selection confirmation is always requested (default)
     */
    T4PATokenConfirmAlways = 0,
/** Push Authentication token selection confirmation is requested only if the token is autoselected
     */
    T4PATokenConfirmOnAutoselect = 1,
/** Push Authentication token selection confirmation is never requested
     */
    T4PATokenConfirmNever = 2
};

/** Push Authentication transaction possible token types:
 */
typedef NS_ENUM(NSUInteger, T4PATokenType){
/** Push Authentication transaction token is a T4Mind token in APP
     */
    T4PATypeApp = 0,
/** Push Authentication transaction token is a T4Mind token NOT in APP (SMS,Email,...)
     */
    T4PATypeNoApp = 1,
/** Push Authentication transaction token is an external token
     */
    T4PATypeExt = 2,
/** Push Authentication transaction token doesn't matter
     */
    T4PATypeAny = 3,
/** Push Authentication transaction token is identified by the signing credential
     */
    T4PATypeCredential = 4
};

/** Push Authentication transaction operation type:
 */
typedef NS_ENUM(NSUInteger, T4PAOpType) {
    /** The produced OTP is consumed by the T4ID backend
     */
    T4PAOpInternal = 0,
    /** The produced OTP is consumed externally
     */
    T4PAOpExternal = 1,
    /** The operation is a signature: OTP and/or PIN are consumed extrenally
     */
    T4PAOpSignature = 2,
    /** The operation is a push message. No OTP is required
     */
    T4PAOpMessage = 3,
    /** The operation is a new token + credential enrollment
     */
    T4PAOpEnrollment = 4
};

/** Push Authentication transaction required authentication:
 */
typedef NS_ENUM(NSUInteger, T4PAAuthType) {
    /** Push Authentication transaction OTP is generated normally (no digest)
     */
    T4PABaseAuth = 0,
    /** Push Authentication transaction OTP is generated using OCRA alg with CONTENT of the transaction as digest
     */
    T4PAContentOCRAAuth = 1,
    /** Push Authentication transaction OTP is generated using OCRA alg with RESULT of the transaction as digest
     */
    T4PAResultOCRAAuth = 2,
    /** Push Authentication transaction OTP is generated using OCRA alg with (CONTENT + RESULT) as digest
     */
    T4PACompleteOCRAAuth = 3,
    /** Push Authentication doesnt require authentication (Transaction Message)
     */
    T4PANoAuth = 4
};

/*
 Push Authentication rejection reason: a string set by the app user describing why the transaction was rejected
 */
typedef NS_ENUM(NSUInteger, T4PARejectionReason) {
    /** Rejection reason should not be requested by the application
     */
    T4PARejectionReasonNone = 0,
    /** Rejection reason is optional: Application should require it, but the user can leave it empty
     */
    T4PARejectionReasonOptional = 1,
    /** Rejection reason is mandatory: The application should require it and the user must specofy a reason
     */
    T4PARejectionReasonMandatory = 2
};

/** Push Authentication transaction failure codes:
 */
typedef NS_ENUM(NSInteger, T4PAFailureCode) {
    T4PAFailureUnknown      = -99,
    T4PAFailureNone         = 0,
    T4PAFailureGeneric      = 1,
    T4PAFailurePINBlocked   = 2,
    T4PAFailurePinInvalid   = 3,
    T4PAFailureOTPResynchReq = 4,
    T4PAFailureTokenLocked  = 5,
    T4PAFailureOTPInvalid   = 6,
    T4PAFailureBusy         = 7,
    T4PAFailurePkBoxException = 8
};

/**
 Push Authentication transaction types in search:
 */
typedef NS_ENUM(NSUInteger, T4PATransactionFilterType){
/** Return all the transactions for the user sorted by the most recent (first)
     */
    T4PATransactionFilterAll = 0,
/** Return the completed transactions sorted by the most recent (first)
     */
    T4PATransactionFilterCompleted = 1,
/** Return the uncomplete transactions sorted by the most recent (first)
     */
    T4PATransactionFilterUncompleted = 2
};

#define PA_CONTENT_TYPE_JSON   @"application/json"
#define PA_CONTENT_TYPE_PDF    @"application/pdf"
#define PA_CONTENT_TYPE_XML    @"application/xml"
#define PA_CONTENT_TYPE_LINK   @"text/html"

@interface T4PushAuthentication : NSObject <NSCopying>

/** Find a Push Authentication locally stored by its transaction ID
 
 @param transactionId Unique identifier for the transaction
 
 @return The Push Authentication object, or _nil_ if not found
 */
+(T4PushAuthentication* _Nullable)findByTransactionId:(NSString* _Nonnull)transactionId;

+(void)submitTransaction:(NSString* _Nonnull)endpoint params:(NSDictionary* _Nonnull)paramsDictionary completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** Returns the Push Authentications filtered by the specified flag.
 
 The returned array of T4PushAuthentication is always sorted by date with the most recent transaction in the first place (array position:0).
 
 @param transactionType The flag allows to specify a type of authentication records to fetch
 
 @return Array containing the fetched T4PushAuthentication objects
 
 @see T4PATransactionFilterType
 */
+(NSArray* _Nullable)findByType:(T4PATransactionFilterType)transactionType;

/** Returns the Push Authentications filtered by the specified transaction status.
 
 The returned array of T4PushAuthentication is always sorted by date with the most recent transaction in the first place (array position:0).
 
 @param status The flag allows to specify a status of authentication records to fetch
 
 @return Array containing the fetched T4PushAuthentication objects
 
 @see T4PAStatus
 */
+(NSArray* _Nullable)findByStatus:(T4PAStatus)status;

/** ![Backend Call](connectm.png "Backend Call")  Returns an Push Authentication transaction data, based on the transaction identifier.
 
 @param paId      transaction identifier
 @param completion The downloaded transaction and completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |

 */

+ (void)getTransaction:(NSString* _Nonnull)paId completion:(void (^ _Nonnull)(T4Response* _Nonnull,T4PushAuthentication* _Nullable))completion; //*


/**
  ![Backend Call](connectm.png "Backend Call")  Updates transaction status with remote status and error info

 @param completion block
 */
- (void)updateStatus:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/**
 ![Backend Call](connectm.png "Backend Call")  Returns the list of PA pending Transactions
 
 @param completion The completion block called when the operation is completed with the transactiona and the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
+ (void)getPendingTransactions:(void (^ _Nonnull)(T4Response* _Nonnull,NSArray* _Nullable))completion; //*

/**
 Returns the transaction identifier for the Push Authentication transaction.
 
 @return The transaction identifier
 */
-(NSString* _Nonnull)getTransactionId;

/**
 Returns the transaction status
 
 @return The transaction status
 
 @see T4PAStatus
 */
-(T4PAStatus)getStatus;

/**
 Returns the transaction title
 
 Usually this is a label identifying the operation type
 
 @return The transaction title
 */
-(NSString* _Nullable)getTitle;

/**
 Returns the transaction apptoval token type.
 
 @return The token type
 
 @see T4PATokenType
 */
-(T4PATokenType)getTokenType;

/**
 Returns the transaction sender
 
 Usually this is an email, URL or an identifier of the entity requesting the oparation
 
 @return The sender
 */
-(NSString* _Nullable)getSender;

-(bool)hideHeader;

/**
 If this flag is enabled, the APP should auto-select the transaction signing token if there is exactly one available token for the operation.
 
 This flag is meaningful only if there is not a specified default token for the transaction, but the signing token is determined by the App.
 
 @return _true_ if auto-select is enabled, otherwise _false_
 */
-(T4PATokenSelect)tokenAutoSelect;

/**
 If this flag is enabled, the APP should always present a confirmation page after a token was selected
 
 This flag is meaningful only if there is not a specified default token for the transaction, but the signing token is determined by the App.
 
 @return The token confirmation value
 
 @see T4PATokenConfirm
 */
-(T4PATokenConfirm)tokenConfirm;

/**
 Returns the type of authentication required by the transaction
 
 @return The Authentication type
 
 @see T4PAAuthType
 */
-(T4PAAuthType)authType;


/**
 Returns the transaction operation type

 @return The Operation type
 
 @see T4PAOpType
 */
-(T4PAOpType)opType;

/**
 Returns the creation time for the operation.
 
 The returned time is when the operation was accepted by the server and issued to the mobile app.
 
 @return Creation time for the operation
 */
-(NSDate* _Nullable)getCreationTime;

/**
 Returns the content mimeType for the operation content
 
 @return The content mimeType
 */
-(NSString* _Nullable)getContentType;

/**
 Returns a dictionary containing the operation descriptive content
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | binaryContent | NSData | The content in binary form |
 | <contentType> | * | Content in object form. The object is specific to the content type returned by the getContentType API |
 
 The supported content types depend by the content that the customer's server can add to the transaction, but an App shoud support the following content types:
 - application/json : The content is an NSDictionary parsed JSON
 - application/pdf : The content is an NSData containing the binary PDF document
 - application/xml : The content is an NSData containing the binary XML document
 - text/html : The content is a NSString with plain string data
 
 @return The content dictionary
 */
-(NSDictionary* _Nullable)getContent;


/**
 The signing token if it is specified in the transaction, otherwise _nil_

 @return The required token's tokenId
 */
-(T4TokenId* _Nullable)getRequiredTokenId;

/**
 Returns an array of tokens which can be used to sign this transaction
 
 The returned array contains T4Token objects. The result can be _nil_ if no suitable tokens are found, or if the transaction doesn't require an implicit OTP signature by an in-app token.
 
 @return The array of candidate tokens
 */
-(NSArray* _Nullable)getTokensAvailableForSign;

// Debug purpose:
-(NSString* _Nullable)getTokenFilteringReport;

/**
 Returns if the transaction requires a specific token to be authenticated
 
 @return _true_ if a specific token is required, _false_ if any token may authenticate the transaction, or if another filter is specified
 */
-(bool)isTokenRequired;

/**
 If the operation requires additional data from the user to complete, an array of NSDictionaries are returned, describing the requested data.

 When confirming the operation, the required data should be specified in the __result__ parameter of the setResult API.
 
 @return additional params required by the operation
 */
-(NSArray* _Nullable)getAdditionalParams;

/**
 Returns the transaction expiry date.
 
 If this time is not specified (returns _nil_) the operation never expires. When the returned date arrives, the operation status is set to T4PAExpired
 
 @return The operation expiry date
 */
-(NSDate* _Nullable)getExpiryDate;

/**
 Returns the Push Signature response value retrieved from the user's choices on the transaction Document list
 
 @return the push signature response
 */
-(NSString* _Nullable)getPushSignatureResponse;


/**
 If the transaction failed, this API returns the failure reason

 @return The failure reason code
 */
-(T4PAFailureCode)getFailureCode;


/**
 If the transaction failed and the failure code is T4PAFailureGeneric, this API returns a failure description message sent by the remote server

 @return The generic failure message. Can be _nil_
 */
-(NSString* _Nullable)getFailureMessage;

/**
 Set the transaction status. The new status is saved on the local DB for this transaction.
 
 @param newStatus new status for the transaction
 
 @return _T4_OK_ if the update was successful, an error code otherwise
 */
-(T4Error)setStatus:(T4PAStatus)newStatus;

-(NSArray* _Nullable)getDocuments;

-(T4PushSignatureDocAuth* _Nullable)getDocumentsAuth;

/**
 Set the PIN value retrieved from the Wizard's PIN page
 
 Set here the special PIN value retrieved by the apposite PIN page specified in the wizard.
 
 @param wizardPIN The PIN value retrieved from the Wizard's PIN page
 */
-(void)setWizardPIN:(NSString* _Nonnull)wizardPIN;

/**
 Set the OTP value retrieved from the Wizard's OTP page
 
 This value should be set if the OTP is retrieved through the OTP wizard page instead of a local APP token.
 
 @param otp The OTP value retrieved from the Wizard's OTP page
 */
-(void)setWizardOTP:(NSString* _Nullable)otp;

/**
 Set the OTP value retrieved from the Wizard's OTP page
 
 @param wizardToken The token who generated the OTP if it is an in-app generated OTP
 @param wizardOTP The otp structure if it was an in-app generated OTP
 */
-(void)setWizardToken:(T4Token* _Nullable)wizardToken otp:(T4Otp* _Nullable)wizardOTP __deprecated_msg("Use [setToken token: authenticationTicket:]");

/**
 Set the token that will be used to sign the transaction. The token should be authenticated through an authentication ticket

 @param token The token used to generate the OTP to sign the transaction
 @param ticket The authentication ticket obtained for the passed token
 */
-(void)setToken:(T4Token* _Nonnull)token authenticationTicket:(NSString* _Nonnull)ticket;

-(T4Token* _Nullable)getAuthenticationToken;
-(NSString* _Nullable)getAuthenticationTicketForToken:(T4Token* _Nonnull)token;

/**
 Set the Push Signature response value retrieved from the user's choices on the transaction Document list
 
 This value should be set if the transaction need a user choice over a list of signable documents.
 
 @param pushSignatureResponse push signature response
 */
-(void)setPushSignatureResponse:(NSString* _Nonnull)pushSignatureResponse;

/**
 ![Backend Call](connectm.png "Backend Call")  Returns the server a response for a requested out of band transaction
 
 @param signingTokenId  The transaction token identifier. If the transaction requires an in-app signature, the token must be specified.
 @param pin             The token PIN for the token specified by accountId in the T4PushAuthentication.
 @param approved        _true_ if the transaction is approver, otherwise _false_. If result is false, the token PIN is not requires, since the transaction data will not be signed.
 @param result          An array containing the user responses to the optional wizard
 @param completion      The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_PIN | 787 | Invalid PIN |
 | T4E_PIN_WRONG_LENGTH | 823 | Wrong pin length |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
| T4E_TRANSACTION_ALREADY_DONE | -4102 | Transaction was already completed |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | Token is not authenticated by a previous call to verifyProtection |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is in an invalid status, an active token is required |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4_ERR_ACCOUNT_NOT_FOUND | -2014 | Signing token not found on device
 | T4_ERR_JSON | -2018 | Missing field in JSON data, or secure pin encryption failed
 | T4ID_SECUREPIN_CERT_INVALID | -3002 | Secure PIN certificate is invalid
 
 */
- (void)setResultWithTokenId:(T4TokenId* _Nullable)signingTokenId pin:(NSString* _Nullable)pin approved:(bool)approved result:(NSArray* _Nullable)result completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/**
 ![Backend Call](connectm.png "Backend Call")  Returns the server a response for a requested out of band transaction
 
 @param signingTokenId  The transaction token identifier. If the transaction requires an in-app signature, the token must be specified.
 @param ticket          The signing token authentication ticket. The ticket can be obtained by calling T4Token::generateAuthenticationTicketWithPin
 @param approved        _true_ if the transaction is approver, otherwise _false_. If result is false, the token PIN is not requires, since the transaction data will not be signed.
 @param result          An array containing the user responses to the optional wizard
 @param completion      The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_PIN | 787 | Invalid PIN |
 | T4E_PIN_WRONG_LENGTH | 823 | Wrong pin length |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_TRANSACTION_ALREADY_DONE | -4102 | Transaction was already completed |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | Token is not authenticated by a previous call to verifyProtection |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is in an invalid status, an active token is required |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4_ERR_ACCOUNT_NOT_FOUND | -2014 | Signing token not found on device
 | T4_ERR_JSON | -2018 | Missing field in JSON data, or secure pin encryption failed
 | T4ID_SECUREPIN_CERT_INVALID | -3002 | Secure PIN certificate is invalid
 
 */
- (void)setResultWithTokenId:(T4TokenId* _Nullable)signingTokenId authenticationTicket:(NSString* _Nullable)ticket approved:(bool)approved result:(NSArray* _Nullable)result completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/**
 ![Backend Call](connectm.png "Backend Call")  Returns the server a response for a requested out of band transaction
 
 @param signingTokenId  The transaction token identifier. If the transaction requires an in-app signature, the token must be specified.
 @param pin             The token PIN for the token specified by accountId in the T4PushAuthentication.
 @param approved        _true_ if the transaction is approver, otherwise _false_. If result is false, the token PIN is not requires, since the transaction data will not be signed.
 @param result          An array containing the user responses to the optional wizard
 @param antiFraudEnabled If _true_, generateOtp API will send antifraud data to server
 @param completion    The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_INVALID_PIN | 787 | Invalid PIN |
 | T4E_PIN_WRONG_LENGTH | 823 | Wrong pin length |
 | T4E_TOKEN_NOT_FOUND_ON_PKBOX | 824 | Token not found on PkBox |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_TOKEN_NOT_EXIST | -4002 | Token does not exist |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_TRANSACTION_ALREADY_DONE | -4102 | Transaction was already completed |
 | T4SDK_TOKEN_NOT_AUTHENTICATED | Token is not authenticated by a previous call to verifyProtection |
 | T4SDK_TOKEN_INVALID_STATUS | -10 | Token is in an invalid status, an active token is required |
 | T4SDK_TOUCHID_NOT_SUPPORTED | -12 | Token is protected with touch-id, but it is not supported on current device |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 | T4_ERR_ACCOUNT_NOT_FOUND | -2014 | Signing token not found on device
 | T4_ERR_JSON | -2018 | Missing field in JSON data, or secure pin encryption failed
 | T4ID_SECUREPIN_CERT_INVALID | -3002 | Secure PIN certificate is invalid
 
 */
- (void)setResultWithTokenId:(T4TokenId* _Nullable)signingTokenId pin:(NSString* _Nullable)pin approved:(bool)approved result:(NSArray* _Nullable)result antiFraudEnabled:(BOOL)antiFraudEnabled completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/**
 ![Backend Call](connectm.png "Backend Call")  Deletes a push authentication transaction.
 
 When deleting a pending transaction, it is also removed from the remote server without a YES/NO answer. If the tsansaction is already resolved with an answer, or expired, it is only deleted locally.
 
 @param transactionId transaction identifier of the Push Authentication transaction
 @param completion    The completion block called when the operation is completed with the operation outcome in the T4Response.
 
 __Errors__
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_PKBOX_ERROR | 257 | PkBox Generic Error |
 | T4E_SVR_INTERNAL_ERROR | -4000 | Time4ID server internal error |
 | T4E_SVR_GENERIC_ERROR | -4001 | Time4ID server generic error |
 | T4E_SVR_CONFIG_ERROR | -4005 | Time4ID server configuration error |
 | T4E_METHOD_NOT_ALLOWED | -4010 | Method not allowed |
 | T4E_SERVICE_NOT_ALLOWED | -4011 | Value not allowed |
 | T4E_TRANSACTION_ALREADY_DONE | -4102 | Transaction was already completed |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain) |
 
 */
+ (void)deleteTransaction:(NSString* _Nonnull)transactionId completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/**
 Returns the extra info stored in the transaction

 @param sKey key identifying the information
 @return the stored value, or _nil_ if there is no such value
 */
-(id _Nullable)getExtraInfo:(NSString* _Nonnull)sKey;

/**
 Allows to set application specific information in a transaction.

 @param sKey key identifying the app specific info
 @param value stored info
 */
-(void)setExtraInfo:(NSString* _Nonnull)sKey value:(id _Nonnull)value;


/**
 Removes application specific information previously stored in a transaction

 @param sKey key identifying the app specific info
 */
-(void)removeExtraInfo:(NSString* _Nonnull)sKey;


/**
 If the transaction contains a 'credential' selection view, the list of credentials available for that view

 @return The list of credentials available
 */
-(NSArray<T4Credential*>* _Nullable)getCredentials;

/**
 Set the rejection reason for rejected transactions
 
 @param rejectionReason The rejection reason
 */
-(void)setRejectionReason:(NSString* _Nonnull)rejectionReason;

/**
 Returns the rejection reason set previously for this transaction
 
 @return The rejection reason
 */
-(NSString* _Nullable)getRejectionReason;

/**
 Returns the rejection reason code
 
 @return The rejection reason code
 */
-(T4PARejectionReason)getRejectionReasonCode;

-(bool)setSelectedCredential:(T4Credential* _Nonnull)cred;

-(T4Credential* _Nullable)getSelectedCredential;

@end
