//
//  GlobalOperationCounter.h
//  Time4ID-Core
//
//  Created by francesco scalise on 17/11/23.
//

#import <Foundation/Foundation.h>
#import <stdatomic.h>

static atomic_int globalOperationCounter;

void incrementGlobalOperationCounter(void);

int getGlobalOperationCounterValue(void);
