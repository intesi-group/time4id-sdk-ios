//
//  T4ListToken.h
//  time4eid
//
//  Created by David Beduzzi on 08/07/14.
//  Copyright (c) 2014 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OtpAccountIdCore.h"
#import "T4ID.h"

// Values in user defaults:
#define GL_CID                          @"CID"
#define GL_CIA                          @"CIA"
#define GL_ENDPOINT_T4E                 @"T4eID_endpoint"
#define GL_ENDPOINT_T4U                 @"T4U_endpoint"
#define GL_PUSH_UPDATE                  @"UpdatePushNotification"
#define GL_PUSH_TOKEN                   @"PushNotificationToken"
#define GL_PUSH_TOKEN_REGISTERED        @"PushTokenRegistered"
#define GL_SHOW_TUTORIAL                @"showTutorial"
#define GL_SHOW_TUTORIAL_G              @"showTutorialG"
#define GL_REMEMBER_ME                  @"RememberUser"
#define GL_SESSION_KEY                  @"SessionKey"
#define GL_LOGIN_USER                   @"User"
#define GL_LAST_USER                    @"LastLoggedOnUser"
#define GL_PHONE_NUMBER                 @"PhoneNumber"
#define GL_PUSH_ID                      @"pushid"
#define GL_TOKENS                       @"Tokens"
#define GL_PENDING_TOKEN                @"PendingToken"
#define GL_USER_DICT                    @"user:%@"
#define GL_PIN_CACHE_TIMEOUT            @"PinCachingTimeout"
#define GL_PIN_LENGTH                   @"PinLength"
#define GL_SLEEP_MODE                   @"SleepMode"
#define GL_TOKEN_BEARER                 @"SRV_TOKEN_BEARER"
#define GL_SRV_TOKEN_REQUIRED           @"SRV_TOKEN_APP_REQUIRED"
#define GL_SRV_DETAILS_RETRIEVED        @"ServiceDetailsRetrieved"
#define GL_SERVICES                     @"Services"
#define GL_SESSION_TOKEN                @"sessionToken"
#define GL_USER_TOKEN                   @"userToken"
#define GL_DEVICE_MODEL                 @"DeviceModel"
#define GL_CAROUSEL_DISPLAYED           @"carouselDisplayed"
#define GL_LAST_REGISTERING_USER        @"lastRegisteringUser"
#define GL_LOGIN_TYPE                   @"loginType"

@interface T4ListToken : NSObject <NSCoding>
{
    // Identification:
    NSString*   m_otpId;
    NSString*   m_otpProvider;
    NSString*   m_user;
    
    // Graphic
    NSString*   m_sDomain;
    NSString*   m_sService;
    NSString*   m_sLabel;
    bool        m_bLabelHashChanged;
    
    // Pin caching
    NSString*   m_sCachedPin;
    NSDate*     m_timePinCached;

    
    // Otp Account
    OtpAccountIdCore* m_otpAccountId;
    
    // Configuration info:
    T4TokenBearer m_nBearer;
    NSString*   m_ctxNode;
}

+(T4ListToken*)findTokenByUser:(NSString*)sUser otpId:(NSString*)otpId otpProvider:(NSString*)otpProvider;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;

//- (id)initWithOtpAccountId: (OtpAccountIdCore*) otpAccountId domain:(NSString*)domain service:(NSString*)service label:(NSString*)label tokenBearer:(T4TokenBearer)tokenBearer ctxNode: (NSString*) sCtxNode;

- (void)setLabel: (NSString*)sLabel;
- (void)setBearer:(T4TokenBearer)nBearer;
- (void)setServiceName:(NSString*)sName;

// Getters:
- (bool) isDisabled;
- (NSString*) getDomain;
- (NSString*) getService;
- (NSString*) getLabel;
- (bool)      labelHasChanged;

- (OtpAccountIdCore*)getAccountId;
- (T4TokenBearer)getBearer;
- (T4PinProtection)getPinProtection;
- (bool)getWrongPinEvidence;
- (NSString*) getCtxNode;


@end
