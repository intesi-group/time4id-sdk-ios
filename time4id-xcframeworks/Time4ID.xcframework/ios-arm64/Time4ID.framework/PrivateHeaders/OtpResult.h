//
//  OtpResult.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 07/04/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef OtpResult_h
#define OtpResult_h

#import "T4Otp.h"

#define OtpResult T4Otp

#endif /* OtpResult_h */
