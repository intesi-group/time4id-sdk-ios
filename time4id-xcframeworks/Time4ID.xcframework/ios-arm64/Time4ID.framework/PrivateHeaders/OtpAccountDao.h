//
//  OtpAccountDao.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/4/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/BaseDao.h>
#import <Time4ID/OtpAccountCore.h>
#import <Time4ID/OtpConfigurationDao.h>
#import <Time4ID/OtpDataDao.h>
#import <Time4ID/OtpAccountIdCore.h>
#import <Time4ID/OtpServiceDao.h>

@interface OtpAccountDao : BaseDao

- (BOOL)insertOtpAccount:(OtpAccountCore *)otpAccount;
- (BOOL)updateOtpAccount:(OtpAccountCore *)otpAccount;
- (BOOL)existsOtpAccountByOtpAccountId:(OtpAccountIdCore *)otpAccountId;
- (void)insertOrUpdateOtpAccount:(OtpAccountCore *)otpAccount;
- (OtpAccountCore *)getOtpAccountByOtpAccountId:(OtpAccountIdCore *)otpAccountId;
- (NSArray *)findAll;
- (NSArray *)findAllOtpAccountId;
- (NSArray *)findAllOtpAccountIdByusername:(NSString *)username;
- (void)deleteOtpAccountByOtpAccountId:(OtpAccountIdCore *)otpAccountId;
- (void)deleteOtpAccountByUsername:(NSString *)username;
- (void)deleteAll;

@end
