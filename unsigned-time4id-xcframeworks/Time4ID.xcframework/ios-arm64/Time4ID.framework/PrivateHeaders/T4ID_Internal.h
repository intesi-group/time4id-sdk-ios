//
//  T4ID_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 11/03/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef T4ID_Internal_h
#define T4ID_Internal_h

#import <sqlite3.h>
#import "T4ID.h"

#import <Realm/Realm.h>

@interface T4ID (LibraryInternalMethods)

+(T4Error)initOldDatabase:(NSString* _Nonnull)dbFilePath;
+(sqlite3* _Nullable)getSQLiteDatabase;

+(NSData* _Nullable)getClientHalfkey:(T4Error* _Nonnull)pError;

+(NSString* _Nonnull)getSalt;
+(T4Error)setSalt:(NSString* _Nonnull)salt;

+(NSString* _Nonnull)getAppVersion;
+(LoginRenewalFunction _Nullable)getLoginRenewalFunction;
+(GoogleOauthFunction _Nullable)getGoogleOauthFunction;
+(NSString* _Nullable)getKeychainGroup;

+(void)fixUserHalfkeyFlag;
+(void)addMigratedTokenUser:(NSString* _Nonnull)sUser;
+(void)updateCertificates:(void (^ _Nonnull)(NSError* _Nullable error))completion;
+(void)getQRCertificateViaBackEnd:(void (^ _Nonnull)(NSError* _Nullable error))completion;

+(ForcedLogoutFunction _Nullable)getForcedLogoutFunction;

@end

#endif /* T4ID_Internal_h */
