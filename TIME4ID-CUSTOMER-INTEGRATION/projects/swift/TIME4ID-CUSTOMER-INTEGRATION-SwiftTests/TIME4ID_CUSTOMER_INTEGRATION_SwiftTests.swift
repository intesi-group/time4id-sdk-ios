//
//  TIME4ID_CUSTOMER_INTEGRATION_SwiftTests.swift
//  TIME4ID-CUSTOMER-INTEGRATION-SwiftTests
//
//  Created by francesco scalise on 24/08/23.
//

import XCTest
@testable import TIME4ID_CUSTOMER_INTEGRATION_Swift
import Time4ID
import OSLog

final class TIME4ID_CUSTOMER_INTEGRATION_SwiftTests: XCTestCase {
    
    private var sut: Time4IDDriver!
    
    override func setUpWithError() throws
    {
        sut = Time4IDDriver()
    }
    
    override func tearDownWithError() throws
    {
        sut = nil
    }
    
    func test_time4id_sdk_flow() async throws
    {
        print("⚠️⚠️⚠️ Warning! perform the test with caution ⚠️⚠️⚠️")
        print("⚠️⚠️⚠️ Always delete data from the simulator before running the test! ⚠️⚠️⚠️")
        
        var sutFunctionUnderTest    = ""
        let data                    = MockData.test_time4id_sdk_flow()
        let licenseFilename         = data.licenseFilename
        let loginType               = data.loginType
        
        do
        {
            sutFunctionUnderTest = "setupTime4ID"
            
            /// 1. Setup Time4ID SDK
            try await sut.setupTime4ID(withLicense: licenseFilename)
            print("--- STEP 1 ---")
            print("✅✅✅ SDK SETUP DONE ✅✅✅")
            
            sutFunctionUnderTest = "login"
            
            /// 2. Login User
            try await sut.login(loginType: loginType)
            print("--- STEP 2 ---")
            print("✅✅✅ LOGIN DONE ✅✅✅")
            
            sutFunctionUnderTest = "initializeToken"
            
            /// 3. Initialize Token
            let tokenID = try await sut.initializeToken()
            print("--- STEP 3 ---")
            print("✅✅✅ TOKEN INITIALIZATION DONE ->\n   TOKEN_ID [\(tokenID)]\n✅✅✅")
            
            sutFunctionUnderTest = "fetchToken"
            
            /// 4. Fetch Token
            var token = try await sut.fetchToken(for: tokenID)
            print("--- STEP 4 ---")
            print("✅✅✅ TOKEN FETCH DONE ✅✅✅")
            
            sutFunctionUnderTest = "generateSeed"
            
            /// 5. Generate Seed
            try await sut.generateSeed(for: token)
            print("--- STEP 5 ---")
            print("✅✅✅ SEED GENERATION DONE ✅✅✅")
            
            sutFunctionUnderTest = "activate"
            
            /// 6. Activate Token
            try await sut.activate(token)
            print("--- STEP 6 ---")
            print("✅✅✅ TOKEN ACTIVATION DONE ✅✅✅")
            
            sutFunctionUnderTest = "generateOTP"
            
            /// 7. Generate OTP
            var otp = try await sut.generateOTP(for: token)
            print("--- STEP 7 ---")
            print("✅✅✅ OTP GENERATION DONE ->\n   OTP [\(otp.otpValue)]\n✅✅✅")
            
            sutFunctionUnderTest = "relogin"
            
            /// 8. Login User
            try await sut.login(loginType: loginType)
            print("--- STEP 8 ---")
            print("✅✅✅ RE-LOGIN DONE ✅✅✅")
            
            sutFunctionUnderTest = "refetchToken"
            
            /// 9. Fetch Token
            token = try await sut.fetchToken(for: tokenID)
            print("--- STEP 9 ---")
            print("✅✅✅ TOKEN RE-FETCH DONE ✅✅✅")
            
            sutFunctionUnderTest = "isSynchronized"
            
            /// 10. Check Token Status
            let status = await token.isSynchronized()
            if status.1 == T4TokenSyncStatus.ok
            {
                print("--- STEP 10 ---")
                print("✅✅✅ TOKEN IS SYNCHRONIZED ✅✅✅")
            }
            else
            {
                print("--- STEP 10 ---")
                print("⚠️⚠️⚠️ TOKEN STATUS -> \(status.1) (Check `T4TokenSyncStatus`) ⚠️⚠️⚠️")
            }
            
            sutFunctionUnderTest = "regenerateOTP"
            
            /// 11. Generate OTP
            otp = try await sut.generateOTP(for: token)
            print("--- STEP 11 ---")
            print("✅✅✅ OTP RE-GENERATION DONE ->\n   OTP [\(otp.otpValue)]\n✅✅✅")
        }
        catch
        {
            XCTFail("❌❌❌ \(sutFunctionUnderTest) - \(error) - check `T4Error` ❌❌❌")
        }
    }
    
    private func print(_ message: String)
    {
        let logger = Logger(subsystem: "TEST", category: "")
        logger.info("\(message)")
    }
    
}

struct MockData {
    
    struct test_time4id_sdk_flow {
        
        let licenseFilename = "test_license.p7m"
        let loginType = LoginType.usernameAndPassword("fscalise@intesigroup.com", "Qwerty123456!")
    }
}
