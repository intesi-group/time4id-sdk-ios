//
//  RToken.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 12/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>

@interface RToken : RLMObject

@property NSString* user;
@property NSString* uniqueTokenId;
@property NSString* otpId;
@property NSString* otpProvider;
@property NSTimeInterval timestampSynchToken;
@property NSData*   dItem;

@end
