//
//  OtpAccountInfo.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/3/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/OtpResult.h>

/**
 *  Contains the data of the token configuration
 */
@interface OtpConfigurationCore : NSObject <NSCopying>

@property (nonatomic) int id;

/**
 *  The token's identifier
 */
@property (nonatomic) int accountId;

/**
 *  The generated OTP length
 */
@property (nonatomic) int otpLenght;

/**
 *  The OTP time step in seconds
 */
@property (nonatomic) int otpTimeStep;

/**
 *  The number of wrong PIN necessary to lock the token if the PIN protection is enabled with PIN error evidence.
 */
@property (nonatomic) int maxTryPinCount;

/**
 *  The otp type
 *
 * @see T4OtpType
 */
@property (nonatomic) T4OtpType otpType;
@property (nonatomic) int flags;

@end

bool testLinkIssue(void);
