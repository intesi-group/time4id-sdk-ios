//
//  RCATemplate.h
//  VirtualRAO
//
//  Created by David Beduzzi on 18/07/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>
#import <Time4ID/RCATemplateQuestion.h>

@interface RCATemplate : RLMObject
@property NSString* templateId;
@property NSString* language;
@property NSString* version;
@property NSInteger sections;
@property RLMArray<RCATemplateQuestion*><RCATemplateQuestion>* questions;

+(RCATemplate*)templateWithDictionary:(NSDictionary*)dictionary error:(NSError**)pError;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<RCATemplate>
RLM_COLLECTION_TYPE(RCATemplate)
