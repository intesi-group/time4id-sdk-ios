//
//  PendingTokenResponse.h
//  T4M_Connector
//
//  Created by Matteo Argento on 21/07/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import "T4Response.h"

/**
 *  Returned by the getPendingTokenWithSessionKey, contains a list of tokens created for the user but not yet registered on the device
 */
@interface PendingTokenResponse : T4Response

/**
 *  Array of InitTokenResponse objects
 */
@property (nonatomic, retain) NSMutableArray *pendingTokenArray;

@end
