//
//  OtpData.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/4/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OtpDataCore : NSObject <NSCopying>

@property (nonatomic) int id;
@property (nonatomic, retain) NSData *cipheredSeed;
@property (nonatomic) int movingFactor;
@property (nonatomic) int accountId;
@property (nonatomic) int wrongPinCount;
@property (nonatomic) int seedLength;

@end
