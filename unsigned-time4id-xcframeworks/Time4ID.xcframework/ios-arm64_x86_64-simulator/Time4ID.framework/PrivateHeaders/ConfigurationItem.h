//
//  ConfigurationItem.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 08/05/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ITEM_E2E        @"END_TO_END_CERT"
#define ITEM_USER       @"activeUser"
#define ITEM_SESSION    @"sessionKey"
#define ITEM_ENC_KEY    @"encryptionKey"
#define ITEM_CIA_TS     @"cia_timestamp"
#define ITEM_CID        @"CID"

@interface ConfigurationItem : NSObject

@property (nonatomic, retain) NSString *key;
@property (nonatomic, retain) NSString *value;

@end
