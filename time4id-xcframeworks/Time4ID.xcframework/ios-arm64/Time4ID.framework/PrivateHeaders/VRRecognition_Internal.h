//
//  VRRecognition_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 04/05/17.
//  Copyright © 2017 Intesi Group SPA. All rights reserved.
//

#ifndef VRRecognition_Internal_h
#define VRRecognition_Internal_h

typedef NS_ENUM(NSInteger, VRRecognitionType){
    VRRecognitionTypeUserACS  = 3,     // ACS + User
    VRRecognitionTypeRAO      = 5,     // QCS + RAO
    VRRecognitionTypeUserQCS  = 6      // QCS + User
};

@interface VRRecognition (LibraryInternalMethods)

-(id)initWithRecognition:(RRecognition*)recognition;
+(void (^)(NSError*, NSString*))getRecognitionStatusUpdateCallback;

@end

#endif /* VRRecognition_Internal_h */
