//
//  T4PushSignatureDocument.h
//  Time4ID_SDK
//
//  Created by Enrico Sallusti on 06/05/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/T4PushSignatureDocAuth.h>
#import <Time4ID/T4PushSignatureInfo.h>
#import <Time4ID/T4Response.h>

@interface T4PushSignatureDocument : NSObject

- (id _Nullable)initWithResponse:(NSDictionary* _Nonnull)info
                   transactionId:(NSString* _Nonnull)transactionId
                   documentIndex:(int)documentIndex
                           error:(NSError* _Nullable * _Nullable)pError;

-(id _Nullable)initWithUri:(NSString* _Nonnull)uri
                     title:(NSString* _Nonnull)title
               contentType:(NSString* _Nonnull)contentType
               description:(NSString* _Nullable)description
           enforcementType:(T4PSEnforcementType)enforcementType
            signatureInfos:(NSArray* _Nullable)signatureInfos
          signatureDocAuth:(T4PushSignatureDocAuth* _Nullable)signatureDocAuth
              documentHash:(NSString* _Nullable)documentHash
     documentHashAlgorithm:(NSString* _Nullable)documentHashAlgorithm
             transactionId:(NSString* _Nonnull)transactionId
          andDocumentIndex:(int)documentIndex;

/**
 Returns the URI of the Document
 
 @return The URI
 */
-(NSString* _Nullable)getUri;

/**
 Returns the Document Title
 
 @return The Title
 */
-(NSString* _Nullable)getTitle;

/**
 Returns the Document content MIMEtype
 
 @return The content MIMEtype
 */
-(NSString* _Nullable)getContentType;

/**
 Returns the Document description
 
 @return The document description
 */
-(NSString* _Nullable)getDescription;

/**
 Returns the document enforcement type
 
 @return The enforcement type
 */
-(T4PSEnforcementType)getEnforcementType;

/**
 Returns an array containing the Document signatures info
 
 @return The T4PushSignatureInfo Array
 
 @see T4PushSignatureInfo
 */
-(NSArray* _Nullable)getSignatureInfo;

/**
 Returns the Backend Authorization information of the Document
 
 @return The Backend Auth information
 
 @see T4PushSignatureDocAuth
 */
-(T4PushSignatureDocAuth* _Nullable)getDocAuth;

/**
 Returns the Document Hash
 
 @return The Document Hash
 */
-(NSString* _Nullable)getDocumentHash;

/**
 Returns the Document Hash Algorithm
 
 @return The Document Hash Algorithm
 */
-(T4MacAlg)getDocumentHashAlgorithm;

/**
 Returns the Document Directory Path
 
 @return The Document Directory Path
 */
-(NSString* _Nullable)getDocumentDirPath;

/**
 Returns the Document File Name
 
 @return The Document File Name
 */
-(NSString* _Nullable)getDocumentFilename;

/**
 Returns the Document Index
 
 @return The Document Index
 */
-(int)getDocumentIndex;

/**
 Returns the document checked by user condition
 
 @return _true_ if the user checked the document, _false_ otherwise
 */
-(BOOL) getIsChecked;

/**
 Set the the user condition on the document
 */
-(void) setIsChecked:(BOOL)checked;

@end
