//
//  T4SessionRenewer.h
//  Time4ID-Core
//
//  Created by francesco scalise on 17/11/23.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

NS_ASSUME_NONNULL_BEGIN

/** Enumeration for session error codes */
typedef NS_ENUM(NSInteger, T4SessionErrorCode) {
    T4SessionErrorCodeSessionExpired = 9001,
    T4SessionErrorCodeTokenUnavailable = 9002,
    T4SessionErrorCodeNullParams = 9003,
    T4SessionErrorCodeInvalidTypeParams = 9004,
    T4SessionErrorCodeVoidParams = 9005
};

/**
 @interface T4SessionRenewer
 Manages the session renewal process.
 */
@interface T4SessionRenewer : NSObject

// Session manager property
@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;

/**
 Initializes a new instance of T4SessionRenewer.

 @param sessionManager The AFHTTPSessionManager to be used for renewing the session.
 @return An instance of T4SessionRenewer.
 */
- (instancetype)initWithSessionManager:(AFHTTPSessionManager *)sessionManager;

/**
 Starts the session renewal process.

 @param completionHandler A block to be executed upon completion of the renewal process.
 */
- (void)renewSessionWithCompletionHandler:(void (^)(NSURLResponse *, id, NSError *))completionHandler;

@end

typedef void (^SessionRenewalCompletion)(BOOL success, NSError *error, void (^completionBlock)(void));

/**
 @interface SessionGatekeeper
 Manages and controls session access, acting as a gatekeeper.
 */
@interface SessionGatekeeper : NSObject

#pragma mark - Session Renewal Status

/**
 Checks if a session renewal is currently in progress.

 @return A boolean indicating if a session renewal is in progress.
 */
+ (BOOL)sessionRenewalInProgress;

#pragma mark - Session Renewal Process

/**
 Initiates the session renewal process.

 @param sessionManager The AFHTTPSessionManager to be used for renewing the session.
 @param completion A block to be executed upon completion of the renewal process.
 */
+ (void)startSessionRenewalWithAFHTTPSessionManager:(AFHTTPSessionManager *)sessionManager
                                         completion:(SessionRenewalCompletion)completion;

#pragma mark - Operation Queue Management

/**
 Adds an operation to the waiting queue.

 @param operationBlock The block to be executed once the session is renewed.
 */
+ (void)addOperationToWaitingQueue:(void (^)(void))operationBlock;

/**
 Executes all operations in the waiting queue.
 */
+ (void)executeWaitingOperations;

@end

NS_ASSUME_NONNULL_END
