//
//  OtpService.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 21/07/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OtpService : NSObject <NSCopying>

@property (nonatomic) int identifier;
@property (nonatomic, retain) NSString *service;
@property (nonatomic, retain) NSString *domain;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) UIImage *icon;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic, retain) NSString *ctxNode;
@property (nonatomic, retain) NSString *contentType;
@property (nonatomic, retain) NSString *hash;
@property (nonatomic, retain) NSString *companyUrl;
@property (nonatomic, retain) NSString *serviceLabel;

@end
