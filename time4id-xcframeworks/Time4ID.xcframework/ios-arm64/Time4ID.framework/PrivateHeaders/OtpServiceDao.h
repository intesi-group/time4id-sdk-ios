//
//  OtpServiceDao.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 21/07/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Time4ID/BaseDao.h>
#import <Time4ID/OtpService.h>

@interface OtpServiceDao : BaseDao

- (BOOL)insertOtpService:(OtpService *)otpService;
- (BOOL)updateOtpService:(OtpService *)otpService;
- (BOOL)updateIcon:(UIImage *)icon forCtxNode:(NSString *)ctxNode andService:(NSString *)service;
- (BOOL)existsOtpServiceByServiceId:(int)serviceId;
- (BOOL)existsOtpServiceByService:(NSString *)service andCtxNode:(NSString *)ctxNode;
- (BOOL)insertOrUpdateOtpService:(OtpService *)otpService;
- (OtpService *)getOtpServiceByServiceId:(int)serviceId;
- (OtpService *)getOtpServiceByService:(NSString *)service andCtxNode:(NSString *)ctxNode;
- (NSArray *)findAll;
- (void)deleteOtpServiceByServiceId:(int)serviceId;
- (void)deleteOtpServiceByService:(NSString *)service andCtxNode:(NSString *)ctxNode;
- (void)deleteAll;

@end
