//
//  Base64Util.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 10/02/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Base64Util : NSObject

+ (NSString *)encodeBase64WithData:(NSData *)objData;
+ (NSData *)decodeBase64WithString:(NSString *)strBase64;

@end
