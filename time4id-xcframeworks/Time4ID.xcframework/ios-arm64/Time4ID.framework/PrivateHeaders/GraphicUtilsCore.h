//
//  GraphicUtilsCore.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 22/07/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GraphicUtilsCore : NSObject

+ (UIColor *)colorWithHex:(NSInteger)hex;
+ (UIColor *)colorWithHex:(NSInteger)hex andWithAlpha:(CGFloat)alpha;
+ (UIColor *)colorWithHexString:(NSString*)hexString alpha:(float)alpha;
+(const unsigned char *) getHfk;
//+ (NSString *)hexStringForColor:(UIColor *)color;

@end
