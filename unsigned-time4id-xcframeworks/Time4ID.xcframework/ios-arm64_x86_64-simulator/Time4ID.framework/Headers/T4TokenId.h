//
//  T4TokenId.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 08/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Time4ID/T4Response.h>

/** Identifies a local token
 
A token is identified by
- the couple otpId + otpProvider or
- by the uniqueTokenId
 
 Both are usually present, but one is sufficient to identify the token.
 */
@interface T4TokenId : NSObject <NSCopying, NSSecureCoding>

/**
 The token provider
 */
@property (nonatomic,retain, readonly) NSString* _Nullable otpProvider;

/**
 The token identifier
 */
@property (nonatomic,retain, readonly) NSString* _Nullable otpId;

/**
 The uniqueTokenId
 */
@property (nonatomic,retain, readonly) NSString* _Nullable uniqueTokenId;

/**
 Creates a specific tokenID with the given parameters.
 
 @param oid       Token OTP identifier
 @param oprovider Token OTP provider
 @param utid      Unique token identifier
 
 @return The created token ID
 
 @warning *Note:* Manually generated T4TokenID may not correspond to a real token. Verify if there is an actual token by calling T4Token find: API.
 
 */
+(T4TokenId* _Nullable) tokenIdWith:(NSString* _Nullable)oid otpProvider:(NSString* _Nullable)oprovider uniqueTokenId:(NSString* _Nullable)utid;


/** ![Backend Call](connectm.png "Backend Call") Sends an OTP to this token.
 Use for non-app tokens: SMS, email, external device.

 @param completion the completion callback with the API result
 */
-(void)pushOtp:(void (^ _Nullable)(T4Response* _Nonnull))completion;


@end
