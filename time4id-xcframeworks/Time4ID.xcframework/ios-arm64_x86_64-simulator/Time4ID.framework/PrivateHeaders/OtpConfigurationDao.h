//
//  OtpConfigurationDao.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/4/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/BaseDao.h>
#import <Time4ID/OtpConfigurationCore.h>


@interface OtpConfigurationDao : BaseDao

- (BOOL)insertOtpConfiguration:(OtpConfigurationCore *)otpConfiguration;
- (BOOL)updateOtpConfiguration:(OtpConfigurationCore *)otpConfiguration;
- (BOOL)existsOtpConfigurationByAccountId:(int)accountId;
- (void)insertOrUpdateOtpConfiguration:(OtpConfigurationCore *)otpConfiguration;
- (OtpConfigurationCore *)getOtpConfigurationByAccountId:(int)accountId;
- (NSArray *)findAll;
- (void)deleteOtpConfigurationById:(int)id;
- (void)deleteAll;

@end
