//
//  VRRecognitionItem.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 03/05/17.
//  Copyright © 2017 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/VRao.h>
#import <Time4ID/VRRecognition.h>


/**
 The recognition item
 The recognition items are the elements of the recognition to be completed in order to successfully submit the recognition
 */
@interface VRRecognitionItem : NSObject


/**
 Returns the recognition ticket

 @return The recognition ticket
 */
-(NSString*)getRecTicket;


/**
 Returns the item identifier

 @return The item identifier
 */
-(NSString*)getItemId;


/**
 Returns the item status
 This is a subset of the recognition statuses. Specifically, an item can be in the following statuses:
 - VRRecStatusNew: The item still wasn't edited by the user
 - VRRecStatusRejected: The item was rejected by the RAO, the reason can be obtained by calling _getRejectionDesctiption_
 - VRRecStatusApproved: The item was approved by the RAO
 - VRRecStatusCompleted: The item was completed by the user

 @return The item's status
 */
-(VRRecStatus)getStatus;


/**
 The item's name
 This value depends by the language specified when the recognition was downloaded

 @return item's name
 */
-(NSString*)getName;


/**
 The item's description
 This value can be _nil_

 @return Item's description
 */
-(NSString*)getDesc;


/**
 The item's value
 Needs to be set by the app
 @return The item's value
 */
-(NSString*)getValue;


/**
 The rejection description
 This call returns a string if the item was rejected by the RAO. Use this to inform the user about the reason why the item value provided was rejected.

 @return The rejection description
 */
-(NSString*)getRejectionDesctiption;


/**
 Returns the item's data type

 @return The data type
 @see VRDataType
 */
-(VRDataType)getDataType;

/**
 Returns the item's info type
 
 @return The info type
 @see VRInfoType
 */
-(VRInfoType)getInfoType;


/**
 Returns the item's flag
 Some items may have a special flage giving the app further info about the value providing process

 @return The flag
 */
-(NSInteger)getFlag;


/**
 Returns _true_ if the item is mandatory in order to submit the recognition. Otherwise returns _false_

 @return _true_ if the item is mandatory in order to submit the recognition. Otherwise _false_
 */
-(bool)isMandatory;

/**
Returns the visibility statys of the item

@return the visibility statys of the item
*/
-(VRVisibility)getVisibility;

/**
Returns the rules for this item's visibility

@return the rules for this item's visibility
*/
-(NSArray*)getRule;

/**
 If this item should be present in a video, the returned value is the item identifier for that video.
 If this item shouln't appear in a video, this API returns _nil_

 @return Video item identifier or _nil_ if not required
 */
-(NSString*)videoRequiredItemId;


/**
 Returns a 0 based index if the item should appear in a video at a specific position.
 If the position is random in the video, or the item isn't present in a video, it returns -1

 @return The video forced position
 */
-(NSInteger)getVideoForcedPosition;


/**
 Returns the item sample image
 For picture type items, a sample image can be provided in order to help the user to provide the correct picture for the item.

 @return Returns the sample image, or _nil_ if a sample image wasn't provided
 */
-(UIImage*)getSampleImage;


/**
 Returns the VRRecognition object where this item belongs

 @return The item's recognition
 */
-(VRRecognition*)getRecognition;


/**
 Returns the item's section number, or -1 if no section was provided

 @return item's section number
 */
-(NSInteger)getSectionNumber;


/**
 Retuns the item's index into its section, or -1 if no section was provided

 @return item's section index
 */
-(NSInteger)getSectionIndex;


/**
 If the item is of type _VRDataTypeChoice_, returns an array of acceptable values

 @return The possible valued for the choice
 */
-(NSArray<NSDictionary*>*)getChoiceAnswers;


/**
 An idem can have extra weblinks addociated to it (for instance for contract approval)

 @return Array of weblinks provided
 */
-(NSArray<NSDictionary*>*)getWeblinks;


/**
 Set the value for an item

 @param value The item's provided value
 @return The operation outcome. The operation can fail for various reasons, but usually because of validation issuer or because of recognition status (not editable in all statuses)
 */
-(NSError*)setValue:(NSString*)value;


/**
 Set the value for an item
 
 @param value The item's provided valus
 @param status The item status to set. As default, if the item value is successfully set the item status
 @return The operation outcome. The operation can fail for various reasons, but usually because of validation issuer or because of recognition status (not editable in all statuses)
 */
-(NSError*)setValue:(NSString*)value status:(VRRecStatus)status;


/**
 If the item is a video item (_VRInfoType_ is _VRInfoTypeVideo_), this API returns a list of items to be completed in order to record the video.
 For other type of items, this API always returns _nil_

 @return List of required items
 */
-(NSArray<VRRecognitionItem*>*)getMissingItemsForVideoItem;


/**
 If this item is required in a video, a change in its value might require an already recorded video to be recorded again.
 Use this API to check that. When called by providing the new item value, it returns the video item that would require to be recorded again
 if the provided value would be set.

 @param value the value to be tested
 @return The video to be recorded again if the provided value vould be set, or _nil_ if no video needs to be recorded again
 */
-(VRRecognitionItem*)valueRequiresResetForVideo:(NSString*)value;

@end
