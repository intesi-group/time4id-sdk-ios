//
//  NSURLSessionTask+Response.h
//  Time4ID-Core
//
//  Created by francesco scalise on 30/06/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSURLSessionTask (Response)

- (NSString *)prettyResponseIn:(NSTimeInterval)elapsedTime data:(NSData * _Nullable)data;

@end

NS_ASSUME_NONNULL_END
