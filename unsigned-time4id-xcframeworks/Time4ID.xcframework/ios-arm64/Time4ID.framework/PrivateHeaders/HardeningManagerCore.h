//
//  HardeningManager.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/2/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/OtpSeedCore.h>
#import <Time4ID/OtpAccountIdCore.h>
#import <Time4ID/CryptoUtilsCore.h>

typedef NS_ENUM(NSUInteger, MigrationStatus){
    migrationNotPerformed = 0,
    migrationDone = 1,
    migrationFailed = -1
};

/**
 *   HardeningManager generates and manages values that makes the SDK data and remote calls safer.<br>
 */
@interface HardeningManagerCore : NSObject

/**
 Generates the CID. CID is an unique identifier of the device. The CID remains unchanged if the app is reinstalled, since it is bound to the device. Is used for calls to the backend.
 
   @return The generated CID.
 
 */
+ (NSString *)generateCid;

/**
 Generates the CIA. CIA is an unique identifier of the specific app installation. If the app is reinstalled the CIA value changes. Is used for calls to the backend.
 
   @return The generated CIA.
 */
+ (NSString *)generateCia;

/**
 Performs consistency and antitampering checks on the internal database. When the antitampering check fails, the application has been tampered, or in any case, the data in the SDK database is no more consistent.
 
   @return Antitampering check result. Returns <i>true</i> if the database is consistent, <i>false</i> if the database is corrupted.
 */
+ (bool)antiTamperingCheck;

+(NSData*)getUnprotectedPinKeyForAccount: (OtpAccountIdCore*)accountId;
+(bool)isSrvHalfkeyPresentForUser:(NSString*)username;
+(OSStatus)setServerHalfkeyForUser: (NSString*) sUser data:(NSData*)halfkey;
+(OSStatus)removeServerHalfkeyForUser: (NSString*) sUser;
+(bool)SeedMigration;
+(MigrationStatus)getMigrationStatusForUser:(NSString*)user;

@end
