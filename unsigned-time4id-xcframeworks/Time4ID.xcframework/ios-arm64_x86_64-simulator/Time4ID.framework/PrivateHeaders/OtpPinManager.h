//
//  OtpPinManager.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/2/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/BaseManagerWithOtpAccount.h>

/**
    Manages the token PIN errors management for tokens with PIN error evidence
 */
@interface OtpPinManager : BaseManagerWithOtpAccount

/**
 Sets the wrong PIN count for the current token.<br>
  <i><font color="red">deprecated</font></i>: Use incrementWrongPinCount and resetWrongPinCount to manage the wrong PIN count.
 
    @param wrongPinCount The wrong PIN count to set.
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 */
- (BOOL)setWrongPinCount:(int)wrongPinCount __deprecated;

/**
 Returns the wrong PIN count for the current token.
 
    @return The wrong PIN count.
 */
- (int)getWrongPinCount;

/**
 Increments the wrong PIN count for the current token.
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 */
- (BOOL)incrementWrongPinCount;

/**
 Returns the consecutive PIN errors count that causes the token to lock.
 
    @return maximum wrong PIN count.
 */
- (int)getMaxWrongPinCount;

/**
 Resets the consecutive PIN errors count that causes the token to lock to 0
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 */
- (BOOL)resetWrongPinCount;

@end
