//
//  T4KeychainStore.h
//  T4eID_CORE
//
//  Created by David Beduzzi on 30/10/14.
//  Copyright (c) 2014 Intesi Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface T4KeychainStore : NSObject

+(OSStatus) saveItem:(NSString*)user key: (NSString*) key value: (NSObject*) value;
+(OSStatus) removeItem:(NSString*)user key: (NSString*) key;
+(NSObject*) getItem:(NSString*)user key: (NSString*) key error:(OSStatus*)pError;

// Shared keychain:
+(OSStatus) setSharedItem: (NSString*) key value: (NSObject*) value;
+(NSObject*) getSharedItem: (NSString*) key error:(OSStatus*)pError;
+(OSStatus)removeSharedItemKey:(NSString*) key;

@end
