//
//  T4User.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 11/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Time4ID/T4Response.h>
#import <Time4ID/T4TokenId.h>
#import <Time4ID/T4Registration.h>

/** User login type
 */
typedef NS_ENUM(NSInteger, T4LoginType) {
    /** Unknown or other login type: this usually correspond to an invalid or bogus login */
    T4LoginOther = -1,
    /** Time4Mind login */
    T4LoginTime4Mind = 0,
    /** Google login */
    T4LoginGoogle = 1,
    /** Facebook login (not yet supported - for future use) */
    T4LoginFacebook = 2,
    /** Twitter login (not yet supported - for future use) */
    T4LoginTwitter = 3
};

/**
 Object representing a logged in user
 */
@interface T4User : NSObject

// User account management:

/** ![Backend Call](connectm.png "Backend Call")  Creates a new Time4Mind account. The created account has to be confirmed!
 
 @param username   Username
 @param password   Password
 @param completion Competion block, called after the account initialization was completed. The completion block receives the operation outcome and, if it was successful, the new user. The user requires to be activated with the _activate_ method.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_INVALID_LOGIN  | 517 | User login failed |
 | T4U_SESSION_GOOGLE_XPIRED | 13057 | Google session expired |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 
 */
+(void)createAccount:(NSString* _Nonnull)username password:(NSString* _Nonnull)password completion:(void (^ _Nonnull)(T4Response* _Nonnull, T4User* _Nullable))completion;

/** Returns the currently logged-in user
 
 @return The logged user object, or _nil_ if noone is logged in.
 
 @see T4User
 */
+(T4User* _Nullable)user;

/**
 Returns the logged user username
 
 @return Username
 */
- (NSString* _Nonnull)getUsername;

/**
 Returns the user's login type
 
 @return The login type
 
 @see T4LoginType
 */
- (T4LoginType)getLoginType;

/**
 Returns the remember-me status for the user
 
 @return the remember-me status
 */
- (bool)getRememberMe;

/** ![Backend Call](connectm.png "Backend Call")  Retrieves information sbout the logged user
 
 @param contextType Context type. Meaning depends by specific implementation. Default is 0, if not specified differently by the service provider.
 @param completion Completion block with the user details. The details are specific to the backend implementation.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_NO_SERVICES | 513 | Searched data wasn't found |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_SESSION_GOOGLE_XPIRED | 13057 | Google session expired |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 
 */
- (void)retrieveInfo:(NSInteger)contextType completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call") Sets the user info attributes
 
 @param contextType Context type. Meaning depends by specific implementation. Default is 0, if not specified differently by the service provider.
 @param attributes  dictionary contatining the user attributes to be set. The attributes are specific to the backend implementation.
 @param completion  Completion block receiving the operation outcome
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_NO_DATA_CHANGED | 770 | An update request for an object have not remote updates |
 | T4U_SESSION_GOOGLE_XPIRED | 13057 | Google session expired |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 
 */
- (void)setInfo:(NSInteger)contextType attributes:(NSDictionary* _Nonnull)attributes completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call")
 Resets the user password. An reset email is sent to the specified email address
 
 @param email      email address to receive the password reset email
 @param completion Completion block receiving the confirmation that the emaill is sent
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_NO_DATA_CHANGED | 770 | An update request for an object have not remote updates |
 | T4U_SESSION_GOOGLE_XPIRED | 13057 | Google session expired |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 
 */
+ (void)resetPassword:(NSString* _Nonnull)email completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call")
 Logout of the currently logged user on the backend server. After calling logout, the current session and OAUTH parameters are invalid.
 
 @param completion Completion block with the operation outcome. It is called with success when the user is logged out.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_MAX_DEVICES | 520 | No more devices can be registered |
 | T4U_USER_NOT_FOUND | 769 | User not found |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 
 */
- (void)logout:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

//->receives a temp sessionKey via email to activate by App

/** ![Backend Call](connectm.png "Backend Call")
 Activates a user account
 
 @param user user being activated
 @param password the new account's password. If this paremeter is not _nil_ the backend will perform a complete login
 @param sessionKey a session key received via account confirmation email
 @param rememberMe _true_ to activate the OAUTH login renewal. Otherwise _false_
 @param completion Completion block with the operation outcome. If successful, the user account has been activated, and a full login performed if the password was provided.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_USER_NOT_FOUND | 769 | User not found |
 | T4U_NO_DATA_CHANGED | 770 | An update request for an object have not remote updates |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 
 */
+ (void)activateUser:(NSString* _Nullable)user password:(NSString* _Nullable)password session:(NSString* _Nonnull)sessionKey rememberMe:(bool)rememberMe completion:(void (^_Nonnull)(T4Response* _Nonnull))completion; //*

/** ![Backend Call](connectm.png "Backend Call")
 Requests the backend service to resend the user account activation email
 
 @param completion Completion block with the operation outcome. I successful the confirmation email was sent.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
| T4U_USER_NOT_FOUND | 769 | User not found |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 
 */
- (void)resendActivationEmail:(void (^ _Nonnull)(T4Response* _Nonnull))completion __deprecated_msg("Use resendActivationEmailToUser. Deprecated in 2.9");


/**
 ![Backend Call](connectm.png "Backend Call")
 Requests the backend service to resend the user account activation email
 
 @param username User to which send the activation email to
 @param completion Completion block with the operation outcome. I successful the confirmation email was sent.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_USER_NOT_FOUND | 769 | User not found |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured |
 */
+ (void)resendActivationEmailToUser:(NSString* _Nonnull)username completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/**
 ![Backend Call](connectm.png "Backend Call")
 Retrieve the credential registration information

 @param registrationId registration ID
 @param completion completion block to receive the response and the registration information
 
 @see T4Registration
 */
+ (void)retrieveCertRegistration:(NSString* _Nonnull)registrationId completion:(void (^ _Nonnull)(T4Response* _Nonnull, T4Registration* _Nullable))completion;


/**
 ![Backend Call](connectm.png "Backend Call")
 Approve the credential registration information
 Sets the registration as approved by the user
 
 @param registrationId registration ID
 @param completion completion block to receive the response and the OTP token information for this credential token
 
 @see T4OtpInfo
 */
+ (void)completeCertRegistration:(NSString* _Nonnull)registrationId completion:(void (^ _Nonnull)(T4Response* _Nonnull, T4OtpInfo* _Nullable))completion;

/**
 ![Backend Call](connectm.png "Backend Call")
 Resend the security code required during a credential registration
 
 @param registrationId registration ID
 @param completion completion called when the operation is completed
 
 @see T4OtpInfo
 */
+ (void)sendSecurityCode:(NSString* _Nonnull)registrationId completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/**
 ![Backend Call](connectm.png "Backend Call")
 Completes the enrollment proces by providing all the required parameters

 @param registrationId credential registration identifier
 @param tokenId OTP token associated to the new credential
 @param securityCode security code provided by the CA
 @param credentialPIN PIN chosen by the user which protects the new credential
 @param completion completion callback called when the registration is completed
 */
+ (void)enrollCertificate:(NSString* _Nonnull)registrationId tokenId:(T4TokenId* _Nonnull)tokenId securityCode:(NSString* _Nonnull)securityCode credentialPIN:(NSString* _Nonnull)credentialPIN completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;


/**
 ![Backend Call](connectm.png "Backend Call")
 Push service OTP for the current user user

 @param channel 0:not specified, 1:SMS, 2:e-mail
 @param completion Completion callback
 */
+(void)pushOtpWithChannel:(int)channel completion:(void (^_Nonnull)(T4Response*_Nonnull))completion;

/**
 For diagnostic use: returns the user's active session.
 
 @return the user's session ID or _nil_ if no active session
 */
- (NSString* _Nullable)getSessionId;

#pragma mark: - Login

/**
 * OAuth Login
 *
 * @param sessionKey Temporary key used to encrypt data for only the current session.
 * @param userToken User Token
 * @param sessionToken Session Token
 * @param completion Completion callback
 */
+ (void)loginWithSessionKey:(NSString *_Nullable)sessionKey userToken:(NSString *_Nullable)userToken sessionToken:(NSString *_Nullable)sessionToken completion:(void(^_Nonnull)(T4Response *_Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  User login
 
 Time4Mind user login using username and password
 
 @param username    Account username
 @param password    Account password
 @param contextType Context type. Meaning depends by specific implementation. Default is 0, if not specified differently by the service provider.
 @param nodeId      Login node, if a specific node is required.
 @param rememberMe  if _true_, a remember-me login is performed and the OAUTH params are locally saved to be used for future silent session renewal. If _false_, the user will have to relogin when the current session expires.
 @param completion  Completion block receiving the login outcome.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_INVALID_LOGIN  | 517 | User login failed  |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured  |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized  |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain)  |
 
 */
+(void)loginUser:(NSString * _Nonnull)username password:(NSString * _Nonnull)password contextType:(NSInteger)contextType nodeId:(NSString* _Nullable)nodeId rememberMe:(bool)rememberMe completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Time4User login with accessToken
 
 Performs a Time4User login using a accessToken provided.
 
 @param accessToken      access token is an opaque string that identifies a user
 @param completion        Completion block receiving the login outcome.
 
 */
+ (void)loginWithAccessToken:(NSString * _Nonnull)accessToken completion:(void (^_Nonnull)(T4Response* _Nonnull))completion;

/** ![Backend Call](connectm.png "Backend Call")  Time4User generic login
 
 Performs a Time4User generic login using a parameters dictionary provided. The params dictionary is specific to the login type performed. The input parameters __must__ contain the key _username_ or _userid_ to identify SDK logged in user.
 
 Examples:
 
 <b>Time4User login:</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | username | NSString | Account username  |
 | password | NSString | Account password  |
 | rememberMe | boolean | If _true_, a remember-me login is performed and the OAUTH params are locally saved to be used for future silent session renewal. If _false_, the user will have to relogin when the current session expires  |
 
 <b>Google login:</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | googletoken | NSString | Token identifier returned by a first call to Google using the Google sign-in API  |
 | rememberMe | boolean | If _true_, a remember-me login is performed and the OAUTH params are locally saved to be used for future silent session renewal. If _false_, the user will have to relogin when the current session expires  |
 
 @param params      Login type specific params
 @param contextType Context type. Meaning depends by specific implementation. Default is 0, if not specified differently by the service provider.
 @param nodeId      Login node, if a specific node is required.
 @param completion  Completion block receiving the login outcome.
 
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_INVALID_LOGIN  | 517 | User login failed  |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured  |
 | T4SDK_INVALID_PARAMS | -24 | Missing _username_ or _userid_ in input params  |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized  |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain)  |
 
 */
+(void)login:(NSDictionary* _Nullable)params contextType:(NSInteger)contextType nodeId:(NSString* _Nullable)nodeId completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;

#pragma mark: - Submit Access Token

/** ![Backend Call](connectm.png "Backend Call")  Time4User submitAccessTokenLogin with a specific access token
 
 Performs a Time4User submitAccessTokenLogin using a provided access token.
 
 
 @param accessToken The provided access token, usually extracted from a login URL containing also enroll information
 @param completion  Completion block receiving the login outcome.
 
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_MISSING_PARAM  | 258 | Missing param in request  |
 | T4U_INVALID_LOGIN  | 517 | User login failed |
 | T4U_TICKET_NOT_FOUND  | 768 | Access token not found on served DB / Database error  |
 | T4U_TOKEN_NODE_VISIBILITY  | 17155 | Access token provided not visible on the provided node  |
 | T4U_SERVER_INTERNAL_ERROR | (-32000) | Time4User server internal error occured  |
 | T4SDK_INVALID_PARAMS | -24 | Missing _username_ or _userid_ in input params  |
 | T4SDK_STORE_NOT_INITIALIZED | -35 | The SDK internal storage was not initialized  |
 | T4SDK_STORAGE_ERROR | -47 | Internal storage error (Realm or Keychain)  |
 
 */
+ (void)submitAccessToken:(NSString *_Nonnull)accessToken nodeId:(NSString *_Nullable)nodeId completion:(void (^_Nonnull)(T4Response * _Nonnull))completion;

@end
