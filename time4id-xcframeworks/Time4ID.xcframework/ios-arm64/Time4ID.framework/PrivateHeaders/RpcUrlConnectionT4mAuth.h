//
//  RpcManager.h
//  iOS_gui
//
//  Created by sinossi on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RpcUrlConnectionT4mAuth : NSObject <NSURLConnectionDelegate>
{
    NSURLConnection *m_connection;
    NSURLResponse *receivedResponse;
    NSMutableData *receivedData;
    NSString *mimeTypeResponse;
    BOOL isConnectionFinished;
    NSInteger statusCode;
    BOOL isError;
    NSError* errConnectionFailed;
}

@property (nonatomic) BOOL isDownload;

- (id)postDataWithUrl:(NSString *)url andDictionary:(NSDictionary *)params andSecureParams:(NSArray*)secureParams andRetry:(BOOL)retry;

@end