//
//  RCertificates.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 07/09/18.
//Copyright © 2018 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>

@interface RCertificates : RLMObject
@property NSString* certificateId;
@property NSData* encoded;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<RCertificates>
RLM_COLLECTION_TYPE(RCertificates)
