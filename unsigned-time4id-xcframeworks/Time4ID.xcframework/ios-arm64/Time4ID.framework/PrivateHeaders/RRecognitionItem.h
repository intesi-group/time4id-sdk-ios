//
//  RRecognitionItem.h
//  VirtualRAO
//
//  Created by David Beduzzi on 18/07/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>
#import <Time4ID/RCATemplateQuestion.h>

@interface RRecognitionItem : RLMObject
@property NSString* parameterId;
@property VRRecStatus status;
@property NSString* value; // String value or data file path
@property NSString* rejectionDesc;
@property RCATemplateQuestion* templateDataItem;

+(RRecognitionItem*)recognitionItemWithDictionary:(NSDictionary*)recognitionItem error:(NSError**)pError;
+(RRecognitionItem*)recognitionItemWithTemplateQuestion:(RCATemplateQuestion*)templateQuestion recognitionData:(NSDictionary*)recognitionData error:(NSError**)pError;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<RRecognitionItem>
RLM_COLLECTION_TYPE(RRecognitionItem)
