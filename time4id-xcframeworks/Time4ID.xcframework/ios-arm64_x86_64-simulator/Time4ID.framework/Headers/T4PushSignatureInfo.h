//
//  T4PushSignatureInfo.h
//  Time4ID_SDK
//
//  Created by Enrico Sallusti on 06/05/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Enforcement type, in order to approve documents signature
 */
typedef enum {
    
    /**
     * Default: Approval is NOT mandatory, signature is NOT selected by default.
     */
    SignatureNotSelected = 0,
    
    /**
     * Approval is NOT mandatory, signature is selected by default.
     */
    SignatureSelected = 1,
    
    /**
     * Signature is selected and it cannot be deselected.
     */
    SignatureMandatory = 2
    
} T4PSEnforcementType;

@interface T4PushSignatureInfo : NSObject

-(id _Nonnull)initWithResponse:(NSDictionary* _Nonnull)info error:(NSError* _Nullable * _Nullable)pError;
-(id _Nonnull)initWithName:(NSString* _Nonnull)name description:(NSString* _Nullable)description andEnforcementType:(T4PSEnforcementType)enforcementType;

/**
 Returns the Document signature name
 
 @return The signature name
 */
-(NSString* _Nullable)getName;

/**
 Returns the Document signature description
 
 @return The signature description
 */
-(NSString* _Nullable)getDescription;

/**
 Returns the signature enforcement type
 
 @return The enforcement type
 */
-(T4PSEnforcementType)getEnforcementType;

/**
 Returns the signature checked by user condition
 
 @return _true_ if the user checked the signature, _false_ otherwise
 */
-(BOOL) getIsChecked;

/**
 Set the the user condition on the signature
 
 */
-(void) setIsChecked:(BOOL)checked;


@end
