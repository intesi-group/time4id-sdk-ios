//
//  T4Credential.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 21/02/18.
//  Copyright © 2018 Intesi Group SPA. All rights reserved.
//

#import <Time4ID/T4Token.h>
#import <Time4ID/T4Registration.h>

/** The credential protection type:
 */
typedef NS_ENUM(NSUInteger, T4PCredentialProtection){
    /** The credential is PIN protected (default)
     */
    T4PCredentialProtectionPIN = 0,
    /** he credential is protected by an internally generated PIN
     */
    T4PCredentialProtectionImplicitPIN = 1
};

@class T4Credential;

typedef void (^T4ServiceReceivedCb)(T4Credential* _Nonnull);

@interface T4Credential : NSObject

+(T4Credential* _Nullable)credentialWithDictionary:(NSDictionary* _Nonnull)dictionary error:(NSError* _Nullable * _Nonnull)pError;

+(void)setServiceReceivedCb:(T4ServiceReceivedCb _Nullable)serviceReceivedCb;

-(bool)isOtpRequired;
-(NSString* _Nonnull)getAlias;
-(NSString* _Nonnull)getLabel;
-(T4TokenId* _Nullable)getTokenId;
-(T4Service* _Nullable)getService;
-(T4OtpDeliveryMode)getOtpMode;
-(NSString* _Nullable)getTokenLabel;
-(NSString* _Nullable)getDeviceLabel;
-(T4PCredentialProtection)getProtection;
-(NSString* _Nonnull)getJSON;

@end
