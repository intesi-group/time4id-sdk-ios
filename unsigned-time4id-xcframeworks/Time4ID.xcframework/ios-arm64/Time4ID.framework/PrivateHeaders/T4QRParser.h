//
//  T4QrParser.h
//  Valid
//
//  Created by Enrico Sallusti on 20/05/15.
//  Copyright (c) 2015 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "T4Response.h"

@interface T4QRParser : NSObject
    
@property (nonatomic,retain) NSURL* uri;
@property (nonatomic,retain) NSString* errorMessage;

- (NSDictionary*) parse: (NSString*) scanResult;
- (BOOL) isValidQrCode: (NSString*) scanResult;
+ (T4MacAlg) toAlg: (NSString*) sAlg;

@end
