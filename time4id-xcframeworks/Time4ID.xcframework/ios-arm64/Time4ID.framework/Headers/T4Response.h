//
//  T4Response .h
//  iOS_gui
//
//  Created by sinossi on 5/25/12.
//  Copyright (c) 2012 Intesi Group SPA. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NSInteger T4Error;

// Error domain:
#define T4SDK                           @"Time4ID_SDK"
#define DOMAIN_SSL                      @"OpenSSL"

// No error:
#define T4_OK                           0

// Operation cancelled:
#define T4_CANCEL                       (-2)
// License errors:
#define T4_LICENSE_LOADING_ERROR       (-25308)
// T4User errors:
#define T4U_NO_DATA_CHANGED             0x302
#define T4U_NO_SERVICES                 513
#define T4U_REGISTRATION_NOT_FOUND      513
#define T4U_ACCOUNT_ALREADY_REGISTERED  514
#define T4U_SESSION_EXPIRED             515
#define T4U_SESSION_INVALID             516
#define T4U_INVALID_LOGIN               517
#define T4U_EXISTS                      518
#define T4U_MAX_DEVICES                 520
#define T4U_TICKED_OP_NOT_ALLOWED       533
#define T4U_USER_CREATED_NOT_ACTIVE     545
#define T4U_TICKET_ALREADY_USED         566
#define T4U_TICKET_ACCOUNT_CREATED      580
#define T4U_TICKET_NOT_FOUND            768
#define T4U_USER_NOT_FOUND              769
#define T4U_ENDTOENDCERT_INVALID        1027
#define T4U_CIPHER_DECIPHER_ERR         1028
#define T4U_USER_ALREADY_ACTIVE         12545
#define T4U_SESSION_GOOGLE_XPIRED       13057
#define T4U_TOKEN_NODE_VISIBILITY       17155
#define T4U_CRED_ALREADY_EMITTED        17446
#define T4U_SERVICE_NOT_FOUND           17664
#define T4U_SERVER_INTERNAL_ERROR       (-32000)
#define T4U_METHOD_NOT_FOUND            (-32601)

// T4ID errors:
#define T4E_PKBOX_ERROR                 257
#define T4E_MISSING_PARAM               258
#define T4E_MISSING_PIN                 771
#define T4E_INVALID_PIN                 787
#define T4E_PIN_LOCKED                  789
#define T4E_INVALID_OTP                 808
#define T4E_TOKEN_LOCKED                809
#define T4E_TOKEN_OUT_OF_SYNCH          816
#define T4E_TOKEN_ALREADY_EXISTS        821
#define T4E_TOKEN_ALREADY_INITIALIZED   822
#define T4E_PIN_WRONG_LENGTH            823
#define T4E_TOKEN_NOT_FOUND_ON_PKBOX    824
#define T4_ERR_RENEWAL_FAILED           (-109)
#define T4_ERR_KEYCHAIN                 (-2011)
#define T4_ERR_HALFKEY                  (-2012)
#define T4_ERR_INTERNAL                 (-2013)
#define T4_ERR_ACCOUNT_NOT_FOUND        (-2014)
#define T4_ERR_INVALID_PIN              (-2015)
#define T4_ERR_DATABASE                 (-2016)
#define T4_ERR_LICENSE                  (-2017)
#define T4_ERR_JSON                     (-2018)
#define T4_ERR_KEYCHAIN_ENTITLEMENT     (-2019)
#define T4ID_ENDTOENDCERT_INVALID       (-3000)
#define T4ID_CIPHER_DECIPHER_ERR        (-3001)
#define T4ID_SECUREPIN_CERT_INVALID     (-3002)
#define T4E_SVR_INTERNAL_ERROR          (-4000)
#define T4E_SVR_GENERIC_ERROR           (-4001)
#define T4E_TOKEN_NOT_EXIST             (-4002)
#define T4E_MAX_SESSION_REACHED         (-4003)
#define T4E_AUTHENTICATION_ERROR        (-4004)
#define T4E_SVR_CONFIG_ERROR            (-4005)
#define T4E_SVR_CODE_INVALID            (-4006)
#define T4E_SVR_DUPLICATE_TOKEN         (-4007)
#define T4E_SVR_MAX_TOKEN_REACHED       (-4009)
#define T4E_METHOD_NOT_ALLOWED          (-4010)
#define T4E_SERVICE_NOT_ALLOWED         (-4011)
#define T4E_WRONG_USER                  (-4013)
#define T4E_WRONG_DEVICE                (-4014)
#define T4E_SERVICE_TOKEN_NOT_FOUND     (-4015)
#define T4E_TRANSACTION_NOT_EXIST       (-4101)
#define T4E_TRANSACTION_ALREADY_DONE    (-4102)
#define T4E_TRANSACTION_EXPIRED         (-4103)

// SDK errors
#define T4SDK_TOKEN_EXPIRED             (13056)
#define T4SDK_TOKEN_INVALID_STATUS      (-10)
#define T4SDK_INVALID_PIN               (-11)
#define T4SDK_TOUCHID_NOT_SUPPORTED     (-12)
#define T4SDK_TOUCHID_AUTH_FAILED       (-13)
#define T4SDK_GENERATION_INTERNAL       (-14)
#define T4SDK_TOKEN_LOCKED              (-15)
#define T4SDK_NO_USER                   (-16)
#define T4SDK_INVALID_TOKENID           (-17)
#define T4SDK_NO_SESSION                (-18)
#define T4SDK_TOUCHID_PASS_NOT_SET      (-19)
#define T4SDK_SQLITE_OPEN_FAILED        (-20)
#define T4SDK_MIGRATION_FAILED          (-21)
#define T4SDK_STORE_INIT_FAILED         (-22)
#define T4SDK_NOT_FOUND                 (-23)   // File to upload was not found on local device
#define T4SDK_INVALID_PARAMS            (-24)   // API call doesn't have the correct params
#define T4SDK_JSON                      (-25)   // JSON deserialization in response failed
#define T4SDK_RESP_INVALID              (-26)   // Response is not an HTTP response
#define T4SDK_RESP_MIMETYPE_NO_JSON     (-27)   // The response data mimetype is not application/json
#define T4SDK_NETWORK_ERROR             (-28)   // Network connection error
#define T4SDK_E2E_DECRYPT_FAILED        (-29)   // The decryption of encrypted parameters sent by the backend failed
#define T4SDK_UNKNOWN                   (-30)   // Unknown error sent by the backend (if the error message is of an unexpected format)
#define T4SDK_INVALID_SEED              (-31)
#define T4SDK_INTERNAL                  (-32)
#define T4SDK_SVR_HALFKEY               (-33)
#define T4SDK_TOKEN_NOT_AUTHENTICATED   (-34)
#define T4SDK_STORE_NOT_INITIALIZED     (-35)
#define T4SDK_OPERATION_ABORTED_BY_USER (-36)   // An SDK operation was aborted because of a user choice
#define T4SDK_RESP_NO_DATA              (-37)   // The backend response is empty (no data)
#define T4SDK_INVALID_PROTECTION        (-38)
#define T4SDK_PIN_TOO_SHORT             (-39)
#define T4SDK_REMOTE_UPDATE             (-40)
#define T4SDK_OTP_NOT_AVAILABLE         (-41)
#define T4SDK_NO_OAUTH_PARAM            (-42)   // Session renewal failed because OATH params were not found
#define T4SDK_INVALID_QRCODE            (-43)
#define T4SDK_PIN_REQUIRED              (-44)
#define T4SDK_TOUCHID_LOCKED            (-45)
#define T4SDK_E2E_INVALID               (-46)
#define T4SDK_STORAGE_ERROR             (-47)
#define T4SDK_UNSUPPORTED_MIMETYPE      (-48)   // The response data mimetype is not one of the supported mimetypes

#define T4SDK_QRCODE_PARSE              (-49)
#define T4SDK_QRCODE_PAYLOAD            (-50)
#define T4SDK_INVALID_CERTIFICATE       (-51)
#define T4SDK_QRCODE_VERIFY             (-52)
#define T4SDK_INVALID_OTP_TICKET        (-53)
#define T4SDK_OTP_PROTECTION_CHANGED    (-54)
#define T4SDK_EMPTY_OTP                 (-55)

#define T4SDK_NOT_PRESENT               (-102)  // The requested field is not present in the response
#define T4SDK_MALFORMED_RESPONSE        (-103)  // The response has a different format from the expected response

#define T4SDK_NO_SSL_CERT_FOUND                         (-104)  // Error no ssl cert found
#define T4SDK_UNABLE_TO_OPEN_CERTIFICATE_ERROR          (-105)  // Error unable to open certificate
#define T4SDK_DECONDING_BASE64_CERTIFICATE_ERROR        (-106)  // Error decoding base64 certificate

/** Algorithms allowed for OTP calculation
 */
typedef NS_ENUM(NSUInteger, T4MacAlg){
    /** SHA-256 (default) */
    ALG_MAC_SHA256   =   0x00,
    /** SHA1 */
    ALG_MAC_SHA1     =   0x01,
    /** SHA-512 */
    ALG_MAC_SHA512   =   0x02,
    /** <i><font color="red">deprecated</font></i> MD5: This algorithm is strongly discouraged, since it generates incorrect OTP values (pseudorandom!) */
    ALG_MAC_MD5      =   0x03,
    /** Algorithm not defined */
    ALG_MAC_NONE     =   0xFF
};

/** PIN protection level
 */
typedef NS_ENUM(NSUInteger, T4PinProtection){
/** Implicit PIN protection. The seed is not protected by a PIN, but by a simmetric key internally generated. This option is allowed only if specified by the License. */
    T4PPNone = 0,
/** Token is protected by a numeric PIN. */
    T4PPNumeric = 0x08,
/** Token is protected by an alfanumeric PIN. */
    T4PPAlfanumeric = 0x10,
/** Token protection will be selected by the user. This setting is not used by the PIN protection, but is an option to allow user selection. */
    T4PPUserSelect = 0x18,
/** Token is protected by a biometric system (TouchId). */
    T4PPBiometric = 0x20
};

/** Type of the generated OTP
 */
typedef NS_ENUM(NSUInteger, T4OtpType) {
    /** The token is event based. Event based tokens have a value depending on an internal counter. The counter is increased each time a new OTP is generated. The issue for event based tokens is to keep aligned the counter on the device with the counter on the backend server. */
    OTP_EVENT_BASED = 0,
    /** The token is time based. Time based tokens are generated based on a moving factor calculated from the current time. This keeps the token counter(moving factor)always aligned with the backend server. The drawback is that a valid verification can be performet at most once for each time frame(usually about 30 seconds). */
    OTP_TIME_BASED = 1
};

/** The T4TokenStatus identifies the different states in which the token can be
 */
typedef NS_ENUM(NSInteger, T4TokenStatus){
    /** The token was not found on the local SDK database.
     */
    T4TokenStatusNotFound = -2,
    /** The token is not initialized. This is normal if the token initialization has been completed, but the token seed has not been imported yet.
     */
    T4TokenStatusNotInitialized = -1,
    /** The token is disabled.
     */
    T4TokenStatusDisabled = 0,
    /** The token is active. This is the normal situation for the healthy, correctly configured token.
     */
    T4TokenStatusActive = 1,
    /** The token is locked. This normally happens when some user locked the token with invalid PIN or OTP treshold.
     */
    T4TokenStatusLocked = 2,
    /** The token is out of synch and needs a resynchronization with the backend.
     */
    T4TokenStatusToBeResynched = 3,
    /** The token is being resynchronized. Resynchronization requires two OTP's and the backend is still expecting the second OTP value to be sent to complete resynchronization.
     */
    T4TokenStatusNextOtpRequired = 4,
    /** The token was initialized with the seed, but was not correctly activated. For a token in this state, it is necessary to call the _activate_ selector to put the token in an active status.
     */
    T4TokenStatusActivationRequired = 10,
    
    /** The token was migrated on another device
     */
    T4TokenStatusMigrated = 11
};

/** Type of the token provider.
 The token provider type identifies the backend entity responsible for the token generation and manintenance.
 */
typedef NS_ENUM(NSUInteger, T4TokenProviderType) {
    /** Time4Mind token. The token is generated in the Intesi Group framework _Time4Mind_ */
    T4TokenProviderTime4Mind = 0,
    /** Google Authenticator token. The token was generated following the Google Authenticator specification. */
    T4TokenProviderGoogle = 1
};

#define QRSCAN_TYPE                  @"type"
#define QRSCAN_ISSUER                @"issuer"
#define QRSCAN_LABEL                 @"label"
#define QRSCAN_SECRET                @"secret"
#define QRSCAN_ALGORITHM             @"algorithm"
#define QRSCAN_DIGITS                @"digits"
#define QRSCAN_COUNTER               @"counter"
#define QRSCAN_PERIOD                @"period"
#define QRSCAN_ACCOUNT_ID            @"accountId"
#define QRSCAN_CTXNODE               @"ctxNode"
#define GOOGLE_PROVIDER              @"google"

/**
 *  Standard response returned by most calls to the Time4Mind backend (Time4User or Time4ID)
 */
@interface T4Response : NSObject <NSCopying>

/**
 *  The error code. Can be generated remotely by the server or locally (for instance if a command parameters check fails or if the network connection or the remote server are unavalialble). If the operation was successful the returned value is T4_OK (0).
 */
@property (nonatomic, retain) NSNumber *errorCode;

/**
 *  A descriptive message for the error. If the operation is successful it's value is undefined.
 */
@property (nonatomic, retain) NSString *errorMessage;

/**
 *  Dictionary of data depending by the operation.
 */
@property (nonatomic, retain) NSDictionary *response;

/**
 *  Initializes the T4Response
 *
 *  @param errorCode    errorCode
 *  @param errorMessage errorMessage
 *  @param info         response data
 *
 *  @return the T4Response
 */
-(id)initWithErrorCode:(int)errorCode errorMessage:(NSString*)errorMessage response:(NSDictionary*)info;

/**
 *  Creates a T4Response  with the given information
 *
 *  @param errorCode    errorCode
 *  @param errorMessage errorMessage
 *  @param info         response data
 *
 *  @return the T4Response
 */
+(T4Response*)responseWithErrorCode:(int)errorCode errorMessage:(NSString*)errorMessage response:(NSDictionary*)info;

/**
 Returns an OK response object
 
 @return A non-error response object
 */
+(T4Response*)ok;

/**
 *  Returns the error state of a T4Response
 *
 *  @return <i>true</i> if the T4Response contains an error, otherwise </false>.
 */
- (BOOL)hasError;

@end
