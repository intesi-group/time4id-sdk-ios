//
//  DatabaseManager.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/3/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sqlite3.h>
#import <Time4ID/CWLSynthesizeSingleton.h>
#import <Time4ID/OtpAccountIdCore.h>
#import <Time4ID/OtpAccountCore.h>
#import <Time4ID/ConfigurationItem.h>

extern NSString *DB_VERSION;

/**
 The database manager class. Should be used just for the Database initialization
 */
@interface DatabaseManagerCore : NSObject
{
    sqlite3 *database;
    NSLock *theLock;
}

CWL_DECLARE_SINGLETON_FOR_CLASS(DatabaseManagerCore);

/**
 Path to the database file
 */
@property (nonatomic, retain) NSString *dbFilePath;

/**
 Creates a database using the file specified in the dbFilePath member
 */
- (void)checkAndCreateDatabase;

/**
 Resets the database file specified in the dbFilePath member
 */
- (void)resetDatabase;
- (BOOL)openDatabaseConnection;

/* DAO METHODS */
- (BOOL)existsOtpAccountByOtpAccountId:(OtpAccountIdCore *)otpAccountId;
- (BOOL)insertOrUpdateOtpAccount:(OtpAccountCore *)otpAccount;
- (OtpAccountCore *)getOtpAccountByOtpAccountId:(OtpAccountIdCore *)otpAccountId;
- (NSArray *)findAllOtpAccount;
- (NSArray *)findAllOtpAccountId;
- (NSArray *)findAllOtpAccountIdByusername:(NSString *)username;
- (void)deleteOtpAccountByOtpAccountId:(OtpAccountIdCore *)otpAccountId;
- (void)deleteOtpAccountByUsername:(NSString *)username;
- (void)deleteAllOtpAccount;

- (BOOL)existsOtpDataByAccountId:(int)accountId;
- (void)insertOrUpdateOtpData:(OtpDataCore *)otpData;
- (OtpDataCore *)getOtpDataByAccountId:(int)accountId;
- (NSArray *)findAllOtpData;
- (void)deleteOtpDataById:(int)id;
- (void)deleteAllOtpData;

- (BOOL)existsOtpServiceByServiceId:(int)serviceId;
- (BOOL)existsOtpServiceByService:(NSString *)service andCtxNode:(NSString *)ctxNode;
- (void)insertOrUpdateOtpService:(OtpService *)otpService;
- (BOOL)updateIcon:(UIImage *)icon forCtxNode:(NSString *)ctxNode andService:(NSString *)service;
- (OtpService *)getOtpServiceByServiceId:(int)serviceId;
- (OtpService *)getOtpServiceByService:(NSString *)service andCtxNode:(NSString *)ctxNode;
- (NSArray *)findAllOtpService;
- (void)deleteOtpServiceById:(int)id;
- (void)deleteOtpServiceByService:(NSString *)service andCtxNode:(NSString *)ctxNode;
- (void)deleteAllOtpService;

- (BOOL)existsOtpConfigurationByAccountId:(int)accountId;
- (void)insertOrUpdateOtpConfiguration:(OtpConfigurationCore *)otpConfiguration;
- (OtpConfigurationCore *)getOtpConfigurationByAccountId:(int)accountId;
- (NSArray *)findAllOtpConfiguration;
- (void)deleteOtpConfigurationById:(int)id;
- (void)deleteAllOtpConfiguration;

- (BOOL)existsConfigurationItem:(ConfigurationItem *)configurationItem;
- (void)insertOrUpdateConfigurationItem:(ConfigurationItem *)configurationItem;
- (ConfigurationItem *)getConfigurationItem:(ConfigurationItem *)configurationItem;
- (NSArray *)findAllConfigurationItem;
- (void)deleteConfigurationItem:(ConfigurationItem *)configurationItem;
- (void)deleteAllConfigurationItem;

@end
