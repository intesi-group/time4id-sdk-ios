//
//  AFNetworkingClient.h
//  Time4ID-Core
//
//  Created by francesco scalise on 14/11/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AFNetworkingClient : NSObject

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithTrustedHosts:(NSArray<NSString *> * _Nonnull)trustedHosts unrestrictedMethods:(NSArray<NSString *> * _Nonnull)unrestrictedMethods;

- (void)cancelAllNetworkOperations;

- (void)executeTaskWithRequest:(NSURLRequest * _Nonnull)request
               uploadProgress:(void (^ _Nullable)(NSProgress * _Nonnull uploadProgress))uploadProgressBlock
             downloadProgress:(void (^ _Nullable)(NSProgress * _Nonnull downloadProgress))downloadProgressBlock
            completionHandler:(void (^ _Nonnull)(NSURLResponse * _Nonnull response, id _Nullable responseObject, NSError *_Nullable error))completionHandler;

@end

NS_ASSUME_NONNULL_END
