//
//  T4Logger.h
//  Time4ID-Core
//
//  Created by francesco scalise on 23/02/23.
//

#import <Foundation/Foundation.h>

#define T4DLog(...) [[T4Logger new] debug:[NSString stringWithFormat:__VA_ARGS__]];
#define T4ELog(...) [[T4Logger new] error:[NSString stringWithFormat:__VA_ARGS__]];

typedef NS_ENUM(NSUInteger, T4LoggerLevel) {
    T4LoggerLevelDebug  = 0,
    T4LoggerLevelInfo   = 1,
    T4LoggerLevelError  = 2,
    T4LoggerLevelFault  = 3,
};

NS_ASSUME_NONNULL_BEGIN

@interface T4Logger: NSObject

@property (nonatomic) T4LoggerLevel level;

- (void)debug:(NSString *)message;

- (void)info:(NSString *)message;

- (void)error:(NSString *)message;

- (void)fault:(NSString *)message;

- (void)print:(id (^)(void))value;

- (void)dump:(id (^)(void))value;

- (void)trace;

- (void)disable;

@end

NS_ASSUME_NONNULL_END
