//
//  T4Utils.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 25/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <OpenSSL/OpenSSL.h>

#import <Foundation/Foundation.h>
#import <Time4ID/T4Response.h>
#import <Time4ID/T4Logger.h>

@interface T4Utils : NSObject

+(id _Nullable)valueOrNil:(NSDictionary* _Nonnull)dictionary key:(NSString* _Nonnull)key class:(Class _Nonnull)objClass;
+ (id _Nullable)valueOrNil:(NSDictionary* _Nonnull)dictionary key:(NSString* _Nonnull)key class:(Class _Nonnull)objClass error:(NSError* _Nullable * _Nullable)pError;
+(void) checkAndStoreSvrHalfkeyCompletion:(bool)forceRenewal completion:(void (^ _Nonnull)(T4Response* _Nonnull response))completion;
+(NSData* _Nullable) decryptSrvHalfKey:(NSData* _Nonnull)eHalfKey;
+(bool)isNullOrNil:(id _Nullable)obj;
+(NSData * _Nullable)getPBKDF2FromString:(NSString * _Nonnull)string;
+(NSData * _Nullable)getPBKDF2FromString:(NSString * _Nonnull)string withSalt:(NSString* _Nonnull)saltString;
+(NSData* _Nullable)dataWithEncodedCertificate:(NSString* _Nonnull)encodedCertificate;
+(NSError* _Nonnull)getSSLError;
+(NSData* _Nullable)getCertificateSHA1Fingerprint:(NSData* _Nonnull)dCert;
+(X509* _Nullable)parseCertificate:(NSData* _Nonnull)encodedCertificate;
+(X509* _Nullable)parseEncodedCertificate:(NSString* _Nonnull)stringCertificate;

+(void) getSecurePinCertificateForCtxNode:(NSString* _Nonnull)ctxNode completion:(void (^_Nonnull)(T4Response* _Nonnull,NSData* _Nullable))completion;
+(void) storeWithSecurePinCertificate:(NSString* _Nonnull)ctxNode otp:(NSString* _Nullable)otp pin:(NSString* _Nullable)pin completion:(void (^_Nonnull)(T4Response*_Nonnull))completion;
+(NSString* _Nonnull)localPathForDocument:(NSString* _Nonnull)filename;
+(NSString* _Nonnull)localPathForLibrary:(NSString* _Nonnull)filename;
+(NSData* _Nullable)sha256digestForFileAtPath:(NSString* _Nonnull)path;

+(NSString* _Nonnull)getAlgString:(T4MacAlg)macAlg;
+(NSString* _Nonnull)getOtpTypeString:(T4OtpType)otpType;
+(NSData* _Nullable)decipherRawSignature:(NSData* _Nonnull)rawSignature withCertificate:(X509* _Nonnull)certificate error:(NSError*_Nullable*_Nullable)ppError;

+(NSString * _Nonnull)preferredLanguage;

@end

time_t getTimeFromASN1(const ASN1_TIME* _Nonnull aTime);
