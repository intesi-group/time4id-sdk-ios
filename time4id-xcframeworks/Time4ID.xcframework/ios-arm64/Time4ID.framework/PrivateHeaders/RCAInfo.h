//
//  RCAInfo.h
//  VirtualRAO
//
//  Created by David Beduzzi on 18/07/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>
#import <Time4ID/RCATemplate.h>

@interface RCAInfo : RLMObject
@property NSString* desc;
@property NSData*   icon;
@property NSString* officeLabel;
@property NSString* officeName;
@property NSString* providerLabel;
@property NSString* providerName;
@property RLMArray<RCATemplate*><RCATemplate>* templates;

+(RCAInfo*)caInfoWithDictionary:(NSDictionary*)dictionary templates:(NSArray<RCATemplate*>*)templates error:(NSError**)error;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<RCAInfo>
RLM_COLLECTION_TYPE(RCAInfo)
