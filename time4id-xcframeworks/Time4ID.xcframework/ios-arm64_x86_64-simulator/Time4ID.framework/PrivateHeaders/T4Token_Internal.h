//
//  T4Token_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 11/03/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef T4Token_Internal_h
#define T4Token_Internal_h

#import <Time4ID/OtpAccountCore.h>

/*
 - Authenticated token
 - Authentication expiry time
 - Cached PIN
 - PIN remotely verified
 */
#define DYNA_AUTHENTICATED  @"authenticated"
#define DYNA_VERIFY_TIME    @"verifyTime"
#define DYNA_CACHED_PIN     @"cachedPin"
#define DYNA_PIN_REMOTELY_VERIFIED  @"pinRemotelyVerified"

typedef NS_ENUM(NSUInteger, T4MigrationType){
    T4Migration1toPrivate       =   0x01,
    T4Migration1toShared        =   0x02,
    T4MigrationPrivateToShared  =   0x03,
    T4MigrationSharedToPrivate  =   0x04
};

@interface T4Token (LibraryInternalMethods)

// Migration from old DB:
- (id)initWithAccount:(OtpAccountCore*)account;
- (id)initWithResponse:(NSDictionary *)response;

+(NSArray*)findAllExclude:(NSString*)excludeProvider;
+(NSString*)descStatus:(T4TokenStatus)status;

-(NSString*)getServiceString;
-(NSString*)getCtxNode; // for Push Authentication
-(NSMutableDictionary*)addTokenInfoWithMigratioInfo:(bool)migrationInfo toDictionary:(NSMutableDictionary*)dictionary;
-(bool)needsToCompleteMigrationToShared;
-(void)cleanupMigrationInfo;

// Set next availability time:
-(T4Error)setNextAvailableTimeWithOtp:(T4Otp*)otp;

// Migration support:
-(bool)setMigrationInfo:(NSString*)oldCID oldSalt:(NSString*)oldSalt migrationType:(T4MigrationType)migrationType;
+(T4Error)upgradeAllTokensToUserHalfkey:(NSData*)userHalfKey fromSvrHalfKey:(NSData*)svrHalfKey;

// For future external use?:
- (NSArray*)generateVerifiedSyncTokens:(bool)removeAuthentication error:(T4Error*)error;

// Extra OTP generation debug info:
+(void)wheelLastOtpInfo;
+(void)setLastGenerationInfo:(NSString*)key value:(NSObject*)object;
+(void)setLastCredentialsInfo:(NSDictionary*)dict;

-(bool)isTokenAuthenticated:(bool)bHasRemotelyVerifiedPin retrievedPin:(NSString**)pSPin;

+(NSData*)getImplicitProtectionKey:(T4Error*)err;

@end

#endif /* T4Token_Internal_h */
