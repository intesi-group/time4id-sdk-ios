//
//  T4TokenStore.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 08/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/T4Response.h>
#import <Time4ID/T4TokenId.h>

@interface T4TokenStore : NSObject

+(T4Error)createTokenStore:(NSString* _Nonnull)sTokenStoreClass;
+(T4Error)migrateTokensWithOldCid:(NSString* _Nullable)oldCid oldSalt:(NSString* _Nullable)oldSalt destroyWhenDone:(bool)destroyWhenDone;
+(T4TokenStore* _Nullable)store;

-(NSArray<NSData*>* _Nonnull)getAllDataItems;
-(NSArray<NSData*>* _Nonnull)getAllDataItemsExclude:(NSString* _Nullable)excludeProvider;
-(NSData* _Nullable)getDataItem:(T4TokenId* _Nonnull)tokenId;
-(OSStatus)setDataItem:(NSData* _Nonnull)dataItem forId:(T4TokenId* _Nonnull)tokenId user:(NSString* _Nonnull)sUser;
-(OSStatus)deleteDataItem:(T4TokenId* _Nonnull)tokenId user:(NSString* _Nonnull)sUser;

@end
