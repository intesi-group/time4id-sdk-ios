//
//  T4PushAuthentication_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 23/03/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef T4PushAuthentication_Internal_h
#define T4PushAuthentication_Internal_h

#import "T4PushAuthentication.h"

#define REQUIRES_SUBMIT(result) (((result)==T4PANew)||((result)==T4PARetry))

@interface T4PushAuthentication (LibraryInternalMethods)

+(NSString* _Nullable)descStatus:(T4PAStatus)status;
-(BOOL)isEqualToPushAuthentication:(T4PushAuthentication * _Nonnull)pushAuthentication;

@end

#endif /* T4PushAuthentication_Internal_h */
