//
//  T4MResponse.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 25/03/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef T4MResponse_h
#define T4MResponse_h

// 1.x compatibility
#import "T4Response.h"
#define T4MResponse T4Response

#endif /* T4MResponse_h */
