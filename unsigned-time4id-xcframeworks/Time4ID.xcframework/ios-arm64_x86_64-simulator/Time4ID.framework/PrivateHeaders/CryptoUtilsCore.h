//
//  CryptoUtils.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/2/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import <Time4ID/ConfigurationItem.h>
#import <Time4ID/T4Response.h>

#define SIZE_SEED_FULL      96

@interface CryptoUtilsCore : NSObject

+ (NSData *)getKeyDerivationPBKDF:(int)keyLength rounds:(int)rounds salt:(NSData *)salt myPassData:(NSData *)myPassData;
+ (NSMutableData *)aesEncryptData:(NSData *)data withKey:(NSData *)key isPaddingEnable:(BOOL)isPaddingEnable;
+ (NSData *)aesDecryptData:(NSData *)data withKey:(NSData *)key isPaddingEnable:(BOOL)isPaddingEnable;
+ (NSData *)getPBKDF2FromPassword:(NSString *)password;
+ (NSData *)getSalt;
+ (unsigned char *)generateRandomSalt256;
+ (unsigned char *)getHmacFromKeyString:(NSString *)keyString andDataChar:(char *)dataChar size:(NSUInteger)size algorithm:(T4MacAlg)alg;
+ (NSData*)getDigest:(NSData*)data alg:(T4MacAlg)alg;
+(NSData*)getMultiDigest:(NSArray*)aData alg:(T4MacAlg)alg;
+ (NSString *)stringToHex:(NSString *)string;

+ (NSUInteger)digestLength:(NSUInteger)alg;

+(T4MacAlg)getTokenAlgFromString:(NSString*)alg;
+(void)deleteEncryptionKey;

/**
 * Checks if the encryption key is saved into the internal DB, otherwise creates it
 * @param keyType the type of key created, it depends from the certificate that will be used then to
 *                encrypt it, when communicating with server:
 *                <ul>
 *                    <li>0:EndToEnd Certificate Encryption </li>
 *                    <li>1:SecurePin Certificate Encryption </li>
 *                </ul>
 * @return the encryptionKey
 */
+(NSData*)getOrCreateEncryptionKey:(int)keyType;

+(NSData*)encryptEncryptionKeyWithCertValue:(NSString*)certValue andEncryptionKey:(NSData*)encryptionKey;
+(NSData*)encryptEncryptionKey:(NSData*)certValue andEncryptionKey:(NSData*)encryptionKey;
+(NSString*)getSSLError;
+(const unsigned char *) getHfk;

// 2.0 API:
+ (NSData *)getHmacFromKey:(NSData *)data andDataChar:(char *)dataChar size:(NSUInteger)size algorithm:(T4MacAlg)alg;
+ (NSError*)aesEncryptOrDecrypt:(CCOperation)operation file:(NSString*)inputFile toFile:(NSString*)encryptedFile withKey:(NSData *)key isPaddingEnable:(BOOL)isPaddingEnable;

@end
