//
//  OtpAccountManager.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/3/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/BaseManagerWithOtpAccount.h>
#import <Time4ID/CWLSynthesizeSingleton.h>
#import <Time4ID/OtpAccountCore.h>
#import <Time4ID/OtpGeneratorManager.h>
#import <Time4ID/OtpConfigurationManager.h>
#import <Time4ID/OtpPinManager.h>
#import <Time4ID/OtpSeedManager.h>
#import <Time4ID/OtpAccountIdCore.h>
#import <Time4ID/HardeningManagerCore.h>
#import <Time4ID/LicenseManager.h>
#import <Time4ID/InitTokenResponse.h>

@protocol OtpServerDelegate <NSObject>
- (BOOL)removeOtpAccount:(NSString *)username;
@end

/**
 OtpAccountManager manages the OTP Token account. An OTP account contains the data necessary to generate an OTP.
 */
@interface OtpAccountManager : BaseManagerWithOtpAccount

CWL_DECLARE_SINGLETON_FOR_CLASS(OtpAccountManager);

@property (nonatomic, retain) id  <OtpServerDelegate> delegate;

@property (nonatomic, retain) NSString* touchIdMessage;

/**
 Initializes the CORE library. The initialization may faile because the license is invalid or the database is corrupt or missing.
 
    @return <i>true</i> if the library was correctly initialized, otherwise </false>.
 */
- (BOOL)initManagers;

/**
 Creates a new token account initializing it with the given inputs
 
    @param otpAccountId The new account initial identifiers: user, id and provider
    @param deviceId     This optional parameter is used to support multiple device tokens (the same service is available on multiple device, in this case you need to give a different <i>deviceId</i> for each token).
 
    @return <i>true</i> if the new token was successfully created, otherwise </false>
 */
- (BOOL)initOtpAccountWithOtpAccountId:(OtpAccountIdCore *)otpAccountId andDeviceId:(int)deviceId __deprecated_msg("use -(BOOL)initOtpAccountWithOtpAccountId:(OtpAccountIdCore *)otpAccountId andInitTokenResponse:(InitTokenResponse*)initTokenResponse instead.");

/**
 Creates a new token account initializing it with the given inputs
 
 @param otpAccountId        The new account initial identifiers: user, id and provider
 @param initTokenResponse   This parameter is used to correctly initialize the OtpAccount
 
 @return <i>true</i> if the new token was successfully created, otherwise </false>
 */
- (BOOL)initOtpAccountWithOtpAccountId:(OtpAccountIdCore *)otpAccountId andInitTokenResponse:(InitTokenResponse*)initTokenResponse;

/**
 Removes the token identified by the given account identifier.
 
    @param otpAccountId     The account to be removed.
    @param removeFromServer <i><font color="red">not supported</font></i> (currently the API removes the account only on the internal SDK).
 
    @return <i>true</i> if the new token was successfully removed, otherwise </false>
 */
- (BOOL)removeOtpAccountWithOtpAccountId:(OtpAccountIdCore *)otpAccountId removeFromServer:(BOOL)removeFromServer;

- (BOOL)removeOtpAccountWithUsername:(NSString *)username removeFromServer:(BOOL)removeFromServer __deprecated;

/**
 Checks if the account for the given account id exists on the local SDK database
 
    @param otpAccountId The account to look for
 
    @return <i>true</i> if the account was found, otherwise </false>
 */
- (BOOL)existsOtpAccountWithOtpAccountId:(OtpAccountIdCore *)otpAccountId;

/**
 Returns all OTP accounts present on the local SDK database
 
    @return An array of OtpAccountIdCore objects identifying the tokens present in the local SDK.
 
     @see OtpAccountIdCore
 */
- (NSArray *)getAllOtpAccountId;

/**
 Returns all OTP accounts present on the local SDK database owned by the specified user.
 
    @param username The user.
 
    @return An array of OtpAccountIdCore objects owned by the specified user.
 
     @see OtpAccountCore
 */
- (NSArray *)getAllOtpAccountIdByUsername:(NSString *)username;

/**
 Returns the OtpAccountCore structure corresponding to the account sepcified.
 
    @param otpAccountId account identified.
 
    @return The corresponding account, or <i>nil</i> if the account was not found on the local database.
 
    @see OtpAccountCore
 */
- (OtpAccountCore *)getOtpAccountWithOtpAccountId:(OtpAccountIdCore *)otpAccountId;

/**
 Returns the OtpGeneratorManager used to generate the OTP.
 
    @param otpAccountId the account's identifier.
 
    @return the OTP generator.
 
    @see OtpGeneratorManager
 */
- (OtpGeneratorManager *)getOtpGeneratorManagerWithOtpAccountId:(OtpAccountIdCore *)otpAccountId;

/**
 Returns the OtpConfigurationManager used to check and modify the token configuration.
 
    @param otpAccountId the account's identifier.
 
    @return the token's configuration manager.
 
    @see OtpConfigurationManager
 */
- (OtpConfigurationManager *)getOtpConfigurationManagerWithOtpAccountId:(OtpAccountIdCore *)otpAccountId;

/**
 Returns the OtpPinManager used to check and modify the token's PIN configuration.
 
    @param otpAccountId the token identifier.
 
    @return the token's PIN manager.
 
    @see OtpPinManager
 */
- (OtpPinManager *)getOtpPinManagerWithOtpAccountId:(OtpAccountIdCore *)otpAccountId;

/**
 Returns the OtpSeedManager used to manage the token's seed.
 
    @param otpAccountId the token identifier.
 
    @return the token's seed manager.
 
    @see OtpSeedManager
 */
- (OtpSeedManager *)getOtpSeedManagerWithOtpAccountId:(OtpAccountIdCore *)otpAccountId;

/**
 <i><font color="red">deprecated</font></i> Sets the account's status to <i>initialized</i>.
 
    @param otpAccountId the token's identifier.
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 */
- (BOOL)setOtpAccountInitializedWithOtpAccountId:(OtpAccountIdCore *)otpAccountId  __deprecated;

/**
 <i><font color="red">deprecated</font></i> Sets the account's status to <i>not initialized</i>.
 
    @param otpAccountId the token identifier.
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 */
- (BOOL)setOtpAccountNotInitializedWithOtpAccountId:(OtpAccountIdCore *)otpAccountId  __deprecated;

/**
 <i><font color="red">deprecated</font></i> Sets the account's status to <i>blocked</i>.
 
    @param otpAccountId the token identifier.
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 */
- (BOOL)setOtpAccountBlockedWithOtpAccountId:(OtpAccountIdCore *)otpAccountId  __deprecated;

/**
 Allows to set the token status to a specified value.
 
    @param otpAccountId The token identifier.
    @param status       The new status for the specified token.
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 
    @see T4TokenStatus
 */
- (BOOL)setOtpAccountStatusWithOtpAccountId:(OtpAccountIdCore *)otpAccountId status:(T4TokenStatus)status;

/**
 Returns the specified token's status
 
    @param otpAccountId The token identifier.
 
    @return The token's status
 
    @see T4TokenStatus
 */
- (T4TokenStatus)getOtpAccountStateWithOtpAccountId:(OtpAccountIdCore *)otpAccountId;

/**
 Returns the token identifier for the token on the local database with the specified uniqueTokenId
 
    @param uniqueTokenId the token's uniqueTokenId. The <b>uniqueTokenId</b> is an identifier that uniquely identifies the token instead of the copy otpId - otpProvider.
    @param username      username owning the token. The token will be searched only among the tokens owned by the specified users.
 
    @return The token identifier corresponding to the found account. Returns <i>nil</i> if the token is not found.
 
    @see OtpAccountIdCore
 */
- (OtpAccountIdCore*)findAccountWithUniqueTokenId:(NSString*)uniqueTokenId andUsername:(NSString*)username;

/**
 Returns the uniqueTokenId for the token on the local database with the specified account identifier
 
 @param accountId The token identifier corresponding to the account.
 
 @return uniqueTokenId the token's uniqueTokenId. The <b>uniqueTokenId</b> is an identifier that uniquely identifies the token instead of the copy otpId - otpProvider. Returns <i>nil</i> if the token is not found.
 
 @see OtpAccountIdCore
 */
- (NSString*)getUniqueTokenIdWithAccountId:(OtpAccountIdCore*)accountId;

/**
 Changes the protection for the token
 
 @param accountId The token identifier corresponding to the account.
 @param sOldPin Old PIN if the token is PIN protected, otherwise this parameter is ignored and can be nil
 @param newPin New PIN if the new protection is T4PPNumeric or T4PPAlfanumeric. Otherwise this parameter is ignored and can be nil
 @param pinProtection New token protection. If the new token protection is T4PPBiometric, a TouchID authentication will be required for the token before the callback <i>onCompletion</i> is called.
 @param disableWrongPinNotification If token protection expects a PIN, this parameter specifies if the wrong PIN notification is disabled for the token
 @param onCompletion Callback that is asynchronously called when the change PIN operation is completed. The returned parameter is the operation outcome. T4_OK if the change PIN was successful, otherwise an error code is returned.
 
 @see OtpAccountIdCore
 */
- (void)changeProtectionForAccount:(OtpAccountIdCore*)accountId oldPin:(NSString*)sOldPin newPin:(NSString*)newPin pinProtection:(T4PinProtection)pinProtection disableWrongPinNotification:(bool)disableWrongPinNotification onCompletion:(void (^)(NSInteger))onCompletion;

@end
