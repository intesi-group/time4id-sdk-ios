//
//  T4Biometrics.h
//  Time4ID_SDK
//
//  Created by Davide Di Stefano on 16/10/2019.
//  Copyright © 2019 Intesi Group SPA. All rights reserved.
//

#import <Time4ID/T4Response.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    T4BiometricTypeNone,
    T4BiometricTypeTouchID,
    T4BiometricTypeFaceID
} T4BiometricType;

@interface T4Biometrics : NSObject

-(instancetype) init;
+(T4Biometrics *) sharedInstance;

-(BOOL) isBiometricsSupported;
-(T4BiometricType) supportedBiometryType;
-(NSString *) supportedBiometryName;
-(NSString *) supportedBiometryIdentifier;

-(void)authenticateWithLocalizedReason:(NSString * _Nullable)reason completion:(void (^)(BOOL success, T4Error t4error, NSError * error))completion;

@end

NS_ASSUME_NONNULL_END
