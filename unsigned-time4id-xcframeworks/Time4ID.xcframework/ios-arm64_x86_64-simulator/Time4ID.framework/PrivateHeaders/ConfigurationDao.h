//
//  OtpKeyValueDao.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 08/05/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Time4ID/BaseDao.h>
#import <Time4ID/ConfigurationItem.h>

@interface ConfigurationDao : BaseDao

- (BOOL)insertConfigurationItem:(ConfigurationItem *)configurationItem;
- (BOOL)updateConfigurationItem:(ConfigurationItem *)configurationItem;
- (BOOL)existsConfigurationItem:(ConfigurationItem *)configurationItem;
- (void)insertOrUpdateConfigurationItem:(ConfigurationItem *)configurationItem;
- (ConfigurationItem *)getConfigurationItem:(ConfigurationItem *)configurationItem;
- (NSArray *)findAllConfigurationItem;
- (void)deleteConfigurationItem:(ConfigurationItem *)configurationItem;
- (void)deleteAllConfigurationItem;

@end
