//
//  BaseDao.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/4/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <sqlite3.h>

extern char * COLUMN_ID;
extern char * COLUMN_ACCOUNTID;
extern char * TABLE_OTPACCOUNT;
extern char * COLUMN_ID_OTPACCOUNT;
extern char * COLUMN_USERNAME;
extern char * COLUMN_CREATIONDATE;
extern char * COLUMN_OTPSTATE;
extern char * COLUMN_DEVICEID;
extern char * COLUMN_OTPID;
extern char * COLUMN_OTPPROVIDER;
extern char * COLUMN_LABEL;
extern char * COLUMN_SRVTOKENBEARER;
extern char * COLUMN_SERVICEID;
extern char * COLUMN_CTXNODE;
extern char * COLUMN_UNIQUETOKENID;
extern char * TABLE_OTPSERVICE;
extern char * COLUMN_SERVICE;
extern char * COLUMN_DOMAIN;
extern char * COLUMN_DESCRIPTION;
extern char * COLUMN_ICON;
extern char * COLUMN_COLOR;
extern char * COLUMN_CONTENT_TYPE;
extern char * COLUMN_COMPANY_URL;
extern char * COLUMN_SERVICE_LABEL;
extern char * COLUMN_HASH;
extern char * COLUMN_COLOR;
extern char * TABLE_OTPDATA;
extern char * COLUMN_CIPHEREDSEED;
extern char * COLUMN_MOVINGFACTOR;
extern char * COLUMN_WRONGPINCOUNT;
extern char * TABLE_OTPCONFIGURATION;
extern char * COLUMN_OTPLENGHT;
extern char * COLUMN_OTPTIMESTEP;
extern char * COLUMN_MAXTRYPINCOUNT;
extern char * COLUMN_OTPTYPE;
extern char * COLUMN_FLAGS;
extern char * TABLE_CONFIGURATION;
extern char * COLUMN_KEY;
extern char * COLUMN_VALUE;
extern char * COLUMN_SEEDLENGTH;

@interface BaseDao : NSObject
{
    sqlite3 *database;
}

- (id)initWithDatabase:(sqlite3 *)database;

@end
