//
//  T4Registration.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 01/12/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

/** Registration status
 */
typedef NS_ENUM(NSUInteger, T4RegistrationStatus){
    /** Incomplete, approved registration */
    T4RegistrationIncompleteApproved = 0,
    /** Incomplete, to be approved */
    T4RegistrationIncompleteNotApproved = 1,
    /** Complete and approved */
    T4RegistrationCompleteApproved = 2,
    /** Complete, to be approved */
    T4RegistrationImcompleteToBeApproved = 3,
    /** Closed */
    T4RegistrationClosed = 4,
    /** Contains errors */
    T4RegistrationError = 5,
    /** Enroll completed */
    T4RegistrationEnrollCompleted = 6,
    /** Push enrollment is pending */
    T4RegistrationPushEnrollPending = 7
};

/** OTP delivery mode
 */
typedef NS_ENUM(NSUInteger, T4OtpDeliveryMode){
    /** SMS */
    T4OtpModeSMS = 0,
    /** App */
    T4OtpModeApp = 1,
    /** email */
    T4OtpModeEmail = 2,
    /** external */
    T4OtpModeExternal = 3,
    /** message (sms+email) */
    T4OtpModeMessage = 4,
    /** oauth */
    T4OtpModeOAuth = 5
};

/** Registration steps
 */
typedef NS_ENUM(NSUInteger, T4RegistrationStep){
    /** eseguire tutti gli step del flusso */
    T4RegStepStart = 0,
    /** fase di conferma completato */
    T4RegStepConfirm = 1,
    /** fase OOB sottomessa in attesa di risposta da device */
    T4RegStepWaiting = 4
};

/** Used for emission of Namirial certificates
 */
typedef NS_ENUM(NSUInteger, T4RegistrationEnvelopeType){
    /** Not assigned: Not a Namirial envelope */
    T4RegEnvelopeNotAssigned = 0,
    /** Paper envelope: in this case the ercNumber must be specified in the T4RegistrationData structure, containing the enrollment phase security code. */
    T4RegEnvelopePaper = 1,
    /** Digital envelope: in this case the email must  be specified in the T4RegistrationData structure. During the enrollment phase the security code will be sent to this email address. */
    T4RegEnvelopeDigital = 4
};

@interface T4RegistrationData : NSObject

@property(nonatomic,retain,readonly) NSString* name;
@property(nonatomic,retain,readonly) NSString* surname;
@property(nonatomic,retain,readonly) NSString* fiscalCode;
@property(nonatomic,retain,readonly) NSString* gender; // M->Male, F->Female
@property(nonatomic,retain,readonly) NSString* dateOfBirth;
@property(nonatomic,retain,readonly) NSString* cityOfBirth;
@property(nonatomic,retain,readonly) NSString* provinceOfBirth;
@property(nonatomic,retain,readonly) NSString* stateOfBirth;
@property(nonatomic,retain,readonly) NSString* address;
@property(nonatomic,retain,readonly) NSString* postalCode;
@property(nonatomic,retain,readonly) NSString* city;
@property(nonatomic,retain,readonly) NSString* province;
@property(nonatomic,retain,readonly) NSString* state;
@property(nonatomic,retain,readonly) NSString* documentType; // CI → carta di identità, PA → patente, PASS → passaporto
@property(nonatomic,retain,readonly) NSString* documentNumber;
@property(nonatomic,retain,readonly) NSString* documentEmittedBy;
@property(nonatomic,retain,readonly) NSString* documentEmittedDate;
@property(nonatomic,retain,readonly) NSString* documentExpiryDate;
@property(nonatomic,retain,readonly) NSString* certUseLimit;
@property(nonatomic,retain,readonly) NSString* email;
@property(nonatomic,retain,readonly) NSString* emailPEC;
@property(nonatomic,retain,readonly) NSString* ercNuber;
@property(nonatomic,assign,readonly) T4RegistrationEnvelopeType envelopeType;
@property(nonatomic,retain,readonly) NSString* telephoneNumber;
@property(nonatomic,retain,readonly) NSString* nationality;
@property(nonatomic,retain,readonly) NSString* cadastreCode;

@end

@interface T4OtpInfo : NSObject

@property(nonatomic,retain,readonly) T4TokenId* tokenId;
@property(nonatomic,assign,readonly) T4OtpDeliveryMode otpMode;
@property(nonatomic,retain,readonly) NSString* status;
@property(nonatomic,retain,readonly) NSString* service;
@property(nonatomic,retain,readonly) NSString* telephoneNumber;
@property(nonatomic,retain,readonly) NSString* email;
@property(nonatomic,retain,readonly) NSString* serialNumber;
@property(nonatomic,retain,readonly) NSString* externalId;
@property(nonatomic,retain,readonly) NSString* label;
@property(nonatomic,retain,readonly) NSString* userId;

-(id)initWithResponse:(NSDictionary*)response error:(NSError**)error;

@end

@interface T4Registration : NSObject

@property(nonatomic,retain,readonly) T4RegistrationData* registrationData;
@property(nonatomic,retain,readonly) NSString* registrationId;
@property(nonatomic,retain,readonly) NSString* registrationIdCA;
@property(nonatomic,retain,readonly) NSString* providerName;
@property(nonatomic,retain,readonly) NSString* providerLabel;
@property(nonatomic,retain,readonly) NSString* registrationOffice;
@property(nonatomic,retain,readonly) NSString* registrationOfficeLabel;
@property(nonatomic,retain,readonly) NSDate*   createDate;
@property(nonatomic,assign,readonly) T4RegistrationStatus status;
@property(nonatomic,assign,readonly) NSInteger certificateId;
@property(nonatomic,assign,readonly) NSInteger credentialType;
@property(nonatomic,assign,readonly) NSInteger credentialSubtype;
@property(nonatomic,retain,readonly) T4OtpInfo* otpInfo;
@property(nonatomic,retain,readonly) NSString* otpAllowed;
@property(nonatomic,retain,readonly) NSString* error;
@property(nonatomic,assign,readonly) NSInteger authMode;
@property(nonatomic,retain,readonly) NSString* registrationXLST;

-(id)initWithResponse:(NSDictionary*)response error:(NSError**)error;

- (void)acceptCertificateIssue:(bool)accept showDate:(NSDate*)showDate completion:(void (^)(T4Response*))completion;

@end
