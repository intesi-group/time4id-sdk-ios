//
//  NSURLRequest+JSONRPC.h
//  Time4ID-Core
//
//  Created by francesco scalise on 19/11/23.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (JSONRPC)

- (NSString *)extractJSONRPCMethod;

@end
