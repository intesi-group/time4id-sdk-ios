//
//  VRRecognitionItem_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 03/05/17.
//  Copyright © 2017 Intesi Group SPA. All rights reserved.
//

#ifndef VRRecognitionItem_Internal_h
#define VRRecognitionItem_Internal_h

@interface VRRecognitionItem (LibraryInternalMethods)

-(id)initWithRecognition:(RRecognition*)recognition item:(RRecognitionItem*)item;

@end

#endif /* VRRecognitionItem_Internal_h */
