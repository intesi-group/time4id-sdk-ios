//
//  OtpConfigurationManager.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/2/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/BaseManagerWithOtpAccount.h>
#import <Time4ID/OtpResult.h>
#import <Time4ID/OtpSeedManager.h>

/**
 The OtpConfigurationManager allows the check and modification of the token's configuration for which it has been obtained.
 
  @see getOtpConfigurationManagerWithOtpAccountId
 */
@interface OtpConfigurationManager : BaseManagerWithOtpAccount

/**
 Sets the algorithm used during OTP calculation the default algoritm is SHA-256 (0)
 
    @param alg The algorithm identifier
 
    @return <i>true</i> if the operation was successful, otherwise </false>.
 
    @see T4MacAlg
 */
- (BOOL)setOtpAlgorithm:(T4MacAlg)alg;

/**
 Returns the MAC algorithm used for the OTP calculation by the current token
 
    @return The algorithm identifier
 
    @see T4MacAlg
 */
- (T4MacAlg)getOtpAlgorithm;

/**
 Sets the status of the Pin notification evidence.<br>When this flag is set to <i>true</i> the wrong PIN notifiction is disabled and the token seed is ciphered without padding, so when it is deciphered, it's impossible to say if the provided PIN is correct or not. <br>
     When this flag is set to <i>false</i> the wrong PIN notification is enabled and the token seed is ciphered with padding. This way, if an incorrect PIN is specified the error is evident by cheching the padding bytes. Ciphering the seed in this mode is not recommended because the token seed will be subject to brute force attack.<br>
 <i><font color="red">deprecated</font></i>: you should set the wrong PIN notification status when setting the seed for the token
 
    @param flag <i>true</i> to disable the wrong PIN notification, otherwise </false>
 
    @return  <i>true</i> if the operation is successful, otherwise </false>
 
    @see OtpSeedManager#setSeedEx
 */
- (BOOL)disableWrongPinNotification:(BOOL)flag __deprecated;

/**
 Returns the PIN error evidence.
 
    @return <i>true</i> if the wrong PIN notification is disabled, otherwise </false>
 */
- (BOOL)isWrongPinNotificationEnabled;

/**
 Sets he length of the generated OTP
 
    @param length length of the generated OTPs with the current token
 
    @return <i>true</i> if the wrong PIN notification is disabled, otherwise </false>
 */
- (BOOL)setOtpLength:(int)length;

/**
 Returns the length of the generated OTP
 
    @return length of the OTPs generated with the current token
 */
- (int)getOtpLength;

/**
 Sets the token's time step for the current token. The time step is the time a generated OTP is valid. After the time step is finished the generated OTP will have a new value.
 
 
    @param timeStep The time step in seconds
 
    @return <i>true</i> if the operation was successful, otherwise </false>
 */
- (BOOL)setTimeStep:(int)timeStep;

/**
    Returns the token's time step for the current token. The time step is the time a generated OTP is valid. After the time step is finished the generated OTP will have a new value.
 
    @return The time step in seconds
 */
- (int)getTimeStep;

/**
 Sets the limit for wrong PIN count before the PIN is locked. Effective only for tokens with wrong PIN notification active.
 
    @param maxTryPinCount number of wrong PIN required to lock the token.
 
    @return <i>true</i> if the operation was successful, otherwise </false>
 */
- (BOOL)setMaxTryPinCount:(int)maxTryPinCount;

/**
 Returns the wrong PIN limit for the current token
 
    @return number of wrong PIN required to lock the token.
 */
- (int)getMaxTryPinCount;

/**
 Set if the token is time based or event based
 
    @param otpType the otp type
 
    @return <i>true</i> if the operation was successful, otherwise </false>
 
    @see T4OtpType
 */
- (BOOL)setOtpType:(T4OtpType)otpType;

/**
 Returns if the token type is time based or event based
 
    @return The token type
 
    @see T4OtpType
 */
- (T4OtpType)getOtpType;

/**
 Enables the Begin-End algorithm for the current token
 
    @return <i>true</i> if the operation was successful, otherwise </false>
 */
- (BOOL)enableBE;

/**
 Disables the Begin-End algorithm for the current token
 
    @return <i>true</i> if the operation was successful, otherwise </false>
 */
- (BOOL)disableBE;

/**
 Returns the Begin-End algorithm abilitation status
 
    @return <i>true</i> if the Begin-End algorithm is enabled, otherwise </false>
 */
- (BOOL)isBEEnabled;

/**
 Enables or disables the raw OTP generation. Raw OTP is a long numeric string.
 
    @param flag <i>true</i> if the raw OTP mode is being enabled, </false> if it is being disabled.
 
    @return <i>true</i> if the operation was successful, otherwise </false>
 */
- (BOOL)enableRawOtp:(BOOL)flag;

/**
 Sets the PIN protection for the current token.<br>
  <i><font color="red">deprecated</font></i>: you should set the PIN protection level when setting the seed for the token
 
    @param pinProtection token PIN protection level
 
    @return <i>true</i> if the operation was successful, otherwise </false>
 
    @see T4PinProtection
    @see OtpSeedManager#setSeedEx
 */
- (BOOL)setPinProtection:(T4PinProtection)pinProtection __deprecated;

/**
 Returns the current token's PIN protection level.
 
    @return The token protection level.
 
    @see T4PinProtection
 */
- (T4PinProtection)getPinProtection;

/**
 * Sets the moving factor type (fixed/variable)
 * @param fixedMovingFactor <i>true</i> if the Moving Factor is fixed, <i>false</i> otherwise
 */
- (BOOL)setFixedMovingFactor:(BOOL)fixedMovingFactor;

/**
 * Get the Moving Factor type (fixed/variable)
 * @return <i>true</i> if the Moving Factor is fixed, <i>false</i> otherwise
 */
- (BOOL)isFixedMovingFactor;

@end
