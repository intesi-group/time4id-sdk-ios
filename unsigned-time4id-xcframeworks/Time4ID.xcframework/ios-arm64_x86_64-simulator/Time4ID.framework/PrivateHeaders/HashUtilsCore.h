//
//  HashUtils.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/2/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HashUtilsCore : NSObject

+ (NSString *)generateMD5HashFromString:(NSString *)stringToHash;
+ (NSData *)generateMD5DataFromString:(NSString *)stringToHash;
+ (NSString *)generateSHA1HashFromString:(NSString *)stringToHash;
+ (NSData *)generateSHA1DataFromString:(NSString *)stringToHash;
+ (NSString *)generateSHA256HashFromString:(NSString *)stringToHash;
+ (NSData *)generateSHA256DataFromString:(NSString *)stringToHash;
+ (NSString *)generateSHA512HashFromString:(NSString *)stringToHash;
+ (NSData *)generateSHA512DataFromString:(NSString *)stringToHash;

@end
