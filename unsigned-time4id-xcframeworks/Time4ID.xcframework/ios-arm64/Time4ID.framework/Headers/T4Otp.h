//
//  OtpResult.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 22/05/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Time4ID/T4Response.h>

/**
 *  T4Otp is a structure containing the OTP value and other members describing the OTP. Is composed by the following fields:
 */
@interface T4Otp : NSObject <NSCopying>

/**
 *  The calculated OTP value: Is the value to be used for user interactive operations
 */
@property (nonatomic, retain) NSString* _Nonnull otpValue;

/**
 *  The RAW OTP value: is the value to be used for implicit OTP operations
 */
@property (nonatomic, retain) NSString* _Nonnull rawValue;

/**
 *  The antifraudData value: is the value to be used for AntiFraud operations
 */
@property (nonatomic, retain) NSString* _Nullable antifraudData;

/**
 *  The start of the validity window for the calculated OTP
 */
@property (nonatomic, retain) NSDate* _Nullable timeBasedGenerationTime;

/**
 *  The OTP Time step in seconds
 */
@property (nonatomic, retain) NSNumber* _Nullable timeBasedStep;

/**
 *  The OTP type
 *
 *  @see T4OtpType
 */
@property (nonatomic, assign) T4OtpType type;

@end
