//
//  RCATemplateQuestion.h
//  VirtualRAO
//
//  Created by David Beduzzi on 18/07/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>
#import "VRao.h"

@interface RCATemplateQuestion : RLMObject
@property NSString*     key;
@property NSString*     parameterId;
@property NSString*     name;
@property NSString*     desc; // description
@property VRInfoType    infoType; // 0-text, 1-picture, 2-video
@property VRDataType    dataType;
@property VRVisibility  visibility;
@property NSData*       storedRule;
@property NSArray*      rule;
@property NSInteger     flag;
@property NSString*     defaultValue;
@property NSInteger     defaultChoiceAnswer;
@property NSArray*      choiceAnswer;
@property NSData*       storedChoiceAnswer;
@property NSArray*      weblinks;
@property NSData*       storedWeblinks;
@property NSString*     videoRequired;
@property NSInteger     videoForcedPosition;
@property bool          mandatory;
@property NSData*       sampleImage;
@property NSInteger     sectionNumber;
@property NSInteger     sectionIndex;

+(RCATemplateQuestion*)templateQuestionWithDictionary:(NSDictionary*)dictionary templateId:(NSString*)templateId error:(NSError**)pError;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<RCATemplateQuestion>
RLM_COLLECTION_TYPE(RCATemplateQuestion)
