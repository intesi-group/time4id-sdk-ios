//
//  RService.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 15/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>
#import "T4Response.h"
#import "T4Service.h"

@interface RService : RLMObject

// Identifier:
@property NSString* service;
@property NSString* ctxNode;

// Primary key:
@property NSString* serviceKey;

// Service properties:
@property NSString* domain;
@property NSString* serviceDescription;
@property NSData* dIcon;
@property NSData* dColor;
@property NSString* contentType;
@property NSData* dHash;
@property NSString* companyUrl;
@property NSString* serviceLabel;
@property NSInteger minProtection;
@property NSInteger minPinLength;
@property NSData* dOtpImage;
@property bool bIsTouchIdRequired;

-(RService*)initWithService:(NSString*)service ctxNode:(NSString*)ctxNode;
-(T4Error)saveService;
+(T4Error)deleteService:(NSString*)service ctxNode:(NSString*)ctxNode;

+(NSString*)keyWithService:(NSString*)service ctxNode:(NSString*)ctxNode;
+(CGFloat)toneWithData:(NSData*)data pos:(NSUInteger)pos;

@end
