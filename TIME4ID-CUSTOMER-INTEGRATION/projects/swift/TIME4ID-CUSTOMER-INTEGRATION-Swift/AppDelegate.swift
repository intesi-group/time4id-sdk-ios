//
//  AppDelegate.swift
//  TIME4ID-CUSTOMER-INTEGRATION-Swift
//
//  Created by francesco scalise on 30/11/22.
//

import UIKit
import Time4ID



/// Put the license into the license folder, add it to xcode project
let licenseFilename: String     = "<LICENSE FILENAME>"
let loginType: LoginType        = .unknown



@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil
        {
             return true
        }
        
        Task {
     
            do
            {
                let sdk = Time4IDDriver()
                
                print("⏳⏳⏳ TASK IS STARTING...\n\n")
                
                /// 1. Setup Time4ID SDK
                try await sdk.setupTime4ID(withLicense: licenseFilename)
                print("--- STEP 1 ---")
                print("✅✅✅ SDK SETUP DONE ✅✅✅")
                
                /// 2. Login User
                try await sdk.login(loginType: loginType)
                print("--- STEP 2 ---")
                print("✅✅✅ LOGIN DONE ✅✅✅")
                
                /// 3. Initialize Token
                let tokenID = try await sdk.initializeToken()
                print("--- STEP 3 ---")
                print("✅✅✅ TOKEN INITIALIZATION DONE ->\n   TOKEN_ID [\(tokenID)]\n✅✅✅")
                
                /// 4. Fetch Token
                var token = try await sdk.fetchToken(for: tokenID)
                print("--- STEP 4 ---")
                print("✅✅✅ TOKEN FETCH DONE ✅✅✅")
                
                /// 5. Generate Seed
                try await sdk.generateSeed(for: token)
                print("--- STEP 5 ---")
                print("✅✅✅ SEED GENERATION DONE ✅✅✅")
                
                /// 6. Activate Token
                try await sdk.activate(token)
                print("--- STEP 6 ---")
                print("✅✅✅ TOKEN ACTIVATION DONE ✅✅✅")
                
                /// 7. Generate OTP
                var otp = try await sdk.generateOTP(for: token)
                print("--- STEP 7 ---")
                print("✅✅✅ OTP GENERATION DONE ->\n   OTP [\(otp.otpValue)]\n✅✅✅")
                
                /// 8. Login User
                try await sdk.login(loginType: loginType)
                print("--- STEP 8 ---")
                print("✅✅✅ RE-LOGIN DONE ✅✅✅")
                
                /// 9. Fetch Token
                token = try await sdk.fetchToken(for: tokenID)
                print("--- STEP 9 ---")
                print("✅✅✅ TOKEN RE-FETCH DONE ✅✅✅")
                
                /// 10. Check Token Status
                let status = await token.isSynchronized()
                if status.1 == T4TokenSyncStatus.ok
                {
                    print("--- STEP 10 ---")
                    print("✅✅✅ TOKEN IS SYNCHRONIZED ✅✅✅")
                }
                else
                {
                    print("--- STEP 10 ---")
                    print("⚠️⚠️⚠️ TOKEN STATUS -> \(status.1) (Check `T4TokenSyncStatus`) ⚠️⚠️⚠️")
                }
                
                /// 11. Generate OTP
                otp = try await sdk.generateOTP(for: token)
                print("--- STEP 11 ---")
                print("✅✅✅ OTP RE-GENERATION DONE ->\n   OTP [\(otp.otpValue)]\n✅✅✅")
                
                print("\n\n🚀🚀🚀 TASK DONE")
            }
            catch
            {
               print("❌❌❌ \(error) - check `T4Error` ❌❌❌")
            }
        }
        
        return true
    }

}

// MARK: - SDK Driver

class Time4IDDriver { }

// MARK: - SDK Setup

extension Time4IDDriver {
    
    func setupTime4ID(
        withLicense filename: String
    ) async throws
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) in
            
            let t4Error = T4ID.initWithLicenseFile(
                filename,
                sharedKeychain: nil
            )
            
            if t4Error == T4_OK
            {
                /// SDK version
                let version = T4ID.getLibraryVersion()
                print("Time4ID SDK \(version) initialized")
                
                continuation.resume()
            }
            else
            {
                let error = Time4IDError.sdkSetupFailed(t4Error)
                continuation.resume(throwing: error)
            }
        }
    }
}

// MARK: - Login User

extension Time4IDDriver {
    
    func login(
        loginType: LoginType
    ) async throws
    {
        switch loginType {
        
        case .unknown:
            throw Time4IDError.loginTypeIsUnknown
        
        case .usernameAndPassword(let username, let password):
            try await login(username: username, password: password)
        
        case .accessToken(let accessToken):
            try await login(accessToken: accessToken)
        }
    }
    
    func login(
        username: String,
        password: String
    ) async throws
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) in
            
            T4User.loginUser(
                username,
                password: password,
                contextType: 0,
                nodeId: nil,
                rememberMe: true
            ) { response in
                
                if response.hasError()
                {
                    let error = Time4IDError.loginFailed(response.errorCode.intValue)
                    continuation.resume(throwing: error)
                }
                else
                {
                    continuation.resume()
                }
            }
        }
    }
    
    func login(
        accessToken: String
    ) async throws
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) in
            
            T4User.login(withAccessToken: accessToken) { response in
                
                if response.hasError()
                {
                    let error = Time4IDError.loginFailed(response.errorCode.intValue)
                    continuation.resume(throwing: error)
                }
                else
                {
                    continuation.resume()
                }
            }
        }
    }
}

// MARK: - Initialize Token

extension Time4IDDriver {
    
    func initializeToken() async throws -> T4TokenId
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<T4TokenId, Error>) in
            
            T4Token.initToken(
                nil,
                params: nil
            ) { response, tokenID in
                
                if response.hasError()
                {
                    let error = Time4IDError.tokenInitializationFailed(response.errorCode.intValue)
                    continuation.resume(throwing: error)
                }
                else
                {
                    continuation.resume(returning: tokenID!)
                }
            }
        }
    }
    
}

// MARK: - Fetch Token

extension Time4IDDriver {
    
    func fetchToken(
        for tokenID: T4TokenId
    ) async throws -> T4Token
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<T4Token, Error>) in
            
            if let token = T4Token.find(tokenID)
            {
                continuation.resume(returning: token)
            }
            else
            {
                let error = Time4IDError.tokenNotFound
                continuation.resume(throwing: error)
            }
        }
    }
}

// MARK: - Generate Seed

extension Time4IDDriver {
    
    func generateSeed(
        for token: T4Token
    ) async throws
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) in
            
            token.getSeed(
                nil,
                protection: .ppNone,
                pin: nil,
                pinErrorEvidence: false
            ) { response in
                
                if response.hasError()
                {
                    let error = Time4IDError.seedGenerationFailed(response.errorCode.intValue)
                    continuation.resume(throwing: error)
                }
                else
                {
                    continuation.resume()
                }
            }
        }
    }
}

// MARK: - Activate Token

extension Time4IDDriver {
    
    func activate(
        _ token: T4Token
    ) async throws
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<Void, Error>) in
            
            token.activate(nil) { response in
                
                if response.hasError()
                {
                    let error = Time4IDError.tokenActivationFailed(response.errorCode.intValue)
                    continuation.resume(throwing: error)
                }
                else
                {
                    continuation.resume()
                }
            }
        }
    }
}

// MARK: - Generate OTP

extension Time4IDDriver {
    
    func generateOTP(
        for token: T4Token
    ) async throws -> T4Otp
    {
        try await withCheckedThrowingContinuation { (continuation: CheckedContinuation<T4Otp, Error>) in
            
            token.generateOtp(nil) { otp, t4Error in
                
                if t4Error == T4_OK
                {
                    continuation.resume(returning: otp!)
                }
                else
                {
                    let error = Time4IDError.otpGenerationFailed(t4Error)
                    continuation.resume(throwing: error)
                }
            }
        }
    }
}

// MARK: - Error

private enum Time4IDError: Error {
    
    case sdkSetupFailed(Int)
    case loginFailed(Int)
    case tokenInitializationFailed(Int)
    case tokenNotFound
    case seedGenerationFailed(Int)
    case tokenActivationFailed(Int)
    case otpGenerationFailed(Int)
    case loginTypeIsUnknown
}

// MARK: - Login Type

enum LoginType {
    
    case unknown
    case usernameAndPassword(_ username: String, _ password: String)
    case accessToken(_ accessToken: String)
}
