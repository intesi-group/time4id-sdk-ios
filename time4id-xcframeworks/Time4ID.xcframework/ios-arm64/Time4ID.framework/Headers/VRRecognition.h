//
//  VRRecognition.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 03/05/17.
//  Copyright © 2017 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/T4Response.h>
#import <Time4ID/VRao.h>

@class VRRecognitionItem;



/**
 The recognition
 Represents a user's recognition in progress
 */
@interface VRRecognition : NSObject


/**
 Sets a callback function to be called by the SDK when a recognition status changes
 The SDK calls the specified function when one of the recognition's status is updated

 @param callback The callback receives a possible error code, and the ticket of the updated recognition
 */
+(void)setRecognitionStatusUpdateCallback:(void (^)(NSError* , NSString* ))callback;


/**
 Returns the CA resctiprion

 @return CA description
 */
-(NSString*)getDescription;


/**
 Returns an icon identifying the CA

 @return The CA icon
 */
-(UIImage*)getIcon;


/**
 A string identifying the CA provider

 @return The CA provider
 */
-(NSString*)getProviderName;


/**
 A label for the CA provider

 @return CA provider label
 */
-(NSString*)getProviderLabel;


/**
 Returns the CA office name

 @return the CA office name
 */
-(NSString*)getOfficeName;


/**
 Returns the CA office label

 @return the CA office label
 */
-(NSString*)getOfficeLabel;


/**
 The recognition language

 @return The recognition 2-letters language code
 */
-(NSString*)getLanguage;


/**
 Returns the recognition ticket

 @return Ticket identifying the recognition
 */
-(NSString*)getTicket;


/**
 Returns the recognition identifier
 The recognition identifier is filled after the recognition was submitted to for approval. Before the submission the returned value is _nil_

 @return The recognition identifier
 */
-(NSString*)getRecognitionId;


/**
 The recognition status

 @return The recognion status
 @see VRRecStatus
 */
-(VRRecStatus)getStatus;


/**
 The recognition start date

 @return The date when the recognition was first created
 */
-(NSDate*)getStartDate;


/**
 The last update date
 Returns the date when the recognition was last modified by the user

 @return The last update date
 */
-(NSDate*)getUpdateDate;


/**
 Returns the product typeq

 @return the product type
 @see VRProductType
 */
-(VRProductType)getProductType;


/**
 Returns the recognition mode

 @return The recognition mode
 @see VRRecognitionMode
 */
-(VRRecognitionMode)getRecognitionMode;


/**
 Return's the user's given name
 This value is normally available only when VRRecognitionMode is VRRecognitionDeVisu

 @return User's given name
 */
-(NSString*)getUserGivenName;


/**
 Returns the user's surname
 This value is normally available only when VRRecognitionMode is VRRecognitionDeVisu
 
 @return User's surname
 */
-(NSString*)getUserSurname;

/**
 Returns the user's email
 This value is normally available only when VRRecognitionMode is VRRecognitionDeVisu
 
 @return User's email
 */
-(NSString*)getUserEmail;


/**
 Returns the number of sections in the recognition

 @return number of sections
 */
-(NSInteger)getSections;


/**
 Returns the recognition items
 The recognition idems are all the elements that has to be actualized in order to submit the recognition

 @return the recognition items
 */
-(NSArray<VRRecognitionItem*>*)getRecognitionItems;


/**
 Returns the number of recognition items with the specified status
 Specify VRRecStatusAllItems to count all items

 @param status The filter status
 @return The number of items in the specified status
 @see VRRecStatus
 */
-(NSUInteger)getRecognitionItemsCountForStatus:(VRRecStatus)status;


/**
 Returns the recognition for the specified ticket if it is locally available
 If the recognition is not present in the local database, returns _nil_
 When _nil_ is returned, it's possible do retrieve the recognition by using _downloadRecognitionForTicket_

 @param ticket The recognition ticket
 @return The recognition, if locally avalialble. Otherwise _nil_
 */
+(VRRecognition*)getLocalRecognitionForTicket:(NSString*)ticket;


/**
 Returns a list of all the locally available recognitions

 @return The local recognitions
 */
+(NSArray<VRRecognition*>*)getLocalRecognitions;


/**
 ![Backend Call](connectm.png "Backend Call") Creates a local recognition based on the provided ticket

 @param ticket the recognition's ticket
 @param completion The callback returning the backend response and, if the call is successful, the downloaded recognition.
 */
+(void)downloadRecognitionForTicket:(NSString*)ticket completion:(void (^)(T4Response*,VRRecognition*))completion;


/**
 Deletes the specified recognition
 This api deletes all the local data corresponding to the recognition, included the multimedia files, like pictures or video

 @param ticket ticket identifying the recognition
 @return the _NSError_ if there was an error, _nil_ if all was OK.
 */
+(NSError*)deleteRecognitionForTicket:(NSString*)ticket;


/**
 Sets the user's explicit privacy agreement
 Call this API after displaying to the user the required privacy agreement, and the user explicitely approved/denied

 @param privacyAgreement Privacy agreement status set
 @return the _NSError_ if there was an error, _nil_ if all was OK.
 */
-(NSError*)setPrivacyAgreement:(bool)privacyAgreement;


/**
 ![Backend Call](connectm.png "Backend Call") Submits the recognition to the backend for revision and approval

 @param update update callback, called to display the upload completed percent
 @param completion completion callback called when the upload has finished. The _T4Response_ object describes the operation outcome
 */
-(void)submitWithUpdate:(bool (^)(CGFloat percent))update completion:(void (^)(T4Response*))completion;


/**
 ![Backend Call](connectm.png "Backend Call") Cancels the recognition process on the remote server
 This call allows the user to modify the recognition after submittion, by cancelling the submitted data on the backend. If the RAO started
 the approval process, this operation will fail.

 @param completion Operation outcome callback
 */
-(void)cancelRecognition:(void (^)(T4Response*))completion;


/**
 ![Backend Call](connectm.png "Backend Call") Returns the RAO noted for a rejected recognition
 When a recognition is rejected, use this API to retrieve the notes about the rejected recotnition items.
 The notes will be automatically saved and stored into the recognition items data.

 @param completion Operation outcome callback
 */
-(void)getRecognitionNotes:(void (^)(T4Response*))completion;


/**
 Esplicitely set the recognition status

 @param status The status to set
 @return The operation outcome
 */
-(NSError*)setStatus:(VRRecStatus)status;


/**
 Returns the size, in bytes, of the multimedia files (video, pictures) that will be uploaded to the backend when the recognition is submitted

 @return The multimedia data size in bytes.
 */
-(NSUInteger)getMMDataSize;


/**
 Returns the list of items to be completed in order to submit the recognition
 Call this API to inform the user about which items still have to be completed or corrected

 @return List of missing items
 */
-(NSArray<VRRecognitionItem*>*)getMissingItemsForSubmit;

@end
