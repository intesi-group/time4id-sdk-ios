//
//  SecurePinCertificateRealm.h
//  Time4ID_SDK
//
//  Created by Enrico Sallusti on 14/12/15.
//  Copyright © 2015 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>

@interface SecurePinCertificateRealm : RLMObject

@property NSString* ctxNode;
@property NSData* certValue;

-(id)initWithCtxNode:(NSString*)ctxNode certValue:(NSData*)certValue;


@end
