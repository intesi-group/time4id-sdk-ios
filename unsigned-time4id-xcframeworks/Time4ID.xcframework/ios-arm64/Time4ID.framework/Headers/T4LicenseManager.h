//
//  T4LicenseManager.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 17/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/** Type of license used to activate the SDK
 */
__attribute__((deprecated("Do not use this flag do distinguish DEV and PROD environments. Use isSupportUntrustedSSLRoot to check if untrusted certificates are supported.")))
typedef NS_ENUM(NSUInteger, T4LicenseType){
/** Production license: This license should be used in all the production applications */
    T4LicenseProd = 0,
/** Collaudation license: License used in test environment. */
    T4LicenseCol = 1,
/** Integration license: License used in development and integration environment. */
    T4LicenseInt = 2,
/** Integration license with IP: License used in development and integration environment. */
    T4LicenseIntIp = 3
};

@interface T4LicenseManager : NSObject

+(T4LicenseManager * _Nullable)sharedManager;

- (instancetype _Nullable)initWithLicense:(NSData *)license error:(NSError**)pError;
- (NSString * _Nullable)getTime4userEndpoint;
- (NSString * _Nullable)getTime4eidEndpoint;
- (NSString * _Nullable)getCtxNode;
- (NSString * _Nullable)getCidSeed;
- (NSString * _Nullable)getCustomer;
- (NSString * _Nullable)getAuthorization;
- (NSString * _Nullable)getAppName;
- (NSArray * _Nullable)getAlternateLogins;
- (BOOL)hideUserRegistration;
- (BOOL)hideGoogleAuthenticator;
- (BOOL)isOobEnabled;
- (BOOL)allowImplicitPin;
- (BOOL)hasSharedTokens;
- (bool)hasAlternateLogin:(NSString*)sAlternateLogin;
- (bool)isSupportUntrustedSSLRoot;
- (T4LicenseType)getLicenseType __attribute__((deprecated("Do not use this flag do distinguish DEV and PROD environments. Use isSupportUntrustedSSLRoot to check if untrusted certificates are supported.")));
- (NSArray * _Nullable)getTrustedHostList;
- (NSString * _Nullable)getCid;
- (NSString * _Nullable)getPrivacyAgreement:(NSString*)language;

-(NSString * _Nullable) oauthEndpoint;
-(NSString * _Nullable) clientId;
-(NSString * _Nullable) clientSecret;

@end

NS_ASSUME_NONNULL_END
