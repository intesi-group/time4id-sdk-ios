//
//  T4KeychainTokenStore.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 08/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import "T4TokenStore.h"
#import "T4TokenId.h"

// #define Time3IDSDK_KEYCHAIN_GROUP   @"T3BDF4PRXW.com.intesigroup.time4id.Time4IDSDK"

@interface T4KeychainTokenStore : T4TokenStore

-(NSArray<NSData*>*  _Nonnull)getAllDataItems;
-(NSArray<NSData*>* _Nonnull)getAllDataItemsExclude:(NSString * _Nullable)excludeProvider;
-(NSData* _Nullable)getDataItem:(T4TokenId* _Nonnull)tokenId;
-(OSStatus)setDataItem:(NSData* _Nonnull)dataItem forId:(T4TokenId* _Nonnull)tokenId user:(NSString* _Nonnull)sUser;
-(OSStatus)deleteDataItem:(T4TokenId* _Nonnull)tokenId user:(NSString* _Nonnull)sUser;

@end
