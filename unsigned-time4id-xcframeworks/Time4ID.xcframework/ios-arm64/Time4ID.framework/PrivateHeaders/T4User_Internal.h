//
//  T4User_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 11/03/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef T4User_Internal_h
#define T4User_Internal_h


#endif /* T4User_Internal_h */

@interface T4User (LibraryInternalMethods)

- (instancetype _Nullable)initWithLoggedUser;

+(void)completeLoginWithResponse:(T4Response* _Nonnull)respLogin commandParams:(NSDictionary* _Nonnull)params completion:(void(^ _Nonnull)(T4Response * _Nonnull))completion;



@end
