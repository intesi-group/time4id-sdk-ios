//
//  OtpAccountIdCore.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 08/01/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Identifies a token in the SDK internal database
 */
@interface OtpAccountIdCore : NSObject <NSCopying>

/**
 *  The user owner of the token
 */
@property (nonatomic, retain) NSString *username;

/**
 *  Cryptographic provider generating the token. The otpProvider and the otpId together uniquely identify the token.
 */
@property (nonatomic, retain) NSString *otpProvider;

/**
 *  The ID that uniquely identifies the token for the cryptographic provider. The otpProvider and the otpId together uniquely identify the token.
 */
@property (nonatomic, retain) NSString *otpId;

/**
 Initializes an accountId
 
   @param user     token owner
   @param provider token crypto provider
   @param otpid    token id for the provider
 
   @return the token identifier
 */
- (id)initWithUser: (NSString*) user otpProvider: (NSString*) provider otpId: (NSString*) otpid;

/**
 Creates an accountId
 
   @param user     token owner
   @param provider token crypto provider
   @param otpid    token id for the provider
 
   @return the token identifier
 */
+ (OtpAccountIdCore*)accountIdWithUser: (NSString*) user otpProvider: (NSString*) provider otpId: (NSString*) otpid;

@end
