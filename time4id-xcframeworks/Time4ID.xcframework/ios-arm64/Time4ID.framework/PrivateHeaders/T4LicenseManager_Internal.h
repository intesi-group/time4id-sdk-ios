//
//  T4LicenseManager_Internal.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 10/08/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#ifndef T4LicenseManager_Internal_h
#define T4LicenseManager_Internal_h

@interface T4LicenseManager (LibraryInternalMethods)

+(BOOL)verifyEndToEndCertificate:(NSData*)endToEndCert error:(NSError **)error;
-(NSString*)getAlternateLoginNode:(NSString*)alternateLogin;
-(NSString*)getVRaoEndpoint;

@end

#endif /* T4LicenseManager_Internal_h */
