//
//  T4Command.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 15/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/T4LicenseManager.h>
#import <Time4ID/T4Response.h>

#define TIMEOUT_CONNECTION  30
#define ENDPOINT_T4USER     [[T4LicenseManager sharedManager] getTime4userEndpoint]
#define ENDPOINT_T4ID       [[T4LicenseManager sharedManager] getTime4eidEndpoint]

@interface T4Command : NSObject <NSURLSessionDataDelegate>


+(void)commandWithUrl:(NSString* _Nonnull)sUrl command:(NSString* _Nonnull)command params:(NSDictionary* _Nonnull)dParams secureParams:(NSArray* _Nullable)secureParams completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;
+(void)commandWithUrl:(NSString* _Nonnull)sUrl command:(NSString* _Nonnull)command params:(NSDictionary* _Nonnull)dParams secureParams:(NSArray* _Nullable)secureParams supportedMimeTypes:(NSArray<NSString*>* _Nullable)aSupportedMimeTypes completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;
-(id _Nonnull)initWithUrl:(NSString* _Nonnull)sUrl command:(NSString* _Nonnull)command params:(NSDictionary* _Nonnull)dParams secureParams:(NSArray* _Nullable)secureParams supportedMimeTypes:(NSArray<NSString*>* _Nullable)aSupportedMimeTypes  completion:(void (^ _Nonnull)(T4Response* _Nonnull))completion;
- (void)execute;

// Utility:
+(void)getEndToEndCertificate:(void (^ _Nonnull)(NSData* _Nullable, T4Response* _Nonnull))completion;
+(NSMutableDictionary* _Nonnull)dictionaryWithSessionAddCidCia:(bool)bAddCidCia;
+(NSData* _Nullable)getEncryptionKey;

// Cookie commands:
+ (void)setCookie:(NSString * _Nonnull)cookie __deprecated_msg("Use +setCookie:error: instead");

/**
 Sets a cookie in the headers dictionary.

 This function sets a given cookie string to the headers dictionary after validating it against the standard HTTP cookie format as defined in RFC 6265.

 @param cookie The cookie string to be set.
 @param error Pointer to an NSError object, which will be set if the cookie validation fails.
 @return Returns YES if the cookie is successfully set, NO otherwise.

 Reference: RFC 6265 - HTTP State Management Mechanism
 URL: https://datatracker.ietf.org/doc/html/rfc6265

 Note: This function assumes that the cookie string provided conforms to the syntax specified in RFC 6265, which includes rules about valid characters, key-value pair format, and other cookie attributes. The function performs a basic validation against invalid characters and key-value pair formatting. For more comprehensive validation, further checks based on the complete RFC specifications might be required.
 */
+ (BOOL)setCookie:(NSString * _Nonnull)cookie error:(NSError* _Nullable * _Nonnull)error;

+(void)setHttpHeader:(NSString* _Nonnull)header value:(NSString* _Nonnull)value;
+(void)updateCookie: (NSString* _Nonnull) cookie addFields:(bool)addFields;
+(void)removeHttpHeader:(NSString* _Nonnull)header;
+(NSDictionary* _Nonnull)getHttpHeaders;
+(NSMutableDictionary* _Nullable)getCookieComponents;


+(NSArray<NSString*>* _Nullable)getSessionLog;
+(void)addExtraLogInfo:(NSString* _Nonnull)extraLogInfo;

#ifdef DEBUG
+(void)testEndToEndCertificateError;
#endif

@end
