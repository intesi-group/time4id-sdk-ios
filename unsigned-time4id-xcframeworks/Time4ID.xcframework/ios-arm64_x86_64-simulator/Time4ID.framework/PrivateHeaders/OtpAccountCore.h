//
//  OtpAccount.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/4/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/OtpConfigurationCore.h>
#import <Time4ID/OtpDataCore.h>
#import <Time4ID/OtpAccountIdCore.h>
#import <Time4ID/OtpAccountDescription.h>
#import <Time4ID/T4Response.h>

// active (1), locked (2), to be resynched (3), next token required (4) disabled (0)

/**
 *  Contains the data of the token inside the internal SDK database
 */
@interface OtpAccountCore : NSObject <NSCopying>

@property (nonatomic) int id;

/**
 *  The token's identifier
 */
@property (nonatomic, retain) OtpAccountIdCore *otpAccountId;

/**
 *  Token creation time
 */
@property (nonatomic, retain) NSDate *creationDate;

/**
 *  The token status
 */
@property (nonatomic) T4TokenStatus otpState;

/**
 The token configuration
 
  @see OtpConfigurationCore
 */
@property (nonatomic, retain) OtpConfigurationCore *otpConfiguration;
@property (nonatomic, retain) OtpDataCore *otpData;

/**
 The token device identifier. This value is used only with the Begin-End algorithm.
 */
@property (nonatomic) int deviceId;
@property (nonatomic, retain) OtpAccountDescription *otpAccountDescription;

/**
 The token's <i>uniqueTokenId</i>. The <i>uniqueTokenId</i> is a unique identifier for the token, which can be used as an alternative to the combination of otpProvider and otpid.
 */
@property (nonatomic, retain) NSString *uniqueTokenId;

@end
