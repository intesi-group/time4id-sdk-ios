//
//  TokenStatus.h
//  T4M_Connector
//
//  Created by Matteo Argento on 22/07/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Describe the token status
 */
@interface TokenStatus : NSObject

/**
 *  Token status
 */
@property (nonatomic) int status;

/**
 *  Number of wrong PIN trials after which the token will be locked
 */
@property (nonatomic) int errorThreshold;

@end
