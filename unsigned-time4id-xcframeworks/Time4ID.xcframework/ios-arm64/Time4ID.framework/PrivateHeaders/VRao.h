//
//  VRao.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 17/03/17.
//  Copyright © 2017 Intesi Group SPA. All rights reserved.
//

#import <Time4ID/T4Response.h>

#define ERR_DOMAIN_VRAO @"ErrorDomainVirtualRAO"

#define VRERR_INVALID_RESPONSE 100
#define VRERR_APP_INTERNAL 101
#define VRERR_INVALID_STATUS 102
#define VRERR_TEMPLATE_VERSION 103
#define VRERR_REC_NOT_FOUND 513

/**
 Recognition statuses

 - VRRecStatusDeleted: The recognition was deleted
 - VRRecStatusNew: New recognition (not submitted)
 - VRRecStatusSubmitted: The recognition was submitted for review
 - VRRecStatusInReview: The recognition review has started
 - VRRecStatusInReviewOk: The RAO review was accepted
 - VRRecStatusRejected: The recognition was rejected by the RAO
 - VRRecStatusApproved: The recognition was approved by the RAO
 - VRRecStatusInterviewReq: The RAO requested an interview with the subject
 - VRRecStatusResubmitted: The recognition was resubmitted after a rejection and data correction
 - VRRecStatusApprovedRAO: The recognition is RAO approved
 - VRRecStatusCompleted: (app internal status) the recognition was locally completed but not yes submitted
 - VRRecStatusInProgress: (app internal status) the recognition completion is in progress
 - VRRecStatusAllItems: (app internal status) when querying for recognition with a partitular status, get all the recognitions
 */
typedef NS_ENUM(NSInteger, VRRecStatus) {
    VRRecStatusDeleted      = -1,
    VRRecStatusNew          = 0,
    VRRecStatusSubmitted    = 1,
    VRRecStatusInReview     = 2,
    VRRecStatusInReviewOk   = 3,
    VRRecStatusRejected     = 4,
    VRRecStatusApproved     = 5,
    VRRecStatusInterviewReq = 6,
    VRRecStatusResubmitted  = 7,
    VRRecStatusApprovedRAO  = 8,
    VRRecStatusCompleted    = 90,
    VRRecStatusInProgress   = 91,
    VRRecStatusAllItems     = 99
};


/**
 Recognition ticket status: tells the status of the ticket

 - VRTicketNotFound: The specified ticket wasn't found on the server
 - VRTicketNew: New ticket
 - VRTicketConsumed: The ticket has already been consumed
 - VRTicketPending: Pending ticket
 */
typedef NS_ENUM(NSInteger, VRTicketStatus){
    VRTicketNotFound = -1,
    VRTicketNew      = 0,
    VRTicketConsumed = 1,
    VRTicketPending  = 2
};


/**
 Type of information to be provided for the recognition

 - VRInfoTypeData: The information is data specified as a string value
 - VRInfoTypePicture: The information is a local path to a picture
 - VRInfoTypeVideo: The information is a local path to a video
 - VRInfoTypeSpecial: The information is specific to the item
 - VRInfoTypeBinary: The information is a local path to a binary file (for instance a PDF file)
 */
typedef NS_ENUM(NSInteger, VRInfoType){
    VRInfoTypeData      = 0,
    VRInfoTypePicture   = 1,
    VRInfoTypeVideo     = 2,
    VRInfoTypeSpecial   = 3,
    VRInfoTypeBinary    = 4
};


/**
 Special flags for recognition items

 - VRRecItemFlagNone: No flag
 - VRRecItemFlagIncludeOldNations: Flag for the field reuiring to specify a nation code (ISO 2-letter code). If this is specified, also non-existinc nations should be proposed to the user
 - VRRecItemFlagAuthorization: The item requires explicit authorization
 */
typedef NS_ENUM(NSInteger, VRRecItemFlags){
    VRRecItemFlagNone      = 0,
    VRRecItemFlagIncludeOldNations   = 1,
    VRRecItemFlagAuthorization = 2
};


/**
 Recognition item data type

 - VRDataTypeNone: Data type not specified
 - VRDataTypeYesNo: The required data is "true" or "false"
 - VRDataTypeString: The required data is a generic string
 - VRDataTypeNumeric: The required data is a numeric string
 - VRDataTypeEMail: The required data is an email address
 - VRDataTypeChoice: The required data is a string value chosen in a specified range
 - VRDataTypeDate: The required data is a date/time expressed as seconds since 1/1/1970
 - VRDataTypeCapitalString: The required data is a capital letters string
 - VRDataTypeTelephone: The required data is a telephone number
 - VRDataTypeNation: The required data is a nation ISO code (2-letters)
 - VRDataTypeNot: The data is a note, a multiline text exchanged between RAO and the user
 - VRDataTypeGeoPosition: The data is a JSON containing an encoded geoposition: { "type":"google", "lat":12.123456, "lng":-12.123456 }
 */
typedef NS_ENUM(NSInteger, VRDataType){
    VRDataTypeNone      = 0,
    VRDataTypeYesNo     = 1,
    VRDataTypeString    = 2,
    VRDataTypeNumeric   = 3,
    VRDataTypeEMail     = 4,
    VRDataTypeChoice    = 5,
    VRDataTypeDate      = 6,
    VRDataTypeCapitalString = 7,
    VRDataTypeTelephone = 8,
    VRDataTypeNation    = 9,
    VRDataNumbersAndPunctuation = 10,
    VRDataTypeNote      = 11,
    VRDataTypeGeoPosition = 12
};


/**
 Product type

 - VRProductACS: ACS credential (automatic signature)
 - VRProductQCS: QCS credential (qualified signature)
 */
typedef NS_ENUM(NSInteger, VRProductType){
    VRProductACS  = 3,
    VRProductQCS  = 6
};

/**
 Item visibility

 - VRProductACS: ACS credential (automatic signature)
 - VRProductQCS: QCS credential (qualified signature)
 */
typedef NS_ENUM(NSInteger, VRVisibility){
    VRVisibilityMandatory  = 0,
    VRVisibilityOptional  = 1,
    VRVisibilityHidden  = 2
};

/**
 The recognition mode

 - VRRecognitionStore: The recognition was started by a store by the user and is performed by the user
 - VRRecognitionInternal: The recognition was started by a RAO and is performed by the user
 - VRRecognitionDeVisu: The recognition was started by a RAO and is performed by the RAO
 */
typedef NS_ENUM(NSInteger, VRRecognitionMode){
    VRRecognitionStore     = 1,
    VRRecognitionInternal  = 2,
    VRRecognitionDeVisu    = 3
};

@interface VRao : NSObject

/**
 ![Backend Call](connectm.png "Backend Call") Creates a new Time4Mind account. The account doesn't have to be confirmed thanks to the provided _ticket_.

 @param username   Username
 @param password   Password
 @param ticket The transaction ticket
 @param completion Competion block, called after the account creation was completed. The completion block receives the operation outcome and, if it was successful, the new user.
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4U_ACCOUNT_ALREADY_REGISTERED | 514/518 | This account is already registered |
 | T4U_TICKED_OP_NOT_ALLOWED | 533 | Operation not allowed: the ticket is not enabled for an account creation |
 | T4U_TICKET_ALREADY_USED  | 566 | Ticket is invalid because it has already been used |
 | T4U_TICKET_ACCOUNT_CREATED | 580 | Ticket was already used to create an account |
 | T4U_TICKET_NOT_FOUND | 768 | Ticket not found |
 
*/
+(void)createAccountWithUsername:(NSString*)username password:(NSString*)password ticket:(NSString*)ticket completion:(void (^)(T4Response*))completion;


/**
 ![Backend Call](connectm.png "Backend Call") Returns an array of tickets present for the current user.

 @param completion The callback returning the outcome and an array of tickets information
 */
+(void)listUserTicketsCompletion:(void (^)(T4Response*, NSArray<NSDictionary*>*))completion;


/**
 ![Backend Call](connectm.png "Backend Call") Downloads the information required for the registration corresponding to the given _ticket_

 @param ticket The transaction ticket
 @param completion The completion block. Contains the recognition information
 
 | Error   | Value    | Description |
 |---------|--------|---------|
 | T4E_MISSING_PARAM | 258 | Missing mandatory parameters |
 | T4U_SESSION_EXPIRED | 515 | User session is expired, a session renewal is rewuired |
 | T4U_SESSION_INVALID | 516 | User session not found, a login is required |
 | T4U_TICKET_ALREADY_USED  | 566 | Ticket is invalid because it has already been used |
 | T4U_TICKET_NOT_FOUND | 768 | Ticket not found |
 
 */
//+(void)getRecognitionInfoWithTicket:(NSString*)ticket completion:(void (^)(T4Response*))completion;


/**
 ![Backend Call](connectm.png "Backend Call") sumbits the recognition to backend and receives the recognition identifier

 @param ticket ticket identifying the recognition
 @param recognitionData the text data
 @param mmFileData multimedia data
 @param completion completion callback
 */
//+(void)submitRecognition:(NSString*)ticket recognitonData:(NSArray*)recognitionData mmFileData:(NSArray*)mmFileData completion:(void (^)(T4Response*))completion;

// TEST!
//+(void)completeRecognition:(NSString*)recognitionId completion:(void (^)(T4Response*))completion;

@end
