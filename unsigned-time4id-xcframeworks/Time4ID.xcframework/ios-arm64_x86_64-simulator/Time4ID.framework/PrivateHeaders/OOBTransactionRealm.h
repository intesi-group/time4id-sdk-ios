//
//  OOBTransactionRealm.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 31/07/15.
//  Copyright (c) 2015 Intesi Group SPA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>
#import <Time4ID/OOBRequest.h>

@interface OOBTransactionRealm : RLMObject

@property NSString*                  transactionId;
@property NSInteger                  status;

// AccountId
@property NSString*                  username;
@property NSString*                  otpId;
@property NSString*                  otpProvider;
@property NSString*                  uniqueTokenIdentifier;

// Transaction settings:
@property NSString*                  service;
@property NSString*                  title;
@property NSString*                  sender;
@property BOOL                       hide;
@property NSInteger                  tokenSelect;
@property NSInteger                  credAuthMode;
@property NSInteger                  selectConfirm;
@property NSDate*                    time;
@property NSString*                  contentType;
@property NSData*                    content;
@property NSData*                    digest;
@property BOOL                       base64;
@property NSData*                    param;
@property NSInteger                  authType;
@property NSInteger                  opType;
@property NSInteger                  tokenType;
@property NSDate*                    expiryDate;
@property NSString*                  ctxNode;
@property NSString*                  pushSignatureInfo;
@property NSString*                  pushSignatureResponse;
@property NSInteger                  failureCode;
@property NSString*                  failureMessage;
@property NSData*                    extraInfo;
@property NSInteger                  rejectionReasonCode;
@property NSString*                  rejectionReason;

// Credentials info:
@property NSData*                    credentials;

-(id)initWithOOBRequest:(OOBRequest*)request user:(NSString*)user;
-(OOBRequest*)OOBRequest;

@end
