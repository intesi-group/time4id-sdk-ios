/**
 * This file is part of the ocra-implementations package.
 *
 * More information: https://github.com/SURFnet/ocra-implementations/
 *
 * @author Ivo Jansch <ivo@egeniq.com>
 * 
 * @license See the LICENSE file in the source distribution
 */
#import <Foundation/Foundation.h>

enum {
    OCRANumberOfDigitsTooLargeError = 100
};

@interface OCRA : NSObject {
    
}

+(NSData*) hexToBytes: (NSString*) str;

+(NSString *) generateOCRAForSuite:(NSString*) ocraSuite
                        key:(NSData*) key
                    counter:(NSString*) counter
                   question:(NSString*) question
                   password:(NSString*) password
         sessionInformation:(NSString*) sessionInformation
                  timestamp:(NSString*) timeStamp
                      error:(NSError**) error;

@end
