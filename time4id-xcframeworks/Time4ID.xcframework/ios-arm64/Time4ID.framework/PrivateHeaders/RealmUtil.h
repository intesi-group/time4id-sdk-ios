//
//  RealmUtil.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 27/03/17.
//  Copyright © 2017 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SCHEMA_VERSION  35

#import <Realm/Realm.h>

RLMRealm* getRealm(void);

RLMRealm* getRealmEx( NSError** pError);
