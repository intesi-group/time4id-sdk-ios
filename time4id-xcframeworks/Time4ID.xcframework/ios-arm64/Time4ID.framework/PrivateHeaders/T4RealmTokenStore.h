//
//  T4RealmTokenStore.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 12/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import "T4TokenStore.h"

@interface T4RealmTokenStore : T4TokenStore

-(NSArray<NSData*>* _Nonnull)getAllDataItems;
-(NSArray<NSData*>* _Nonnull)getAllDataItemsExclude:(NSString * _Nullable)excludeProvider;
-(NSData* _Nullable)getDataItem:(T4TokenId* _Nonnull)tokenId;
-(OSStatus)setDataItem:(NSData* _Nonnull)dataItem forId:(T4TokenId* _Nonnull)tokenId user:(NSString* _Nonnull)sUser;
-(OSStatus)deleteDataItem:(T4TokenId* _Nonnull)tokenId user:(NSString* _Nonnull)sUser;
@end
