//
//  OtpSeed.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/9/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Contains the information about the token seed. <b>The seed in this structure is NOT ciphered!</b>
 */
@interface OtpSeedCore : NSObject

/**
 *  Binary seed data
 */
@property (nonatomic, retain) NSData *seedData;

/**
 *  Hexadecimal encoded seed data
 */
@property (nonatomic, retain) NSString *seed;

/**
 *  Random filler for the seed when it will be ciphered on the DB
 */
@property (nonatomic, retain) NSData *randomFillerData;

@end
