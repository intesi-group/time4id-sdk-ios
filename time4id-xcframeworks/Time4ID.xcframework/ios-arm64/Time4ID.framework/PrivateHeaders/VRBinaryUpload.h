//
//  VRBinaryUpload.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 09/05/17.
//  Copyright © 2017 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/RRecognition.h>

@interface VRBinaryUpload : NSObject  <NSURLConnectionDelegate>

+(void)binaryUploadRecognition:(RRecognition* _Nonnull)recognition tokens:(NSArray<NSDictionary*>* _Nonnull)tokens toUrl:(NSURL* _Nonnull)url subpath:(NSString* _Nullable)subpath withUpdate:(bool (^ _Nullable)(CGFloat percent))update completion:(void (^ _Nonnull)(NSError* _Nullable))completion;

@end
