//
//  T4PushSignatureDocAuth.h
//  Time4ID_SDK
//
//  Created by Enrico Sallusti on 06/05/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    /**
     * Default
     */
    None = 0,
    
    Time4Mind = 1,
    
    BasicAuthentication = 2,
    
    SingleSignOn = 3,
    
} T4PSDocAuthType;

@interface T4PushSignatureDocAuth : NSObject

-(id _Nonnull)initWithResponse:(NSDictionary* _Nonnull)info error:(NSError* _Nullable * _Nullable)pError;

-(id _Nonnull)initWithDocumentAuthType:(T4PSDocAuthType)documentAuthType;

/**
 Returns the Document Authentication type
 
 @return The Auth Type
 */
-(T4PSDocAuthType)getDocAuthType;

/**
 Returns the Document Single Sign On Parameters
 
 @return The Single Sign On Parameters
 */
-(NSArray* _Nullable)getSsoParams;

/**
 Returns the Document Single Sign On Login String
 
 @return The Single Sign On Login String
 */
-(NSString* _Nullable)getSsoLoginString;

/**
 Returns the Document Single Sign On Parameters type
 
 @return The Single Sign On Parameters type
 */
-(NSArray* _Nullable)getSsoParamsType;

/**
 Returns the Document Single Sign On Parameters is password condition
 
 @return The Single Sign On Parameters is password condition
 */
-(NSArray* _Nullable)getSsoParamIsPassword;

/**
 Returns the Document Single Sign On Template
 
 @return The Single Sign On Template
 */
-(NSString* _Nullable)getSsoTemplate;

/**
 Returns the Document Single Sign On Login Mode
 
 @return The Single Sign On Login Mode
 */
-(NSString* _Nullable)getSsoLoginMode;

/**
 Returns the Document Time4Mind Session Cookie
 
 @return The Time4Mind Session Cookie
 */
-(NSString* _Nullable)getT4mCookie;


@end
