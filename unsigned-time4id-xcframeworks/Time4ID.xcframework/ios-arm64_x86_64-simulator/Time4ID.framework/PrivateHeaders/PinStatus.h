//
//  PinStatus.h
//  T4M_Connector
//
//  Created by Matteo Argento on 22/07/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  The PIN status
 */
@interface PinStatus : NSObject

/**
 *  Pin status, indicates if the token is locked
 */
@property (nonatomic) int status;

/**
 *  Additional information in case the token is locked
 */
@property (nonatomic) int lockType;

/**
 *  The time when the token was locked
 */
@property (nonatomic, retain) NSDate *lockDate;

/**
 *  Number of wrong PIN trials after which the token will be locked
 */
@property (nonatomic) int errorThreshold;


@end
