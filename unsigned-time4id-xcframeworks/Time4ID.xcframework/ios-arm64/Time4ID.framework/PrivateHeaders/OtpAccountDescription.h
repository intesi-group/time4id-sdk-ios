//
//  OtpAccountDescription.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 21/07/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/OtpService.h>

typedef enum
{
    SMS = 1,
    EMAIL = 2,
    APP = 4
} srvTokenBearer;

@interface OtpAccountDescription : NSObject <NSCopying>

@property (nonatomic, retain) OtpService *service;
@property (nonatomic, retain) NSString *label;
@property (nonatomic, retain) NSString *serviceName;
@property (nonatomic, retain) NSString *ctxNode;
@property (nonatomic) srvTokenBearer srvTokenBearer;

@end
