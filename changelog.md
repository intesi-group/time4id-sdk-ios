# Changelog
All notable changes to this project will be documented in this file.

## [3.8.2] - 2024-12-06
### Added
- (NSDate *)getNextOtpAvailableDate methods added in token management, in order to have the next available window to use OTP in all the generation otp flows
### Changed
- Fixed getNextOtpAvailableTime method, in order to have the next available window to use OTP in all the generation otp flows
- getNextOtpAvailableTime methods documentation, fit the current implementation

## [3.8.1] - 2024-09-10
### Changed
- Updated OpenSSL dependency to version 3.1.5
- Upgraded Realm DB to version 10.51.0
- getQRCertificate API rework: now in login phase retrieve the QR code certificate and store it in the local DB only if there isn't a certificate saved.
- parseDynamicLinkMessage API rework: After a QRcode was scanned, if no cert is present in the DB or the cert is not valid, the getQRCertificate API will be called to retrieve the cert.

## [3.7.5] - 2024-03-25
### Added
- Privacy manifest management compliance, for Apple compliance:
https://developer.apple.com/documentation/bundleresources/privacy_manifest_files
### Changed
- Upgraded OpenSSL dependency to version 3.1.4

## [3.7.2] - 2024-02-23
### Added
 - unrestrictedMethods: added whitelist for "retrieveUserInfo" and "activateUser" API
 - Added unsigned XCFramework packages into release package uploaded
### Changed
 - Fixed sessionKey management into loginWithSessionKey() API and activateUser() API in order to use injected sessionKey

## [3.7.1] - 2024-02-12
### Added
- New verification CA chain management for license file
- Added AFNetworwing dependency for http management rework
- HTTP multithread management rework
### Changed
- fixed UTC support into CA verification management
- License CA verification management
- Error management rework for error code -3000/-3001 and 1027/1028: Error on secure cert download now is handle with a clean logout

## [3.7.0] - 2024-01-25
### Added
- New verification CA chain management for license file
- Added AFNetworwing dependency for http management rework
- HTTP multithread management rework
### Changed
- License CA verification management
- Error management rework for error code -3000/-3001 and 1027/1028: Error on secure cert download now is handle with a clean logout

## [3.5.3] - 2023-10-20
### Changed
 - Logout API improvements

## [3.5.2] - 2023-08-29
### Added
 - iOS17 support management
 - iOS17 compliance with code signature for dependencies by Intesi Group

## [3.5.1] - 2023-08-22
### Changed
 - iOS17 HTTP client compliance

## [3.4.0] - 2023-03-13
### Changed
 - iOS Deployment Target updated to 12.0 (before it was 11.3)

## [3.3.0] - 2023-03-13
### Changed
 - Realm DB updated to version 10.37.0

## [3.2.0] - 2023-03-16
### Added
- JWT Login (check the updated sample project!)
```swift

T4User.login(withAccessToken: accessToken) { [...] }

```

## [3.1.0] - 2023-03-13
### Changed
 - OpenSSL 1.1.1t

## [3.0.1] - 2023-02-16
### Changed
 - isSynchronized API fixed: callback fixed when isSynchronazed returns token already synched

## [3.0.0] - 2023-02-01
### Changed
 - New binary management

## [2.25.3] - 2022-11-30
### Changed
 - OpenSSL updated to v1.1.1900

## [2.24.0] - 2022-08-31
### Added
- SSL EndToEnd encryption certs management added: multi certs management

## [2.20.4] - 2021-03-31
### Changed
 - Updated Realm version from 3.21 to 10.5.2
 - Fixed Token.enrollForService API that on certain circumstances does not guarantee the complete token enrollment (was left in "T4TokenStatusActivationRequired" status)

## [2.20.3] - 2021-03-23
### Added
- Added new error constant T4SDK_TOKEN_EXPIRED with int value 13056, in order to handle session expired after remember me token also expired
- Now the SDK can be used with the latest version of XCode, using the XCFramework build system

## [2.20.0] - 2021-01-12
### Changed
- Check if generated OTP is empty

