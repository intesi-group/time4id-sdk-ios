//
//  T4Config.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 08/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "T4Response.h"

#define CF_USER         @"user"
#define CF_SESSION      @"session"
#define CF_LOGINTYPE    @"loginType"
#define CF_USERTOKEN    @"userToken"
#define CF_SESSIONTOKEN @"sessionToken"
#define CF_REMEMBERME   @"rememberMe"
#define CF_TOKENPOSITION @"tokenPosition"

#define CF_E2E_CERT     @"endToEndCertificate"
#define CF_E2E_ENCRYPT_SESSION  @"endToEndEncryptionSession"
#define CF_E2E_ENCRYPT_KEY  @"endToEndEncryptionKey"

#define CF_SRV_INFO_RETRIEVED   @"serviceInfoRetrieved"
#define CF_SRV_TOKEN_BEARER     @"serviceTokenBearer"
#define CF_SRV_TOKEN_REQUIRED   @"serviceTokenRequired"

#define CF_SVR_HALFKEY  @"svrHalfkey"
#define CF_SALT         @"T4eID_CORE_RANDOM_SALT"
#define CF_USERHALFKEY  @"userHalfkeyUsed:%@"

@interface T4Config : NSObject

+(T4Config* _Nonnull)sharedConfig;

-(T4Error)setStringParam:(NSString* _Nonnull)key value:(NSString* _Nonnull)value;
-(T4Error)setDataParam:(NSString* _Nonnull)key value:(NSData* _Nonnull)value;
-(T4Error)setIntegerParam:(NSString* _Nonnull)key value:(NSInteger)value;

-(T4Error)deleteParam:(NSString* _Nonnull)key;

-(NSString* _Nonnull)getCid;
-(NSString* _Nonnull)getCia;
-(NSString* _Nullable)getStringParam:(NSString* _Nonnull)key;
-(NSInteger)getIntegerParam:(NSString* _Nonnull)key;
-(NSData* _Nullable)getDataParam:(NSString* _Nonnull)key;

// Migration to 2.0
-(T4Error)migrateLoggedUser;

@end
