//
//  LicenseManager.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 05/06/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Time4ID/CWLSynthesizeSingleton.h>
#import <Time4ID/HashUtilsCore.h>

/**
 *  Type of license used to activate the SDK
 */
typedef NS_ENUM(NSUInteger, T4LicenseType){
    /**
     *  Production license: This license should be used in all the production applications
     */
    T4LicenseProd = 0,
    /**
     *  Collaudation license: License used in test environment.
     */
    T4LicenseCol = 1,
    /**
     *  Integration license: License used in development and integration environment.
     */
    T4LicenseInt = 2,
    /**
     *  Integration license with IP: License used in development and integration environment.
     */
    T4LicenseIntIp = 3
};

/**
 *  LicenseManager gives informations about the license used to register the SDK.
 */
@interface LicenseManager : NSObject
{
    NSDictionary *licenseContent;
    int lastReadTimestamp;
    NSArray* m_trustedHosts;
}

CWL_DECLARE_SINGLETON_FOR_CLASS(LicenseManager);

/**
 License validity. The license can be invalid for several reasons. Can be corrupted, expired or simply missing. Without a valid license the use of SDK functionalities is
 not possible and will probably result in an application crash. In case this function returns <i>false</i> the application should be terminated after an error message.
 
   @return <i>true</i> if license is valid, <i>false</i> if the licence is invalid.
 */
- (BOOL)isValid;

/**
 Returns the server endpoint used for calls to the Time4User backend
 
   @return Time4User endpoint
 */
- (NSString *)getTime4userEndpoint;

/**
 Returns the server endpoint for the calls to TimedID backend
 
   @return Time4ID endpoint
 */
- (NSString *)getTime4eidEndpoint;
- (NSString *)getCtxNode;
- (NSString *)getCidSeed;

/**
 Returns the value specified in the <b>customer</b> field of the license
 
   @return customer specified in license
 */
- (NSString *)getCustomer;

/**
 Returns the value specified in the <b>authorization</b> field of the license
 
   @return  authorization specified in license
 */
- (NSString *)getAuthorization;

/**
 The license permits to manage tokens protecting them internally without the need of a PIN. If the license does not permit implicit PIN, all tokens require a PIN protection
 
   @return <i>true</i> if license allows implicit PIN protection (PIN can be <i>nil</i> <i>false</i> if the PIN protection is mandatory.
 */
- (BOOL)allowImplicitPin;

- (NSArray*)getTrustedHostList;

- (bool)hasAlternateLogin:(NSString*)sAlternateLogin;

/**
 Returns the license type
 
   @return The licence type.
   @see T4LicenseType
 */
- (T4LicenseType)getLicenseType;

/**
 Returns the intermediate server name depending on the license type.
 
 @param licenseType The license type (T4LicenseType)
 
 @return The intermediate server name. It is '.int4mind.' for INTEGRATION, '.test4mind.' for COLLAUD and ''.time4mind.' for PRODUCTION
 */
- (NSString*)getMidServerName: (T4LicenseType)licenseType;

/**
 * Getter for the Alternate login array
 * @return the Alternate login array
 */
- (NSArray *)getAlternateLogins;

/**
 * Getter for the condition on whether the user registration is enabled or not
 * @return <code>true</code> if the user Login should be hidden, <code>false</code> otherwise
 */
- (BOOL)hideUserRegistration;

/**
 * Getter for the condition on whether google authenticator features are enabled or not
 * @return <code>true</code> if the google authenticator should be hidden, <code>false</code> otherwise
 */
- (BOOL)hideGoogleAuthenticator;

/**
 * Getter for the condition on whether OOB transactions features are enabled or not
 * @return <code>true</code> if OOB transactions should be present, <code>false</code> otherwise
 */
- (BOOL)isOobEnabled;

-(NSString *) oauthEndpoint;
-(NSString *) clientId;
-(NSString *) clientSecret;

@end
