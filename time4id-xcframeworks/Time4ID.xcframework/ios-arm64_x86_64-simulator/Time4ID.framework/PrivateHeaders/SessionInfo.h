//
//  SessionKey.h
//  T4eID_AUTH
//
//  Created by Matteo Argento on 9/17/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import "T4Response.h"

/**
 *  SessionInfo is returned by the backend upon a successful login
 */
@interface SessionInfo : T4Response

/**
 *  If the user log in successfully, contains the session key to use in all the other calls. If the login fails is <i>nil</i>
 */
@property (nonatomic, retain) NSString *sessionKey;

@end
