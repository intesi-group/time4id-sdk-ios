//
//  OOBRequest.h
//  T4M_Connector
//
//  Created by David Beduzzi on 19/02/15.
//  Copyright (c) 2015 sinossi. All rights reserved.
//

#import <Time4ID/T4Response.h>
#import <Time4ID/OtpAccountIdCore.h>

/**
 Push Notification transaction possible statuses:
 */
typedef NS_ENUM(NSUInteger, T4OOBStatus){
    /**
     Push Authentication transaction has not yet been processed by receiver
     */
    T4OOBNew = 0,
    /**
     Push Authentication transaction has been approver
     */
    T4OOBApproved = 1,
    /**
     Push Authentication transaction has been rejected
     */
    T4OOBDenied = 2,
    /**
     There has been an internal error
     */
    T4OOBError = 3,
    /**
     Push Authentication transaction has expired
     */
    T4OOBExpired = 4,
    /**
     Push Authentication transaction has been canceled by the user
     */
    T4OOBCanceled = 5,
    /**
     The Transaction response was submitted but is being validated by backend
     */
    T4OOBWaiting = 6,
    /**
     The response for an autentication that was waiting was not given within the specified timeout
     */
    T4OOBUndefined = -1
};

/**
 Token selection for tokenless transactions:
 */
typedef NS_ENUM(NSUInteger, T4OOBTokenSelect){
    /**
     Always select from token list
     */
    T4OOBTokenSelectAlways = 0,
    /**
     Select from token list unless there is exactly 1 token available, in this case select the token automatically
     */
    T4OOBTokenSelectIfMany = 1,
};

/**
 Push Authentication token confirmation:
 */
typedef NS_ENUM(NSUInteger, T4OOBTokenConfirm){
    /**
     Push Authentication token selection confirmation is always requested (default)
     */
    T4OOBTokenConfirmAlways = 0,
    /**
     Push Authentication token selection confirmation is requested only if the token is autoselected
     */
    T4OOBTokenConfirmOnAutoselect = 1,
    /**
     Push Authentication token selection confirmation is never requested
     */
    T4OOBTokenConfirmNever = 2
};

/**
 Push Authentication transaction possible token types:
 */
typedef NS_ENUM(NSUInteger, T4OOBTokenType){
    /**
     Push Authentication transaction token is a T4Mind token in APP
     */
    T4OOBTypeApp = 0,
    /**
     Push Authentication transaction token is a T4Mind token NOT in APP (SMS,Email,...)
     */
    T4OOBTypeNoApp = 1,
    /**
     Push Authentication transaction token is an external token
     */
    T4OOBTypeExt = 2
};

/**
 Push Authentication transaction possible statuses:
 */
typedef NS_ENUM(NSUInteger, T4OOBAuthType) {
    /**
     Push Authentication transaction OTP is generated normally (no digest)
     */
    T4OOBBaseAuth = 0,
    /**
     Push Authentication transaction OTP is generated using OCRA alg with CONTENT of the transaction as digest
     */
    T4OOBContentOCRAAuth = 1,
    /**
     Push Authentication transaction OTP is generated using OCRA alg with RESULT of the transaction as digest
     */
    T4OOBResultOCRAAuth = 2,
    /**
     Push Authentication transaction OTP is generated using OCRA alg with (CONTENT + RESULT) as digest
     */
    T4OOBCompleteOCRAAuth = 3
};

#define OOB_CONTENT_TYPE_JSON   @"application/json"
#define OOB_CONTENT_TYPE_PDF    @"application/pdf"
#define OOB_CONTENT_TYPE_XML    @"application/xml"
#define OOB_CONTENT_TYPE_LINK   @"text/html"

/**
 *  T4MResponse  to the Push Authentication transaction retrieval APIs. Contains the informations regarding the transaction.
 */
@interface OOBRequest : T4Response <NSCopying>

/**
 *  Transaction identifier
 */
@property (nonatomic, retain) NSString* transactionId;

/**
 *  Transaction status
 */
@property (nonatomic) T4OOBStatus status;

/**
 *  Account id for the token which authentication the transaction requires
 */
@property (nonatomic, retain) OtpAccountIdCore* accountId;

/**
 *  UniqueTokenIdentifier id for the token which authentication the transaction requires
 */
@property (nonatomic, retain) NSString* uniqueTokenId;

/**
 *  Service name for the token to be chosen (OPTIONAL)
 */
@property (nonatomic, retain) NSString* service;

/**
 *  Descriptive title of the transaction
 */
@property (nonatomic, retain) NSString* title;

/**
 *  Transaction's sender identifier
 */
@property (nonatomic, retain) NSString* sender;

/**
 * Hides the header in App (OPTIONAL)
 */
@property (nonatomic, assign) BOOL hide;

/**
 * Automatically selects the token for a transaction without token defined if there is just one token present. (OPTIONAL)
 */
@property (nonatomic, assign) T4OOBTokenSelect tokenSelect;

/**
 *  Selection confirmation (OPTIONAL)
 */
@property (nonatomic) T4OOBTokenConfirm selectConfirm;

/**
 *  Transaction creation time
 */
@property (nonatomic, retain) NSDate* time;

/**
 *  Content-type of the content of the transaction
 */
@property (nonatomic, retain) NSString* contentType;

/**
 *  Transaction content
 *  Result is in the key "contentType" of the returned dictionary
 *  
 */
@property (nonatomic, retain) NSDictionary* content;

/**
 *  Transaction digest. If present, it is the data to be signed by the client. If not present, the digest is calculated on the content. (OPTIONAL)
 */
@property (nonatomic, retain) NSData* digest;

/**
 * if true the content is base64-encoded (OPTIONAL)
 */
@property (nonatomic, assign) BOOL base64;

/**
 *  Transacion wizard Params (OPTIONAL)
 */
@property (nonatomic, retain) NSArray* param;

/**
 *  Identifies the OTP generation algorithm for the transaction
 *  DEFAULT VALUE: 3
 */
@property (nonatomic) T4OOBAuthType authType;

/**
 *  Identifies the transaction type
 *  DEFAULT VALUE: 0
 */
@property (nonatomic) NSInteger opType;

/**
 *  Identifies the Token Type for the transaction
 *  DEFAULT VALUE: 0
 */
@property (nonatomic) T4OOBTokenType tokenType;

/**
 *  Transaction expiration Date
 */
@property (nonatomic, retain) NSDate* expiryDate;

/**
 *  Context Node, to be used as filter for the securePinCertificate if present
 */
@property (nonatomic, retain) NSString* ctxNode;

/**
 *  Wizard OTP (NOT SAVED IN DB)
 */
@property (nonatomic, retain) NSString* wizardOtp;

/**
 *  Wizard PIN (NOT SAVED IN DB)
 */
@property (nonatomic, retain) NSString* wizardPin;


- (id)initWithErrorCode:(int)error errorMessage:(NSString *)errorMessage response:(NSDictionary *)info username:(NSString*)username;

@end
