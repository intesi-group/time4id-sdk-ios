//
//  OtpServiceManager.h
//  T4eID_T4M_AUTH
//
//  Created by Matteo Argento on 09/01/14.
//  Copyright (c) 2014 sinossi. All rights reserved.
//

#import <Time4ID/CWLSynthesizeSingleton.h>
#import <Time4ID/T4Response.h>
#import <Time4ID/InitTokenResponse.h>
#import <Time4ID/PendingTokenResponse.h>
#import <Time4ID/OtpConfigurationManager.h>
#import <Time4ID/OOBRequest.h>
#import <Time4ID/Base64Util.h>
#import <Time4ID/NSDataConversion.h>

/**
 Token registration mode
 */
typedef enum {
    SMS_MODE, APP_MODE
} initTokenOtpMode;

/**
 *  Manages all the token operation interacting with the service backend
 */
@interface OtpServiceManager : NSObject

@property (nonatomic,retain) NSString* touchIdMessage;

CWL_DECLARE_SINGLETON_FOR_CLASS(OtpServiceManager);

/**
 Sends a request to the backend for a registration OTP. The OTP will be sent by the service configuret token bearer. Normally SMS or e-mail.<br>
 <i><font color="red">deprecated</font></i>: Use the version with otpId instead.
 
   @param sessionKey  logged user session, obtained from SessionInfo.
   @param otpProvider the cryptogtaphic provider for the token.
   @param PIN         Token PIN or _nil_ if not required.
   @param cid         The CID (see HardeningManager:generateCid).
   @param cia         The CIA (see HardeningManager:generateCia).
 
   @return T4MResponse  containing the operation result.
 */
- (T4Response *)pushOTPWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider PIN:(NSString *)PIN cid:(NSString *)cid cia:(NSString *)cia __deprecated;

/**
 Sends a request to the backend for a registration OTP. The OTP will be sent by the service configuret token bearer. Normally SMS or e-mail.
 
 @param sessionKey  logged user session, obtained from SessionInfo.
 @param otpProvider the cryptogtaphic provider for the token.
 @param PIN         Token PIN or _nil_ if not required.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
 
 @return T4MResponse  containing the operation result.
 */
- (T4Response *)pushOTPWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider PIN:(NSString *)PIN cid:(NSString *)cid cia:(NSString *)cia otpId:(NSString *)otpId;

/**
 Completes the token enroll by downloading the token seed from the backend and saving it on the local database. The seed on the local database is saved encrypted with a symmetrical key derived by the given PIN. If no PIN protection is specified, the seed is encrypted with an internally generated simmetrical key.<br>
 <i><font color="red">deprecated</font></i>: Use the version with getSeedWithSessionKeyEx instead.
 
 @param sessionKey       Logged user session, obtained from SessionInfo.
 @param username         Current user
 @param otpProvider      the cryptogtaphic provider for the token.
 @param otp              The code received from the backend that protects the received seed.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param pin              The PIN that protects the encrypted seed or _nil_ if the pinProtection is T4PPNone.
 
 @return The operation outcome.
 */
- (T4Response *)getSeedWithSessionKey:(NSString *)sessionKey username:(NSString *)username otpProvider:(NSString *)otpProvider otp:(NSString *)otp cid:(NSString *)cid cia:(NSString *)cia pin:(NSString *)pin __deprecated;

/**
 Completes the token enroll by downloading the token seed from the backend and saving it on the local database. The seed on the local database is saved encrypted with a symmetrical key derived by the given PIN. If no PIN protection is specified, the seed is encrypted with an internally generated simmetrical key.<br>
  <i><font color="red">deprecated</font></i>: Use the version with getSeedWithSessionKeyEx instead.
 
 @param sessionKey       Logged user session, obtained from SessionInfo.
 @param username         Current user
 @param otpProvider      the cryptogtaphic provider for the token.
 @param otp              The code received from the backend that protects the received seed.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param pin              The PIN that protects the encrypted seed or _nil_ if the pinProtection is T4PPNone.
 @param otpId            With the otpProvider uniquely identifies the token for thich the OTP is required
 
 @return The operation outcome.
 */
- (T4Response *)getSeedWithSessionKey:(NSString *)sessionKey username:(NSString *)username otpProvider:(NSString *)otpProvider otp:(NSString *)otp cid:(NSString *)cid cia:(NSString *)cia pin:(NSString *)pin otpId:(NSString *)otpId __deprecated;

/**
 Completes the token enroll by downloading the token seed from the backend and saving it on the local database. The seed on the local database is saved encrypted with a symmetrical key derived by the given PIN. If no PIN protection is specified, the seed is encrypted with an internally generated simmetrical key.
 
   @param sessionKey       Logged user session, obtained from SessionInfo.
   @param username         Current user
   @param otpProvider      the cryptogtaphic provider for the token.
   @param otp              The code received from the backend that protects the received seed.
   @param authCode         The authentication code. If required, is alternative to the _otp_ parameter.
   @param cid               The CID (see HardeningManager:generateCid).
   @param cia               The CIA (see HardeningManager:generateCia).
   @param pin              The PIN that protects the encrypted seed or _nil_ if the pinProtection is T4PPNone.
   @param otpId            With the otpProvider uniquely identifies the token for thich the OTP is required.
   @param pinProtection    The PIN protection level.
   @param pinErrorEvidence The PIN error evicence. If _true_, trying to generate an OTP with an incorrect PIN encounters an error. If _false_ the OTP generation is always successful, but the generated OTP is incorrect.
 
   @return The operation outcome.
 */
- (T4Response *)getSeedWithSessionKeyEx:(NSString *)sessionKey username:(NSString *)username otpProvider:(NSString *)otpProvider otp:(NSString *)otp authCode:(NSString*)authCode cid:(NSString *)cid cia:(NSString *)cia pin:(NSString *)pin otpId:(NSString *)otpId pinProtection: (T4PinProtection ) pinProtection pinErrorEvidence:(bool)pinErrorEvidence;

/**
 Activates a newly registered token.<br>
 <i><font color="red">deprecated</font></i>: Use the version with otpId instead.
 
 @param sessionKey  Logged user session, obtained from SessionInfo.
 @param otpProvider the cryptogtaphic provider for the token.
 @param otp         An OTP generated using the token identified by otpProvider and otpId. <b>Use generateAuthenticationOtp for this OTP</b>
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 
 @return The operation outcome.
 */
- (T4Response *)activateTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider otp:(NSString *)otp cid:(NSString *)cid cia:(NSString *)cia __deprecated;

/**
 Activates a newly registered token
 
   @param sessionKey  Logged user session, obtained from SessionInfo.
   @param otpProvider the cryptogtaphic provider for the token.
   @param otp         An OTP generated using the token identified by otpProvider and otpId. <b>Use generateAuthenticationOtp for this OTP</b>
   @param cid         The CID (see HardeningManager:generateCid).
   @param cia         The CIA (see HardeningManager:generateCia).
   @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
 
   @return The operation outcome.
 */
- (T4Response *)activateTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider otp:(NSString *)otp cid:(NSString *)cid cia:(NSString *)cia otpId:(NSString *)otpId;

/**
 Authenticates an OTP or a challenge response with the backend.<br>
 <i><font color="red">deprecated</font></i>: Use the version with otpId instead.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param otp               An OTP generated using the token identified by otpProvider and otpId. <b>Use generateOtpExtended for this OTP</b>
 @param signatureData     The data signed by the challenge response
 @param challengeResponse The challenge response
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 
 @return The operation outcome.
 */
- (T4Response *)authenticateWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider otp:(NSString *)otp signatureData:(NSString *)signatureData challengeResponse:(NSString *)challengeResponse cid:(NSString *)cid cia:(NSString *)cia __deprecated;

/**
 Authenticates an OTP or a challenge response with the backend.
 
   @param sessionKey        Logged user session, obtained from SessionInfo.
   @param otpProvider       The cryptogtaphic provider for the token.
   @param otp               An OTP generated using the token identified by otpProvider and otpId. <b>Use generateOtpExtended for this OTP</b>
   @param signatureData     The data signed by the challenge response
   @param challengeResponse The challenge response
    @param cid         The CID (see HardeningManager:generateCid).
    @param cia         The CIA (see HardeningManager:generateCia).
    @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
 
   @return The operation outcome.
 */
- (T4Response *)authenticateWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider otp:(NSString *)otp signatureData:(NSString *)signatureData challengeResponse:(NSString *)challengeResponse cid:(NSString *)cid cia:(NSString *)cia otpId:(NSString *)otpId;

/**
 Requests the server to create a token for the client
 
 @param username        Logged user username.
 @param sessionKey      Logged user session, obtained from SessionInfo.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param telephoneNumber User's telephone number. This is required only if the confirmation code will be sent to the user via SMS (token bearer = 1), otherwise can be left _nil_
 @param pin             Security PIN if token bearer is SMS. Otherwise can be _nil_
 @param serialNumber    Hardware token identifier (for future use)
 @param otpMode         How the seed protection code is transferred to the client: 0 → SMS 1 → APP 2 → EMAIL
 @param service         The service which the new token belongs to. For instance _Time4ID_
 @param label           New token label
 
   @return The operation outcome.
 */
- (InitTokenResponse *)initializeTokenWithUsername:(NSString *)username sessionKey:(NSString *)sessionKey cid:(NSString *)cid cia:(NSString *)cia telephoneNumber:(NSString *)telephoneNumber pin:(NSString *)pin serialNumber:(NSString *)serialNumber otpMode:(initTokenOtpMode)otpMode otpProvider:(NSString *)otpProvider service:(NSString *)service label:(NSString *)label;

/**
 Requires a new token from the backend. If a new token is generated, the corresponding structures are created and initialized on the local database. The token registration will be then completed by downloading the token seed and saving it encrypted on the local database with getSeedWithSessionKeyEx.
 
   @param username        Logged user username.
   @param sessionKey      Logged user session, obtained from SessionInfo.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
   @param telephoneNumber User's telephone number. This is required only if the confirmation code will be sent to the user via SMS (token bearer = 1), otherwise can be left _nil_
   @param pin             Security PIN if token bearer is SMS. Otherwise can be _nil_
   @param serialNumber    Hardware token identifier (for future use)
   @param otpMode         How the seed protection code is transferred to the client: 0 → SMS 1 → APP 2 → EMAIL
   @param service         The service which the new token belongs to. For instance _Time4ID_
   @param label           New token label
 
   @return Informations about the newly created token.
 
    @see InitTokenResponse
 */
- (InitTokenResponse *)implicitInitializeTokenWithUsername:(NSString *)username sessionKey:(NSString *)sessionKey cid:(NSString *)cid cia:(NSString *)cia telephoneNumber:(NSString *)telephoneNumber pin:(NSString *)pin serialNumber:(NSString *)serialNumber otpMode:(initTokenOtpMode)otpMode service:(NSString *)service label:(NSString *)label;

/**
 Save a newly created token to the local database. This can be required for operations that do not automatically save the downloaded token to the local SDK database.
 
   @param initTokenResponse A token initialization response obtained by the backend.
   @param username          Current logged user. The token will be saved as owned by this user.
 
   @return _true_ if the token was correctly create in the SDK internal database, otherwise _false_
 */
- (BOOL)saveTokenWithInitTokenResponse:(InitTokenResponse *)initTokenResponse username:(NSString *)username;

/**
 Retrieves an already created token from the backend
 
 @param username        Logged user username.
 @param sessionKey      Logged user session, obtained from SessionInfo.
   @param identifier The remote token identifier. An identifier, received by the backend for the token to download.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 
 @return Informations about the remote token.
 
 @see InitTokenResponse
 */
- (InitTokenResponse *)getTokenWithUsername:(NSString *)username sessionKey:(NSString *)sessionKey identifier:(NSString *)identifier cid:(NSString *)cid cia:(NSString *)cia;

/**
 Renews an already created token. A token renewal can be necessary if for some reason the token cryptographic data (e.g. seed) becomes corrupted or no more safe.
 After a renewal, it will be necessary to download again the token seed. Indeed the backend will re-create a new seed for the token.<br>
  <i><font color="red">deprecated</font></i>: Use the version with otpId instead.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 
 @return The operation outcome.
 */
- (T4Response *)renewTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia __deprecated;

/**
 Renews an already created token. A token renewal can be necessary if for some reason the token cryptographic data (e.g. seed) becomes corrupted or no more safe.
 After a renewal, it will be necessary to download again the token seed. Indeed the backend will re-create a new seed for the token.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
 
   @return The operation outcome.
 */
- (T4Response *)renewTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia otpId:(NSString *)otpId;

/**
 Sets a new token label on the backend token database.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
   @param otp         An implicit otp generated using the token to authenticate the operation.
   @param label       The new label.
 
   @return The operation outcome.
 */
- (T4Response *)updateTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia otpId:(NSString *)otpId otp:(NSString *)otp label:(NSString *)label;

/**
 Deletes a token on the backend.<br>
  <i><font color="red">deprecated</font></i>: Use the version with otpId instead.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 
 @return The operation outcome.
 */
- (T4Response *)deleteTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia __deprecated;

/**
 Deletes a token on the backend.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
 
   @return The operation outcome.
 */
- (T4Response *)deleteTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia otpId:(NSString *)otpId;

/**
 Synchronizes the token with the backend using two consecutive OTP values.<br>
 <i><font color="red">deprecated</font></i>: Use the version with otpId instead.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param otp         First OTP
 @param otpNext     Second OTP
 
 @return The operation outcome.
 
 @see OtpResult
 */
- (T4Response *)synchTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia otp:(NSString *)otp otpNext:(NSString *)otpNext __deprecated;

/**
 Synchronizes the token with the backend using two consecutive OTP values.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
   @param otp         First OTP
   @param otpNext     Second OTP
   @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
 
   @return The operation outcome.
 
    @see OtpResult
 */
- (T4Response *)synchTokenWithSessionKey:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia otp:(NSString *)otp otpNext:(NSString *)otpNext otpId:(NSString *)otpId;

/**
 Generates a challenge for a challenge-response type authentication.<br>
 <i><font color="red">deprecated</font></i>: Use the version with otpId instead.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 
 @return The operation outcome.
 */
- (T4Response *)generateChallenge:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia  __deprecated;

/**
 Generates a challenge for a challenge-response type authentication
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param otpProvider       The cryptogtaphic provider for the token.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param otpId       With the otpProvider uniquely identifies the token for thich the OTP is required.
 
   @return The operation outcome.
 */
- (T4Response *)generateChallenge:(NSString *)sessionKey otpProvider:(NSString *)otpProvider cid:(NSString *)cid cia:(NSString *)cia otpId:(NSString *)otpId;

/**
 Retrieves the list of pending tokens for the logged user
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 
   @return List of pending tokens on the backend. The array elements are InitTokenResponse objects
 */
- (PendingTokenResponse *)getPendingTokenWithSessionKey:(NSString *)sessionKey cid:(NSString *)cid cia:(NSString *)cia;

- (T4Response*)checkAndStoreSvrHalfkeyForUser:(NSString*)username sessionKey:(NSString*)sessionKey cid:(NSString*)cid cia:(NSString*)cia;

/**
 Returns an Out Of Band transaction data, based on the transaction identifier.
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
   @param username   Logged user.
   @param oobId      transaction identifier
 
   @return An object containing the informations about the transaction
 */
- (OOBRequest *)getOOBWithSessionKey:(NSString *)sessionKey cid:(NSString *)cid cia:(NSString *)cia username:(NSString*)username id:(NSString*)oobId;

/**
 Returns the list of OOB pending Transactions
 
 @param sessionKey        Logged user session, obtained from SessionInfo.
 @param cid         The CID (see HardeningManager:generateCid).
 @param cia         The CIA (see HardeningManager:generateCia).
 @param username   Logged user.
 
 @return The list of pending OOB Transactions
 */
- (T4Response *)pendingOOBWithSessionKey:(NSString *)sessionKey cid:(NSString *)cid cia:(NSString *)cia username:(NSString*)username;

/**
 Returns the server a response for a requested out of band transaction
 
 @param sessionKey      Logged user session, obtained from SessionInfo.
 @param cid             The CID (see HardeningManager:generateCid).
 @param cia             The CIA (see HardeningManager:generateCia).
 @param username        Logged user. if _deletePending_ is _true_ the username is needed in order to delete the transaction internally, could be _nil_ otherwise
 @param request         An OOB request object returned by getOOBWithSessionKey.
 @param pin             The token PIN for the token specified by accountId in the OOBRequest.
 @param approved        _true_ if the transaction is approver, otherwise _false_. If result is false, the token PIN is not requires, since the transaction data will not be signed.
 @param result          An array containing the user responses to the optional wizard
 @param deletePending   _true_ if the transaction should be deleted, otherwise _false_. If the transaction is deleted the _response_ param is ignored.
 @param onCompletion    Asynchronous block called, containing the operation outcome

 */
- (void)setOOBResultWithSessionKey:(NSString *)sessionKey cid:(NSString *)cid cia:(NSString *)cia username:(NSString*)username oobRequest:(OOBRequest*)request pin:(NSString*)pin approved:(bool)approved result:(NSArray*) result deletePending:(bool) deletePending onCompletion:(void (^)(T4Response*))onCompletion;

typedef NS_ENUM(NSUInteger, T4OOBFilter){
    /**
     All the transactions
     */
    T4OOBAll = 0,
    /**
     Completed transactions (Approved or Denied)
     */
    T4OOBCompleted = 1,
    /**
     Uncompleted transactions
     */
    T4OOBUncompleted = 2
};

/**
 Gets the history OOB transactions for the given user
 
 @param user   Current logged in user
 @param filter Transaction completion filter (see T4OOBFilter).
 
 @return Array of resulting OOBRequest objects
 @see OOBRequest
 @see T4OOBStatus
 */
-(NSArray*)getOOBTransactionsWithUser:(NSString*)user filter:(T4OOBFilter)filter;

/**
 Get the number of transactions in transaction history for the given user
 
 @param user   Current logged in user
 @param filter Transaction completion filter (see T4OOBFilter).
 
 @return Resulting number of transactions
 @see T4OOBStatus
 */
-(NSInteger)getOOBTransactionsCountWithUser:(NSString*)user filter:(T4OOBFilter)filter;

/**
 Delete from history the transaction with the specified Id
 
 @param transactionId id of the transaction to be deleted
 
 @return this command always returns true
 */
-(bool)deleteOOBTransactionWithTransactionId:(NSString*)transactionId;

/**
 Delete from history the transactions for the given user and filter
 
 @param user   Current logged in user
 @param filter Transaction completion filter (see T4OOBFilter).
 
 @return false if failed, otherwise true
 */
-(bool)deleteOOBTransactionsWithUser:(NSString*)user filter:(T4OOBFilter)filter;

/**
 Set a status for the transactions with the specified Id
 
 @param status Transaction status (T4OOBStatus).
 @param transactionId id of the transaction to be deleted
 
 @return false if failed, otherwise true
 */
-(bool)setOOBTransactionStatus:(T4OOBStatus)status WithTransactionId:(NSString*)transactionId;

/*
 Extra keys:
 updatedTokenCount : number of tokens changing status
 enabledDevices : info about which devices enabled tokens disabled on current device
 - accountId : accountId for disabled token
 - dev : string descriptive of device that enabled token
 */
- (T4Response*)updateTokenStatusWithSessionKey:(NSString *)sessionKey cid:(NSString *)cid cia:(NSString *)cia accountIds: (NSArray*)accountIds;

/**
 Gets the information of the new token to be created from the scanned QR code, and creates the token using the InitTokenResponse structure
*/
- (InitTokenResponse*) initializeTokenResponseWithGoogleAuthQRCode:(NSDictionary*)QrCodeData username:(NSString*)username;

/**
 * Completes google authenticator token generation by setting the seed in the internal DB and by initializing the token
 * @param secret the seed contained in the QRCode
 * @param otpAccountId the OTP account
 * @param pin the security PIN of the token
 * @param pinProtection the type of PIN protection available for the token
 * @return a response from the server in the form of a T4MResponse object
 */
-(T4Response*) completeTokenGenerationWithGoogleAuthSecret:(NSString*)secret OtpAccountId:(OtpAccountIdCore*)otpAccountId pin:(NSString*)pin andPinProtection:(T4PinProtection)pinProtection;

/**
 * Retrieves the EndToEnd Certificate to be used in order to ensure maximum security in all server calls
 * @param sessionKey the currently available session key
 * @return a response from the server in the form of a {@link T4MResponse} object
 */
-(T4Response*) getEndToEndCertificate:(NSString*) sessionKey;

/**
 * Retrieves the SecurePin Certificate to be used in order to ensure maximum security in the setOOBResult API when we have a sign transaction
 * @param sessionKey the currently available session key
 * @param ctxNode node where the service belongs
 * @return a response from the server in the form of a {@link T4MResponse} object
 */
-(T4Response*) getSecurePinCertificateWithSessionKey:(NSString*)sessionKey andCtxNode:(NSString*)ctxNode;


- (T4Response *)getTokenInfoWithSessionKey:(NSString*)sessionKey cid:(NSString*)cid cia:(NSString*)cia OtpProvider:(NSString*) otpProvider andOtpId: (NSString*) otpId;

@end

bool isNumeric( NSString* s );
