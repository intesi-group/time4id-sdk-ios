//
//  OtpDataDao.h
//  T4eID_CORE
//
//  Created by Matteo Argento on 9/4/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/BaseDao.h>
#import <Time4ID/OtpDataCore.h>

@interface OtpDataDao : BaseDao

- (BOOL)insertOtpData:(OtpDataCore *)otpData;
- (BOOL)updateOtpData:(OtpDataCore *)otpData;
- (BOOL)existsOtpDataByAccountId:(int)accountId;
- (BOOL)insertOrUpdateOtpData:(OtpDataCore *)otpData;
- (OtpDataCore *)getOtpDataByAccountId:(int)accountId;
- (NSArray *)findAll;
- (void)deleteOtpDataById:(int)id;
- (void)deleteAll;

@end
