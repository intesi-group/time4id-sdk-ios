//
//  AuthenticatorManager.h
//  T4eID_AUTH
//
//  Created by Matteo Argento on 9/17/13.
//  Copyright (c) 2013 sinossi. All rights reserved.
//

#import <Time4ID/SessionInfo.h>
#import <Time4ID/CWLSynthesizeSingleton.h>
#import <Time4ID/OtpAccountIdCore.h>
#import <Time4ID/T4Response.h>
#import <Time4ID/DatabaseManagerCore.h>
//#import "GraphicUtilsCore.h"

/**
 *  AuthenticationManager manager the user account and user connection with the Time4User and Time4ID backend
 */
@interface AuthenticatorManager : NSObject
{
    NSMutableDictionary* m_dHttpHeaders;
    NSMutableDictionary* m_dCookieComponents;
    //NSString* m_sSessionKey;
}

- (id)init;

CWL_DECLARE_SINGLETON_FOR_CLASS(AuthenticatorManager);

/**
 Logout the current user. Closes the session.
 
   @param sessionKey session where the user is logged in
 
   @return T4MResponse  from the remote server.
 
  @see T4MResponse 
 */
- (T4Response *)logout:(NSString *)sessionKey;

/**
 Try to login to Time4Mind services
 
   @param username        username
   @param accountPassword password
   @param nodeId          Time4Mind node where to log-in.
 
   @return A SessionInfo object containing information about the login status and the corresponding session.
 */
- (SessionInfo *)submitLoginWithUsername:(NSString *)username accountPassword:(NSString *)accountPassword nodeId:(NSString *)nodeId;

/**
 Try to login to Time4Mind services
 
   @param username        username
   @param accountPassword password
   @param nodeId          Time4Mind node where to log-in.
   @param contextType     Extra value to specify to the backend. If not explicitely requested, can be <i>nil</i>.
 
   @return A SessionInfo object containing information about the login status and the corresponding session.
 */
- (SessionInfo *)submitLoginWithUsername:(NSString *)username accountPassword:(NSString *)accountPassword nodeId:(NSString *)nodeId contextType:(NSNumber *)contextType;

/**
 Login to the Time4Mind serviced using a parameters dictionary
 
   @param paramsDictionary login parameters. The parameters depend by the backend configuration.
   @param nodeId           Time4Mind node where to log-in
 
   @return A SessionInfo object containing information about the login status and the corresponding session.
 
   @see SessionInfo
 */
- (SessionInfo *)submitLoginWithParamsDictionary:(NSDictionary *)paramsDictionary nodeId:(NSString *)nodeId;

/**
 Login to the Time4Mind serviced using a parameters dictionary
 
   @param paramsDictionary login parameters. The parameters depend by the backend configuration.
   @param nodeId           Time4Mind node where to log-in.
   @param contextType      Extra value to specify to the backend. If not explicitely requested, can be <i>nil</i>.
 
   @return A SessionInfo object containing information about the login status and the corresponding session.
 
   @see SessionInfo
 */
- (SessionInfo *)submitLoginWithParamsDictionary:(NSDictionary *)paramsDictionary nodeId:(NSString *)nodeId contextType:(NSNumber *)contextType;

/**
 Login to the Time4Mind serviced using a parameters dictionary
 
 @param paramsDictionary login parameters. The parameters depend by the backend configuration.
 @param nodeId           Time4Mind node where to log-in.
 @param contextType      Extra value to specify to the backend. If not explicitely requested, can be <i>nil</i>.
 @param dPushId      Device PushID for remote notifications
 
 @return A SessionInfo object containing information about the login status and the corresponding session.
 
 @see SessionInfo
 */
- (SessionInfo *)submitLoginWithParamsDictionary:(NSDictionary *)paramsDictionary nodeId:(NSString *)nodeId contextType:(NSNumber *)contextType pushId:(NSData*)dPushId;

/**
 User login with username and password
 
 @param username        username
 @param accountPassword user password
 @param dPushId      Device PushID for remote notifications
 @param nodeId       node identifier
 @param contextType  context type
 @param rememberMe     true if the OAUTH renewal session mechanism is active (OAUTH parameters will be returned in case of successful login).
 
 @return session and in case of remember me login, OAUTH parameters
 */
- (T4Response *)submitLoginWithUsername:(NSString *)username accountPassword:(NSString *)accountPassword pushId:(NSData*)dPushId nodeId:(NSString *)nodeId contextType:(NSNumber *)contextType rememberMe:(bool)rememberMe;


- (T4Response *)getFormTemplateWithSessionKey:(NSString *)sessionKey formName:(NSString *)formName layout:(NSNumber *)layout language:(NSString *)language contextType:(NSNumber *)contextType;
- (T4Response *)getFormTemplateWithSessionKey:(NSString *)sessionKey formName:(NSString *)formName layout:(NSNumber *)layout language:(NSString *)language;
- (T4Response *)submitFormDataWithSessionKey:(NSString *)sessionKey method:(NSString *)method formData:(NSDictionary *)formData contextType:(NSNumber *)contextType;
- (T4Response *)submitFormDataWithSessionKey:(NSString *)sessionKey method:(NSString *)method formData:(NSDictionary *)formData;

/**
 Retrieve information sbout the logged user
 
   @param sessionKey logged user session, obtained from SessionInfo
 
   @return a T4MResponse  structure containing an NSDictionary with the user details. The details are specific to the backend implementation.
 */
- (T4Response *)retrieveUserInfoWithSessionKey:(NSString *)sessionKey;

/**
 Retrieve information sbout the logged user
 
   @param sessionKey logged user session, obtained from SessionInfo
   @param contextType Extra value to specify to the backend. If not explicitely requested, can be <i>nil</i>.
 
   @return a T4MResponse  structure containing an NSDictionary with the user details. The details are specific to the backend implementation.
 */
- (T4Response *)retrieveUserInfoWithSessionKey:(NSString *)sessionKey contextType:(NSNumber *)contextType;

/**
 Sets the user information to the backend
 
   @param sessionKey           logged user session, obtained from SessionInfo
   @param attributesDictionary dictionary containing the user info. The user informations are specific to the backend implementation.
 
   @return a T4MResponse  object with the operation result.
 */
- (T4Response *)setUserInfoWithSessionKey:(NSString *)sessionKey attributesDictionary:(NSDictionary *)attributesDictionary;

/**
 Retrieves the information associated with a specific service pubblicated by the backend. The backend answers with adictionary of data describing the service specified by <i>ctxNode</i> and <i>service</i>.
 
   @param sessionKey logged user session, obtained from SessionInfo
   @param ctxNode    node where the service belongs
   @param service    internal service name
   @param iconSize   size of the icon requested can be <b>S</b>, <b>M</b> or <b>L</b> depending on the size of the icon requested.
   @param hash       service informations digest. This digest can be obtained by a previous call to this API. The hash is in the response dictionary and should be passed here. If no calls to getCompanyServiceInfoWithSessionKey were made just pass <i>nil</i>. The hash is used to check if the service informations are changed. The backend compares the given digest with a calculated value, and sends the informations only if needed.
 
    @return a T4MResponse  structure containing in the attached dictionary a description of the service. The service description data are the following:
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | domain | NSString | The internal service name  |
 | description | NSString | Description of the service   |
 | icon | NSString | The icon associated with the service, with the specified size. If the icon for that size is not avaliable or too big, this value can be <i>nil</i>. In this case you need to download the icon separately using getCompanyServiceIconWithSessionKey. The binary icon is bare64 encoded. |
 | bgColor | NSString | Color associated with the service. Is in the format: #RRGGBB |
 | contentType | NSString | Content-Type     |
 | companyUrl | NSString | URL to the company website     |
 | hash  | NSString | Service data digest, to be passed in following calls when trying to retrieve the data for the same service |
 | serviceLabel  | NSString | Service descriptive name |
 
 */
- (T4Response *)getCompanyServiceInfoWithSessionKey:(NSString *)sessionKey ctxNode:(NSString *)ctxNode service:(NSString *)service iconSize:(NSString *)iconSize hash:(NSString *)hash;

/**
 Retrieves the icon associated with the service specified by <i>ctxNode</i> and <i>service</i>.
 
 @param sessionKey logged user session, obtained from SessionInfo
 @param ctxNode    node where the service belongs
 @param service    internal service name
 @param iconSize   size of the icon requested can be <b>S</b>, <b>M</b> or <b>L</b> depending on the size of the icon requested.
 
 @return a T4MResponse  structure containing in the attached dictionary a with the service icon:
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | icon | NSString | The icon associated with the service, with the specified size. If the icon for that size is not avaliable or too big, this value can be <i>nil</i>. In this case you need to download the icon separately using getCompanyServiceIconWithSessionKey. The binary icon is bare64 encoded. |
 
 */
- (T4Response *)getCompanyServiceIconWithSessionKey:(NSString *)sessionKey ctxNode:(NSString *)ctxNode service:(NSString *)service iconSize:(NSString *)iconSize;

/**
 Returns the service profile information for the specified service
 
   @param sessionKey logged user session, obtained from SessionInfo.
   @param service The backend service. For instance _"Time4eID"_ if you want to retrieve informations about the Time4ID service.
 
   @return returns a dictionary containing all the data describing the service environment
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | accountData | NSDictionary | Dictionary containing the data about the current user account |
 | serviceData | NSDictionary | Dictionary containing the data about service configuration |
 
 <p>
 <b>accountData</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | accountStatus | NSNumber | 0 → not active<br>1 → active<br>2 → not active password reset pending<br>3 → active, password reset pending<br>4 → deactive<br>5 → deactive, password reset pending |
 | authMode | NSNumber | 0 → password<br>1 → otp<br>2 → token |
 | description   | NSString    | Account description. Can be _nil_ |
 | externalUid | NSString | User identifier. Can be _nil_ |
 | login | NSString | User's login, usually it is the registration email |
 | nickname | NSString | User's nickname. Can be _nil_ |
 
 <p>
 <b>serviceData</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | attributes | NSDictionary | Service attributes |
 | description | NSString | Service description |
 | name | NSString | The service name. Should be the same value passed in the _service_ parameter |
 
 <p>
 <b>attributes</b>
 
 | Field   | Type    | Description |
 |---------|--------|---------|
 | BEGIN_END_DISABLED | NSNumber | _true_ if the begin-end algorithm is enabled, otherwise _false_ |
 | CTX_NODE | NSString | Context node of this service |
 | DEFAULT_OTP_MODE | NSString |  |
 | DOMAIN | NSString | Service domanin, for instance: _Time4Mind_ |
 | LANGUAGE | NSString | Service language. For instance: it, en |
 | SINGLE_DEVICE_MODE | NSNumber | _true_ if a service token can be present on a single user's device. If _false_ the begin-end algorithm should be enabled to distinguish which device generated an OTP. |
 | SRV_TOKEN_APP_REQUIRED | NSNumber | _true_ if the service token is required, otherwise _false_ |
 | SRV_TOKEN_BEARER | NSNumber | Service token bearer.<br>0 → App (service token is required for this mode)<br>1 → sms<br>2 → e-mail |
 
 */
- (T4Response *)getServiceProfileWithSessionKey:(NSString *)sessionKey service:(NSString *)service;

/**
 *  Reset the password for the logged user. Use this API for account retrieval in case of forgotten password.
 *
 *  @param mail user's email. Should be the email which the user registered for his Time4Mind account.
 *
 *  @return The operation success or failure. Extra information in the errorMessage field of the T4MResponse 
 */
- (T4Response *)sendEmailResetPassword:(NSString*)mail;

/**
 *  Sets the specified cookie in the HTTP headers for all the subsequent remote calls
 *
 *  @param cookie The cookie to set in the _Cookie_ header
 */
-(void)setCookie: (NSString*) cookie;

/**
 Updates the cookie dictionary
 
 *
 *  @param cookie The cookie to set in the _Cookie_ header
 */
-(void)updateCookie: (NSString*) cookie;

/**
 *   Sets the specified header in the HTTP headers for all the subsequent remote calls
 *
 *  @param header Header name
 *  @param value  Header value
 */
-(void)setHeader: (NSString*) header withValue: (NSString*) value;

/**
 *  Remove the specified header in the HTTP headers for all the subsequent remote calls
 *
 *  @param header Header name
 */
-(void)removeHeader: (NSString*) header;

/**
 *  Returns a dictionary containing all the extra HTTP headers configured
 *
 *  @return The HTTP extra headers configured
 */
-(NSDictionary*)getHeadersDictionary;

/**
 The components of the cookies received by the Set-Cookie
 
 @return dictionary with the cookie components
 */
-(NSMutableDictionary*)getCookieComponents;

/**
 Renews the provided session information using OAUTH parameters
 
 @param sessionToken OAUTH parameter #1
 @param userToken    OAUTH parameter #2
 @param dPushId      Device PushID for remote notifications
 @param nodeId       node identifier
 @param contextType  context type
 
 @return response containing the new OAUTH parameters and the renewed session
 */
- (T4Response *)renewLoginWithSessionToken:(NSString *)sessionToken userToken:(NSString *)userToken pushId:(NSData*)dPushId nodeId:(NSString *)nodeId contextType:(NSNumber *)contextType;

-(T4Response *)createAccountWithUser:(NSString*)user andPassword:(NSString*)password;
-(T4Response *)activateUserWithSession:(NSString*)session;
-(T4Response *)resendActivationEmailForUser:(NSString*)user;
-(T4Response *)modifyPushIdWithSessionKey:(NSString*)sessionKey andPushToken:(NSData*) pushToken;

@end
