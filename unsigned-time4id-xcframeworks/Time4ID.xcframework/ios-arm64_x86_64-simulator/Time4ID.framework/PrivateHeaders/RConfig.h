//
//  RConfig.h
//  Time4ID_SDK
//
//  Created by David Beduzzi on 08/02/16.
//  Copyright © 2016 Intesi Group SPA. All rights reserved.
//

#import <Realm/Realm.h>

#define CF_INTEGER  0
#define CF_DATA     1
#define CF_STRING   2

@interface RConfig : RLMObject

@property NSString* key;
@property NSInteger type;
@property NSData* value;


-(RConfig*)initWithKey:(NSString*)key string:(NSString*)value;
-(RConfig*)initWithKey:(NSString*)key data:(NSData*)value;
-(RConfig*)initWithKey:(NSString*)key integer:(NSInteger)value;


-(NSString*)stringValue;
-(NSData*)dataValue;
-(NSInteger)integerValue;

@end
